<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Notification extends Model
{
    protected $table = "notifications";
    protected $guarded = [];


    public function sentByUser()
    {
        return $this->belongsTo(User::class, 'sent_by', 'id');
    }

    public function sentToUser()
    {
        return $this->belongsTo(User::class, 'sent_to', 'id');
    }
}
