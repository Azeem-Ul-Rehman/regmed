<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = "categories";
    protected $guarded = [];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'cat_role')->withTimestamps();
    }

}
