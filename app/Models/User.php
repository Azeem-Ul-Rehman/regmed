<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'city', 'country', 'role_id', 'gender', 'phone_number', 'address', 'user_type', 'status', 'profile_pic',
        'category_id', 'activated', 'postal_code', 'language', 'membership', 'contact_person', 'service_provider', 'creator_id', 'comments',
        'latitude', 'longitude', 'fcm_token', 'is_login', 'otp_code', 'otp_status', 'registration_date', 'last_login', 'company', 'membership_date', 'offices', 'work_field',
        'posters', 'posters_date', 'demo_kit', 'demo_kit_date', 'experience_with_cb', 'experience_with_at', 'delivery', 'delivery_date', 'signed_contract',
//      Start Unused Fields
        'contract_on', 'practice', 'estimated_due_date', 'mother_name',
//        End Unused Fields
        'interested_in', 'date_of_birth', 'how_did_it_find_us', 'father_name', 'expected_date_of_birth_of_baby', 'expected_date_clinic', 'twin_pregnancy',
        'date_of_liposuction', 'doctor', 'clinic', 'area_of_interest', 'willing_to_travel_inside_eu', 'willing_to_travel_outside_eu', 'vacations_included',
        'agreement_signed_on','signed_agreement_sent','attached_files',
        'contract_signed','contract_signed_date','kit_sent','kit_sent_date','process_completed','mild_wife'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'activated' => 'boolean',
    ];


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role')->withTimestamps();
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'This action is unauthorized.');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function serviceProvider()
    {
        return $this->belongsTo(User::class, 'service_provider', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Activation of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activation()
    {
        return $this->hasOne(Activation::class);
    }


    public function sentBy()
    {
        return $this->hasMany(Notification::class, 'sent_by', 'id');
    }

    public function sentTo()
    {
        return $this->hasMany(Notification::class, 'sent_to', 'id');
    }

//     $this->middleware('role:SUPER_ADMIN');
//      $this->middleware('role:STAFF');
//      $this->middleware('role:CUSTOMER');
//      $this->middleware('role:DRIVER');

}
