<?php

namespace App\Exports;

use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use DateTime;

class UsersExportAll implements FromCollection, WithHeadings, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $dataArray = [];
        $indexValue = 0;

        $registration_date_from = $this->request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->registration_date, '0', '10'));
        $registration_date_to = $this->request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->registration_date, '13'));


        $request = $this->request;
        $details = User::where(function ($query) use ($request, $registration_date_from, $registration_date_to) {

            if (!is_null($request->category_id)) {
                if ($request->category_id == 'all') {
                    $query->whereIn('category_id', ['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18']);
                } else {
                    $query->where('category_id', $request->category_id);
                }
            }
            if (!is_null($request->creater_id)) {
                $query->where('creator_id', $request->creetor_id);
            }
            if (!is_null($request->city)) {
                $query->where('city', $request->city);
            }
            if (!is_null($request->country)) {
                $query->where('country', $request->country);
            }
            if (!is_null($request->registration_date)) {
                $query->whereBetween('registration_date', [$registration_date_from->format('Y-m-d'), $registration_date_to->format('Y-m-d')]);
            }


            $query->whereNotIn('user_type', ['super-admin', 'prospect', 'customer']);
        })->get();
//        $details = Consignment::where(['eta' => $this->request->eta])->get();
        foreach ($details as $user) {
            $dataArray[$indexValue] = array(
                [
                    'name' => $user->first_name . ' ' . $user->last_name ?? '-',
                    'role' => $user->user_type ?? '-',
                    'category' => $user->category ? $user->category->name : '-',
                    'email' => $user->email,
                    'phone_number' => $user->phone_number ?? '-',
                    'address' => $user->address ?? '-',
                    'city' => $user->city ?? '-',
                    'country' => $user->country ?? '-',
                    'postal_code' => $user->postal_code ?? '-',
                    'status' => $user->status ?? '-',
                    'last_login' => $user->last_login ? date('Y-m-d H:i A', strtotime($user->last_login)) : '-',
                    'language' => $user->language ?? '-',
                    'contact_person' => $user->contact_person ?? '-',
                    'practice' => $user->practice ?? '-',
                    'contract_on' => $user->contract_on ?? '-',
                    'comments' => $user->comments ?? '-',
                    'membership' => $user->membership ?? '-',
                    'membership_date' => $user->membership_date ? date('Y-m-d', strtotime($user->membership_date)) : '-',
                    'delivery_date' => $user->delivery_date ? date('Y-m-d', strtotime($user->delivery_date)) : '-',
                    'posters' => $user->posters ?? '-',
                    'posters_date' => $user->posters_date ? date('Y-m-d', strtotime($user->posters_date)) : '-',
                    'demo_kit' => $user->demo_kit ?? '-',
                    'demo_kit_date' => $user->demo_kit_date ? date('Y-m-d', strtotime($user->demo_kit_date)) : '-',
                    'offices' => $user->offices ?? '-',
                    'work_field' => $user->work_field ?? '-',
                    'experience_with_cb' => $user->experience_with_cb ?? '-',
                    'experience_with_at' => $user->experience_with_at ?? '-',
                    'signed_contract' => $user->signed_contract ?? '-',

                ]);
            $indexValue++;
        }
        return collect($dataArray);
    }

    public function headings(): array
    {
        return [
            [
                'Name',
                'Role',
                'Category',
                'Email',
                'Phone Number',
                'Company',
                'Address',
                'City',
                'Country',
                'Postal Code',
                'Status',
                'Last Login',
                'Language',
                'Contact Person',
                'Practice',
                'Contract On',
                'Comments',
                'Membership',
                'Membership Date',
                'Material Delivery Date',
                'Posters',
                'Posters Date',
                'Demo Kit',
                'Demo Kit Date',
                'Offices',
                'Work Field',
                'Experience With CB/CT',
                'Experience With AT',
                'Signed Contract',

            ]
        ];
    }

    public function title(): string
    {
        return "Users Report";
    }
}
