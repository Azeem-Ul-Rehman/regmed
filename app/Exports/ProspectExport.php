<?php

namespace App\Exports;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use DateTime;

class ProspectExport implements FromCollection, WithHeadings, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $dataArray = [];
        $indexValue = 0;

        $contract_date_from = $this->request->contract_signed_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->contract_signed_date, '0', '10'));
        $contract_date_to = $this->request->contract_signed_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->contract_signed_date, '13'));


        $registration_date_from = $this->request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->registration_date, '0', '10'));
        $registration_date_to = $this->request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->registration_date, '13'));

        $kit_sent_date_from = $this->request->kit_sent_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->kit_sent_date, '0', '10'));
        $kit_sent_date_to = $this->request->kit_sent_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->kit_sent_date, '13'));


        $date_of_birth_from = $this->request->date_of_birth == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->date_of_birth, '0', '10'));
        $date_of_birth_to = $this->request->date_of_birth == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->date_of_birth, '13'));

        $expected_date_of_birth_of_baby_from = $this->request->expected_date_of_birth_of_baby == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->expected_date_of_birth_of_baby, '0', '10'));
        $expected_date_of_birth_of_baby_to = $this->request->expected_date_of_birth_of_baby == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->expected_date_of_birth_of_baby, '13'));

        $date_of_liposuction_from = $this->request->date_of_liposuction == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->date_of_liposuction, '0', '10'));
        $date_of_liposuction_to = $this->request->date_of_liposuction == null ? null : DateTime::createFromFormat('d-m-Y', substr($this->request->date_of_liposuction, '13'));


        $request = $this->request;
        $details = User::where(function ($query) use ($request, $date_of_liposuction_from, $date_of_liposuction_to, $expected_date_of_birth_of_baby_from, $expected_date_of_birth_of_baby_to, $date_of_birth_from, $date_of_birth_to, $contract_date_from, $contract_date_to, $registration_date_from, $registration_date_to, $kit_sent_date_from, $kit_sent_date_to) {

            if (!is_null($request->category_id)) {
                $query->where('category_id', $request->category_id);
            }
            if (!is_null($request->creater_id)) {
                $query->where('creator_id', $request->creetor_id);
            }
            if (!is_null($request->city)) {
                $query->where('city', $request->city);
            }
            if (!is_null($request->country)) {
                $query->where('country', $request->country);
            }
            if (!is_null($request->contract_signed)) {
                $query->where('contract_signed', $request->contract_signed);
            }
            if (!is_null($request->contract_signed_date)) {
                $query->whereBetween('contract_signed_date', [$contract_date_from->format('Y-m-d'), $contract_date_to->format('Y-m-d')]);
            }
            if (!is_null($request->registration_date)) {
                $query->whereBetween('registration_date', [$registration_date_from->format('Y-m-d'), $registration_date_to->format('Y-m-d')]);
            }
            if (!is_null($request->kit_sent)) {
                $query->where('kit_sent', $request->kit_sent);
            }
            if (!is_null($request->kit_sent_date)) {
                $query->whereBetween('kit_sent_date', [$kit_sent_date_from->format('Y-m-d'), $kit_sent_date_to->format('Y-m-d')]);
            }
            if (!is_null($request->process_completed)) {
                $query->where('process_completed', $request->process_completed);
            }
            if (!is_null($request->interested_in)) {
                $query->where('interested_in', $request->interested_in);
            }
            if (!is_null($request->date_of_birth)) {
                $query->whereBetween('date_of_birth', [$date_of_birth_from->format('Y-m-d'), $date_of_birth_to->format('Y-m-d')]);
            }
            if (!is_null($request->expected_date_of_birth_of_baby)) {
                $query->whereBetween('expected_date_of_birth_of_baby', [$expected_date_of_birth_of_baby_from->format('Y-m-d'), $expected_date_of_birth_of_baby_to->format('Y-m-d')]);
            }
            if (!is_null($request->twin_pregnancy)) {
                $query->where('twin_pregnancy', $request->twin_pregnancy);
            }
            if (!is_null($request->date_of_liposuction)) {
                $query->whereBetween('date_of_liposuction', [$date_of_liposuction_from->format('Y-m-d'), $date_of_liposuction_to->format('Y-m-d')]);
            }
            if (isset($request->id) && !is_null($request->id)) {
                $query->where('creator_id', $request->id)->orWhere('service_provider', $request->id);
            }

            if (auth()->user()->hasRole('COUNSELORS')) {
                $query->where('creator_id', Auth::id());
            }
            $query->where('user_type', 'prospect');
        })->get();
//        $details = Consignment::where(['eta' => $this->request->eta])->get();
        foreach ($details as $prospect) {
            $dataArray[$indexValue] = array(
                [
                    'name' => $prospect->first_name . ' ' . $prospect->last_name ?? '-',
                    'role' => $prospect->user_type ?? '-',
                    'category' => $prospect->category ? $prospect->category->name : '-',
                    'email' => $prospect->email,
                    'phone_number' => $prospect->phone_number ?? '-',
                    'address' => $prospect->address ?? '-',
                    'city' => $prospect->city ?? '-',
                    'country' => $prospect->country ?? '-',
                    'postal_code' => $prospect->postal_code ?? '-',
                    'status' => $prospect->status ?? '-',
                    'interested_in' => str_replace('_', ' ', strtoupper($prospect->interested_in)) ?? '-',
                    'date_of_birth' => $prospect->date_of_birth ? date('Y-m-d', strtotime($prospect->date_of_birth)) : '-',
                    'how_did_it_find_us' => $prospect->how_did_it_find_us ?? '-',
                    'father_name' => $prospect->father_name ?? '-',
                    'expected_date_of_birth_of_baby' => $prospect->expected_date_of_birth_of_baby ? date('Y-m-d', strtotime($prospect->expected_date_of_birth_of_baby)) : '-',
                    'expected_date_clinic' => $prospect->expected_date_clinic ?? '-',
                    'twin_pregnancy' => strtoupper($prospect->twin_pregnancy) ?? '-',
                    'contract_signed' => $prospect->contract_signed ?? '-',
                    'contract_signed_date' => $prospect->contract_signed_date ? date('Y-m-d', strtotime($prospect->contract_signed_date)) : '-',
                    'kit_sent' => $prospect->kit_sent ?? '-',
                    'kit_sent_date' => $prospect->kit_sent_date ? date('Y-m-d', strtotime($prospect->kit_sent_date)) : '-',
                    'process_completed' => $prospect->process_completed ?? '-',
                    'mild_wife' => $prospect->mild_wife ?? '-',
                    'comments' => $prospect->comments ?? '-',
                ]);
            $indexValue++;
        }
        return collect($dataArray);
    }

    public function headings(): array
    {
        return [
            [
                'Name',
                'Role',
                'Category',
                'Email',
                'Phone Number',
                'Address',
                'City',
                'Country',
                'Postal Code',
                'Status',
                'Interested In',
                'Date of Birth',
                'How Did If Find Us',
                'Father Name',
                'Expected Baby Date of Birth',
                'Expected Clinic',
                'Twin pregancy',
                'Contract Signed',
                'Contract Signed Date',
                'Kit Sent',
                'Kit Sent Date',
                'Process Completed',
                'Dr./Midwife',
                'Comments',
            ]
        ];
    }

    public function title(): string
    {
        return "Prospect Report";
    }
}
