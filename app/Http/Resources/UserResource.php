<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        if ((!is_null($this->profile_pic))) {
            $profile_pic = $this->profile_pic;
        } else {
            $profile_pic = 'default.png';
        }
        return [
            "id"                                => $this->id,
            "name"                              => $this->first_name . " " . $this->last_name,
            "first_name"                        => $this->first_name ?? '',
            "last_name"                         => $this->last_name ?? '',
            "email"                             => $this->email ?? '',
            "phone_number"                      => $this->phone_number ?? '',
            "address"                           => $this->address ?? '',
            "user_type"                         => $this->user_type ?? '',
            "status"                            => $this->status ?? '',
            "is_login"                          => $this->is_login ?? '',
            "fcm_token"                         => $this->fcm_token ?? '',
            "role_id"                           => $this->role_id ?? '',
            "role_name"                         => $this->roles ? $this->roles[0]->name : '',
            "category_id"                       => $this->category_id ?? '',
            "category_name"                     => $this->category ? $this->category->name : '',
            "postal_code"                       => $this->postal_code ?? '',
            "language"                          => $this->language ?? '',
            "contact_person"                    => $this->contact_person ?? '',
            "practice"                          => $this->practice ?? '',
            "service_provider"                  => $this->service_provider ?? '',
            "service_provider_name"             => $this->serviceProvider ? $this->serviceProvider->first_name . " ". $this->serviceProvider->last_name : '' ,
            "creator"                           => $this->creator_id ?? '',
            "creator_name"                      => $this->creator ? $this->creator->first_name . " ". $this->creator->last_name : '' ,
            "contract_on"                       => $this->contract_on ?? '',
            "membership"                        => $this->membership ?? '',
            "membership_date"                   => $this->membership_date ? date('Y-m-d',strtotime($this->membership_date)) : '',
            "comments"                          => $this->comments ?? '',
            "company"                           => $this->company ?? '',
            "city"                              => $this->city ?? '',
            "country"                           => $this->country ?? '',
            "delivery"                          => $this->delivery ?? '',
            "delivery_date"                     => $this->delivery_date ? date('Y-m-d',strtotime($this->delivery_date)) : '',
            "posters"                           => $this->posters ?? '',
            "posters_date"                      => $this->posters_date ? date('Y-m-d',strtotime($this->posters_date)) : '',
            "demo_kit"                          => $this->demo_kit ?? '',
            "demo_kit_date"                     => $this->demo_kit_date ? date('Y-m-d',strtotime($this->demo_kit_date)) : '',
            "experience_with_cb"                => $this->experience_with_cb ?? '',
            "experience_with_at"                => $this->experience_with_at ?? '',
            "signed_contract"                   => $this->signed_contract ?? '',
            "offices"                           => $this->offices ?? '',
            "work_field"                        => $this->work_field ?? '',
            "last_login"                        => $this->last_login ? date('Y-m-d',strtotime($this->last_login)) : '',
            "registration_date"                 => $this->registration_date ? date('Y-m-d',strtotime($this->registration_date)) : '',
            "profile_pic"                       => asset("/uploads/user_profiles/" . $profile_pic),
            "interested_in"                     => $this->interested_in ?? '',
            "date_of_birth"                     => $this->date_of_birth ? date('Y-m-d',strtotime($this->date_of_birth)) : '',
            "how_did_it_find_us"                => $this->how_did_it_find_us ?? '',
            "father_name"                       => $this->father_name ?? '',
            "expected_date_of_birth_of_baby"    => $this->expected_date_of_birth_of_baby ? date('Y-m-d',strtotime($this->expected_date_of_birth_of_baby)) : '',
            "expected_date_clinic"              => $this->expected_date_clinic ?? '',
            "twin_pregnancy"                    => $this->twin_pregnancy ?? '',
            "date_of_liposuction"               => $this->date_of_liposuction ? date('Y-m-d',strtotime($this->date_of_liposuction)) : '',
            "doctor"                            => $this->doctor ?? '',
            "clinic"                            => $this->clinic ?? '',
            "area_of_interest"                  => $this->area_of_interest ?? '',
            "willing_to_travel_inside_eu"       => $this->willing_to_travel_inside_eu ?? '',
            "willing_to_travel_outside_eu"      => $this->willing_to_travel_outside_eu ?? '',
            "vacations_included"                => $this->vacations_included ?? '',
            "contract_signed"                   => $this->contract_signed ?? '',
            "mild_wife"                         => $this->mild_wife ?? '',
            "contract_signed_date"              => $this->contract_signed_date ? date('Y-m-d',strtotime($this->contract_signed_date)) : '',
            "kit_sent"                          => $this->kit_sent ?? '',
            "kit_sent_date"                     => $this->kit_sent_date ? date('Y-m-d',strtotime($this->kit_sent_date)) : '',
            "process_completed"                 => $this->process_completed ?? '',
            "created_at"                        => $this->created_at ? date('Y-m-d',strtotime($this->created_at)) : '',
            'sentBy'                            => $this->sentBy,
            'sentByUnRead'                      => $this->sentBy->where('is_read',false)->count(),
            'sentTo'                            => $this->sentTo


        ];
    }
}
