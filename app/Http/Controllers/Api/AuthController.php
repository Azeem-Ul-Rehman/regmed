<?php

namespace App\Http\Controllers\Api;


use App\Http\Resources\UserResource;
use App\Mail\ContactUsEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    public $successStatus = 200;

    public function otp(Request $request)
    {
        $details = ['email' => $request->email];

        $validator = Validator::make($details, [
            'email' => 'required|exists:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {

            $otp_code = null;
            $user = User::where('email', $request->email)->first();

//            if (is_null($user->otp_code)) {
            $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 4);
//            } else {
//                $otp_code = $user->otp_code;
//            }

            $user->update([
                'otp_code' => $otp_code,
            ]);


            $data = [
                'email' => $user->email,
                'code' => $otp_code,
            ];

            $this->verificationEmail($user, $otp_code);


            return response()->json(['status' => true, 'message' => 'OTP code has been sent on given number.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function verificationEmail($user, $otp_code)
    {
        $details = [
            'greeting' => 'Hi ' . $user->first_name . ' ' . $user->last_name,
            'body' => 'Your OTP Code: ' . $otp_code,
            'from' => config('app.admin'),
            'subject' => 'Reset Password'

        ];
        Mail::to($user->email)->send(new ContactUsEmail($details));
    }

    public function verifyOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {


            $user = User::where([
                'email' => $request->get('email'),
                'otp_code' => $request->get('otp_code'),
            ])->first();

            if (!is_null($user)) {

                $user->otp_status = 'verified';
                $user->save();

                return response()->json(['status' => true, 'message' => 'OTP Code verified successfully.'], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'Provided OTP code is not correct.'], 200);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function forgotPasswordRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {

            $user = User::where('email', $request->email)->first();

            if (is_null($user) || is_null($user->email) || empty($user->email)) {
                return response()->json(["status" => false, "message" => 'Your account is not associated with this email address.'], 404);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['status' => true, "message" => 'Your password has been changes successfully.'], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
//            'city_id' => 'required|integer',
            'shop_name' => 'required',
            'phone_number' => 'required|unique:users,phone_number',
//            'gender' => 'required',
            'address' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
            'fcm_token' => 'required'
        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
//            'city_id.required' => 'City is required.',
            'shop_name.required' => 'Shop Name is required.',
//            'gender.required' => 'Gender is required.',
            'phone_number.required' => 'Phone Number is required.',
            'address.required' => 'Address is required.',
            'email.required' => 'Email is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false,'error' => $validator->errors()], 400);
        }

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }

        $role = Role::find(2);
        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
//            'city_id'        => $request->get('city_id'),
            'area_name' => $request->get('shop_name'),
            'role_id' => $role->id,
//            'gender'         => $request->get('gender'),
            'phone_number' => $request->get('phone_number'),
            'address' => $request->get('address'),
            'user_type' => $role->name,
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'profile_pic' => $profile_image,
            'fcm_token' => $request->fcm_token
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);


        $userData = User::where('email', $request->email)->first();
        $token = $user->createToken('AppName')->accessToken;
        $message = 'Thank you for becoming a member of Smart Food Products!';
        return response()->json(['status' => true, 'message' => $message, 'data' => new UserResource($userData), 'token' => $token], 201);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required | exists:users,email',
            'password' => 'required'
        ], [
            'email . required' => 'Email is required',
            'password . required' => 'Password is required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {


            $user = Auth::user();
            if ($user->status == 'suspended') {
                return response()->json(['status' => false, 'message' => 'Your account is suspended . '], 401);
            }
            if ($user->activated == false) {
                return response()->json(['status' => false, 'message' => 'Your Account is not activated yet . Please verify your Account . '], 401);
            }
            $user->last_login = Carbon::now()->toDateTimeString();
            $user->save();

            $success['token'] = $user->createToken('AppName')->accessToken;
            $success['data'] = new UserResource($user);
            return response()->json(['status' => true,'success' => $success], $this->successStatus);
        } else {
            return response()->json(['status' => false,'message' => 'Invalid Password'], 401);
        }
    }

    public function updateFcmToken(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'fcm_token' => 'required'
        ], [
            'fcm_token . required' => 'FCM Token is required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }


        $user = Auth::user();
        $user->fcm_token = $request->fcm_token;
        $user->save();

        return response()->json(['message' => 'FCM Toekn updated successfully', 'data' => new UserResource($user)], 201);


    }

    public function logout()
    {
        try {
            $user = Auth::user();
            $user->fcm_token = null;
            $user->save();

            foreach ($user->tokens as $token) {
                $token->revoke();
                $token->delete();
            }

            return response()->json(['status' => true], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

        return response()->json(null, 204);
    }

    public function getUser()
    {
        $user = new UserResource(auth()->user());
        return response()->json(['status' => true, 'data' => $user], $this->successStatus);
    }


}
