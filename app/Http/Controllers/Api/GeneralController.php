<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Mail\VerifyMail;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Notification;
use App\Models\Role;
use App\Models\Setting;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;

class GeneralController extends Controller
{
    public function roles()
    {
        try {
            return response()->json(['data' => Role::whereNotIn('name', ['SUPER_ADMIN', 'PROSPECT', 'CUSTOMER'])->orderBy('name', 'asc')->get()], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function categories($role_id)
    {
        try {

            $categories = Category::whereHas('roles', function ($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->orderBy('created_at', 'desc')->get();
            return response()->json(['data' => $categories], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function serviceProviders($category_id)
    {
        try {

            $serviceProviders = User::where('category_id', $category_id)->where('user_type', 'service-provider')->orderBy('created_at', 'desc')->get();

            return response()->json(['data' => $serviceProviders], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function settings()
    {
        try {

            return response()->json(['data' => Setting::get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user = Auth::user();
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $image->move($destinationPath, $name);
            $profile_image = $name;


            $user->update([
                'profile_pic' => $profile_image,
            ]);
            return response()->json(['status' => true, 'message' => 'Profile image updated successfully.', 'data' => new UserResource($user)], 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateProfile(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'phone_number' => 'required|unique:users,phone_number,' . $user->id,
            'address' => 'required',

        ], [
            'shop_name.required' => 'Shop Name is required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'address' => $request->address,

            ]);
            return response()->json(['status' => true, 'message' => 'Profile updated successfully.', 'data' => new UserResource($user)], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            if (!(Hash::check($request->get('current-password'), $user->password))) {
                // The passwords matches
                return response()->json(['status' => 'error', 'message' => 'Your current password does not matches with the password you provided. Please try again.'], 200);
            }

            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same
                return response()->json(['status' => 'error', 'message' => 'New Password cannot be same as your current password. Please choose a different password.'], 200);
            }

            if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
                //Change Password
                $user->password = bcrypt($request->get('new-password'));
                $user->is_login = true;
                $user->save();

                return response()->json(['status' => 'success', 'message' => 'Password Changed successfully.'], 200);

            } else {
                return response()->json(['status' => 'error', 'message' => 'New Password must be same as your confirm password.'], 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }


    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'role_id' => 'required|integer',
            'category_id' => 'required|integer',
            'phone_number' => 'required',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required_if:type,others',
            'password_confirmation' => 'required_if:type,others',
            'country' => 'required',
//            'company' => 'required',
            'city' => 'required',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'role_id.required' => 'Role is required.',
            'category_id.required' => 'Category is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if (!is_null($request->attached_files) && isset($request->attached_files) && $request->has('attached_files')) {
            $image = $request->file('attached_files');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/attachedFiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $attached_files = $name;
        } else {

            $attached_files = null;
        }

        $role = Role::find((int)$request->get('role_id'));
        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->role_id = $request->role_id;
        $user->user_type = str_replace('_', '-', strtolower($role->name));
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        $user->country = $request->country;
        $user->company = $request->company;
        $user->city = $request->city;
        $user->category_id = $request->category_id;
        $user->status = $request->status;
        $user->email = $request->email;

        $user->postal_code = $request->postal_code;
        $user->language = $request->language;
        $user->contact_person = $request->contact_person;

        $user->service_provider = $request->service_provider;
        $user->creator_id = Auth::id();
        $user->comments = $request->comments;


        $user->delivery = $request->delivery ?? 'no';
        $user->mild_wife = $request->mild_wife ?? 'no';
        $user->delivery_date = $request->delivery_date ?? null;
        $user->posters = $request->posters ?? 'no';
        $user->posters_date = $request->posters_date ?? null;
        $user->demo_kit = $request->demo_kit ?? 'no';
        $user->demo_kit_date = $request->demo_kit_date ?? null;

        $user->experience_with_cb = $request->experience_with_cb ?? null;
        $user->experience_with_at = $request->experience_with_at ?? null;
        $user->signed_contract = $request->signed_contract ?? null;
        $user->offices = $request->offices ?? null;
        $user->work_field = $request->work_field ?? null;
        $user->membership = $request->membership ?? 'no';
        $user->membership_date = $request->membership_date ?? null;


        //Prospects fields additional
        $user->mother_name = $request->mother_name;
        $user->estimated_due_date = $request->estimated_due_date;
        $user->practice = $request->practice;
        $user->contract_on = $request->contract_on;

        $user->interested_in = $request->interested_in ?? 'cb_ct';
        $user->date_of_birth = $request->date_of_birth;
        $user->how_did_it_find_us = $request->how_did_it_find_us;
        $user->father_name = $request->father_name;
        $user->expected_date_of_birth_of_baby = $request->expected_date_of_birth_of_baby;
        $user->expected_date_clinic = $request->expected_date_clinic;
        $user->twin_pregnancy = $request->twin_pregnancy ?? 'no';
        $user->date_of_liposuction = $request->date_of_liposuction;
        $user->doctor = $request->doctor;
        $user->clinic = $request->clinic;
        $user->area_of_interest = $request->area_of_interest;
        $user->willing_to_travel_inside_eu = $request->willing_to_travel_inside_eu;
        $user->willing_to_travel_outside_eu = $request->willing_to_travel_outside_eu;
        $user->vacations_included = $request->vacations_included;

        $user->signed_agreement_sent = $request->signed_agreement_sent;
        $user->agreement_signed_on = $request->agreement_signed_on;
        $user->attached_files = $attached_files ?? null;


        $user->contract_signed = $request->contract_signed ?? 'no';
        $user->contract_signed_date = $request->contract_signed_date ?? null;
        $user->kit_sent = $request->kit_sent ?? 'no';
        $user->kit_sent_date = $request->kit_sent_date ?? null;
        $user->process_completed = $request->process_completed ?? 'no';

        if (isset($request->password) && !is_null($request->password) && $request->type == 'others') {
            $user->password = Hash::make($request->get('password'));
        }
        $user->save();


        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        $this->confirmationEmail($user);
        $message = 'User has been Created Successfully.';
        return response()->json(['message' => $message, 'data' => new UserResource($user), 'status' => true], 201);
    }

    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone_number' => 'required',
            'country' => 'required',
//            'company' => 'required',
            'city' => 'required',
            'address' => 'required',
            'status' => 'required',
            'id' => 'required',
        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'address.required' => 'Address is required.',
            'phone_number.required' => 'Phone Number is required.',
            'status.required' => 'Status is required.',
            'id.required' => 'Use Id is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $user = User::find($request->id);

        if (!is_null($request->attached_files) && isset($request->attached_files) && $request->has('attached_files')) {
            $image = $request->file('attached_files');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/attachedFiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $attached_files = $name;
        } else {

            $attached_files = $user->attached_files;
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        $user->company = $request->company;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->status = $request->status;
        $user->postal_code = $request->postal_code;
        $user->language = $request->language;
        $user->contact_person = $request->contact_person;

        $user->service_provider = $request->service_provider;
        $user->comments = $request->comments;


        $user->delivery = $request->delivery ?? 'no';
        $user->delivery_date = $request->delivery_date ?? null;
        $user->posters = $request->posters ?? 'no';
        $user->posters_date = $request->posters_date ?? null;
        $user->demo_kit = $request->demo_kit ?? 'no';
        $user->demo_kit_date = $request->demo_kit_date ?? null;
        $user->mild_wife = $request->mild_wife ?? null;
        $user->experience_with_cb = $request->experience_with_cb ?? null;
        $user->experience_with_at = $request->experience_with_at ?? null;
        $user->signed_contract = $request->signed_contract ?? null;
        $user->offices = $request->offices ?? null;
        $user->work_field = $request->work_field ?? null;
        $user->membership = $request->membership ?? 'no';
        $user->membership_date = $request->membership_date ?? null;


        //Prospects fields additional
        $user->mother_name = $request->mother_name;
        $user->estimated_due_date = $request->estimated_due_date;
        $user->practice = $request->practice;
        $user->contract_on = $request->contract_on;

        $user->interested_in = $request->interested_in ?? 'cb_ct';
        $user->date_of_birth = $request->date_of_birth;
        $user->how_did_it_find_us = $request->how_did_it_find_us;
        $user->father_name = $request->father_name;
        $user->expected_date_of_birth_of_baby = $request->expected_date_of_birth_of_baby;
        $user->expected_date_clinic = $request->expected_date_clinic;
        $user->twin_pregnancy = $request->twin_pregnancy ?? 'no';
        $user->date_of_liposuction = $request->date_of_liposuction;
        $user->doctor = $request->doctor;
        $user->clinic = $request->clinic;
        $user->area_of_interest = $request->area_of_interest;
        $user->willing_to_travel_inside_eu = $request->willing_to_travel_inside_eu;
        $user->willing_to_travel_outside_eu = $request->willing_to_travel_outside_eu;
        $user->vacations_included = $request->vacations_included;

        $user->signed_agreement_sent = $request->signed_agreement_sent;
        $user->agreement_signed_on = $request->agreement_signed_on;
        $user->attached_files = $attached_files ?? null;

        $user->contract_signed = $request->contract_signed ?? 'no';
        $user->contract_signed_date = $request->contract_signed_date ?? null;
        $user->kit_sent = $request->kit_sent ?? 'no';
        $user->kit_sent_date = $request->kit_sent_date ?? null;
        $user->process_completed = $request->process_completed ?? 'no';

        if (isset($request->password) && !is_null($request->password) && $request->type == 'others') {
            $user->password = Hash::make($request->get('password'));
        }
        $user->save();
        $message = 'User has been Updated Successfully';
        return response()->json(['message' => $message, 'data' => UserResource::collection(User::whereNotIn('user_type', ['super-admin', 'prospect', 'customer'])->orderBy('created_at', 'desc')->get())], 201);
    }

    public function deleteUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|string',

        ], [
            'id.required' => 'User Id is required.',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $user = User::findOrFail($request->id);
        $user->delete();

        $message = 'User has been Deleted Successfully';
        return response()->json(['message' => $message, 'data' => UserResource::collection(User::whereNotIn('user_type', ['super-admin', 'prospect', 'customer'])->orderBy('created_at', 'desc')->get())], 201);
    }

    public function getAllUsers()
    {
        try {
            $getUsers = UserResource::collection(User::whereNotIn('user_type', ['super-admin', 'prospect', 'customer'])->orderBy('created_at', 'desc')->get());
            return response()->json(['data' => $getUsers], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function getAllProspects()
    {
        try {
            $getUsers = UserResource::collection(User::whereIn('user_type', ['prospect', 'customer'])->orderBy('created_at', 'desc')->get());
            return response()->json(['data' => $getUsers], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function confirmationEmail($user)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $activation = $user->activation()->create([
            'token' => $randomString
        ]);
        Mail::to($user->email)->send(new VerifyMail($user, $randomString));
    }

    public function getNotificationByUser($user_id)
    {
        try {
            $getUsers = Notification::where('sent_to', $user_id)->orWhere('sent_by', $user_id)->with('sentByUser', 'sentToUser')->orderBy('created_at', 'desc')->get();
            return response()->json(['data' => $getUsers], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function sendNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
            'sent_to' => 'required',

        ], [
            'message.required' => 'Message is required.',
            'sent_to.required' => 'Sent To is required.',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $notification = Notification::create([
            'sent_by' => Auth::id(),
            'sent_to' => $request->get('sent_to'),
            'message' => $request->get('message'),
        ]);
        $getUsers = Notification::where('sent_to', $request->get('sent_to'))->orWhere('sent_by', $request->get('sent_to'))->with('sentByUser', 'sentToUser')->orderBy('created_at', 'desc')->get();

        $message = 'Notificaton Sent Successfully';
        return response()->json(['message' => $message, 'status' => true, 'data' => $getUsers], 201);
    }

    public function isReadNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'notification_id' => 'required',
        ], [
            'notification_id.required' => 'Notification Id is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $notification = Notification::find($request->notification_id);
        $notification->is_read = true;
        $notification->save();
        $message = 'Notificaton Read Successfully';
        return response()->json(['message' => $message, 'status' => true, 'data' => $notification], 201);
    }

    public function getAllUsersIfNotification()
    {
        try {
            $getUsers = UserResource::collection(User::whereNotIn('user_type', ['super-admin', 'admin', 'prospect', 'customer'])->orderBy('created_at', 'desc')->get());
            $users = [];
            if (!empty($getUsers) && count($getUsers) > 0) {
                foreach ($getUsers as $user) {
                    if ((!empty($user->sentBy) && count($user->sentBy) > 0) || (!empty($user->sentTo) && count($user->sentTo) > 0)) {
                        array_push($users, $user);
                    }
                }
            }
            return response()->json(['data' => $users], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function getLatestNotifications()
    {
        try {

            $user = Auth::user();
            if ($user->user_type == 'admin') {
                $getUsers = Notification::where('is_read', false)->where('sent_from', '!=', $user->id)->with('sentByUser', 'sentToUser')->orderBy('created_at', 'desc')->get();
            } else {
                $getUsers = Notification::where('is_read', false)->where('sent_to', $user->id)->with('sentByUser', 'sentToUser')->orderBy('created_at', 'desc')->get();
            }


            return response()->json(['data' => $getUsers], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    //All counselors of service Provider

    public function getAllCounselorsOfServiceProvider()
    {
        $user_id = Auth::id();
        try {
            $getUsers = UserResource::collection(User::where('service_provider', $user_id)->where('user_type', 'counselors')->orderBy('created_at', 'desc')->get());
            return response()->json(['data' => $getUsers], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    //All prospects of service Provider

    public function getAllProspectsOfServiceProvider()
    {
        $user_id = Auth::id();
        try {
            $getUsers = UserResource::collection(User::where('creator_id', $user_id)->orWhere('service_provider',Auth::id())->where('user_type', 'prospect')->orderBy('created_at', 'desc')->get());
            return response()->json(['data' => $getUsers], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    //All prospects of Counselors

    public function getAllProspectsOfCounselors()
    {
        $user_id = Auth::id();
        try {
            $getUsers = UserResource::collection(User::where('creator_id', $user_id)->where('user_type', 'prospect')->orderBy('created_at', 'desc')->get());
            return response()->json(['status' => true, 'data' => $getUsers], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function serviceProvidersWithoutCategory()
    {
        try {

            $serviceProviders = User::where('user_type', 'service-provider')->orderBy('created_at', 'desc')->get();

            return response()->json(['data' => $serviceProviders], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function contactUs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
        ], [
            'message.required' => 'Message is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $user = Auth::user();
        $contactUs = ContactUs::create([
            'user_id' => $user->id,
            'full_name' => $user->fullName(),
            'email' => $user->email,
            'message' => $request->get('message'),
        ]);
        $message = 'Message Sent Successfully';
        return response()->json(['message' => $message, 'status' => true], 201);
    }


}
