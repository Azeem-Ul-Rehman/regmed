<?php

namespace App\Http\Controllers\Backend;

use App\Exports\ProspectExport;
use App\Exports\ProspectExportAll;
use App\Exports\ProspectExportAt;
use App\Exports\ProspectExportOthers;
use App\Exports\ProspectExportTreatment;
use App\Exports\UsersExportAll;
use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use App\Models\Category;
use App\Models\City;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;

class UserController extends Controller
{

    public function index(Request $request)
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {


            if (!empty($request->all())) {


                $registration_date_from = $request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->registration_date, '0', '10'));
                $registration_date_to = $request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->registration_date, '13'));


                $users = User::where(function ($query) use ($request, $registration_date_from, $registration_date_to) {

                    if (!is_null($request->category_id)) {

                        if ($request->category_id == 'all') {
                            $query->whereIn('category_id', ['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18']);
                        } else {
                            $query->where('category_id', $request->category_id);
                        }
                    }
                    if (!is_null($request->creater_id)) {
                        $query->where('creator_id', $request->creetor_id);
                    }
                    if (!is_null($request->city)) {
                        $query->where('city', $request->city);
                    }
                    if (!is_null($request->country)) {
                        $query->where('country', $request->country);
                    }
                    if (!is_null($request->registration_date)) {
                        $query->whereBetween('registration_date', [$registration_date_from->format('Y-m-d'), $registration_date_to->format('Y-m-d')]);
                    }


                    $query->whereNotIn('user_type', ['super-admin', 'prospect', 'customer']);
                })->get();

            } else {
                if (auth()->user()->hasRole('SUPER_ADMIN')) {
                    $users = User::orderBy('id', 'DESC')->whereNotIn('user_type', ['super-admin', 'prospect', 'customer'])->get();
                } else {
                    $users = User::orderBy('id', 'DESC')->whereNotIn('user_type', ['super-admin', 'prospect', 'customer'])->where('id', '!=', Auth::id())->get();
                }
            }
            $cities = User::orderBy('id', 'DESC')->where('user_type', 'prospect')->groupBy('city')->pluck('city')->toArray();
            $categories = Category::whereHas('roles', function ($q) {
                $q->where('role_id', '!=', '6');
            })->get();

            $creaters = User::whereIn('user_type', ['prospect'])->groupBy('creator_id')->get();
            $countries = User::orderBy('id', 'DESC')->where('user_type', 'prospect')->groupBy('country')->pluck('country')->toArray();


            return view('backend.users.index', compact('users', 'countries', 'cities', 'creaters', 'categories'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function create()
    {
        $serviceProviders = User::where('user_type', 'service-provider')->get();


        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {


            $roles = Role::whereNotIn('name', ['SUPER_ADMIN', 'PROSPECT', 'CUSTOMER'])->orderBy('name', 'asc')->get();
            return view('backend.users.create', compact('roles', 'serviceProviders'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'role_id' => 'required|integer',
            'category_id' => 'required|integer',
            'phone_number' => 'required',
            'country' => 'required',
            'company' => 'required',
            'city' => 'required',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'role_id.required' => 'Role is required.',
            'category_id.required' => 'Category is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {

            $profile_image = 'default.png';
        }

        $role = Role::find((int)$request->get('role_id'));


        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'role_id' => $request->get('role_id'),
            'country' => $request->get('country'),
            'company' => $request->get('company'),
            'city' => $request->get('city'),
            'phone_number' => $request->get('phone_number'),
            'address' => $request->get('address'),
            'user_type' => str_replace('_', '-', strtolower($role->name)),
            'category_id' => $request->get('category_id'),
            'status' => $request->get('status'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'postal_code' => $request->get('postal_code'),
            'language' => $request->get('language'),
            'contact_person' => $request->get('contact_person'),
            'practice' => $request->get('practice'),
            'service_provider' => $request->get('service_provider'),
            'creator_id' => Auth::id(),
            'contract_on' => $request->get('contract_on'),

            'delivery' => $request->get('delivery') ?? 'no',
            'delivery_date' => $request->get('delivery_date') ?? null,
            'posters' => $request->get('posters') ?? 'no',
            'posters_date' => $request->get('posters_date') ?? null,
            'demo_kit' => $request->get('demo_kit') ?? 'no',
            'demo_kit_date' => $request->get('demo_kit_date') ?? null,
            'comments' => $request->get('comments') ?? null,
            'experience_with_cb' => $request->get('experience_with_cb') ?? null,
            'experience_with_at' => $request->get('experience_with_at') ?? null,
            'signed_contract' => $request->get('signed_contract') ?? null,
            'offices' => $request->get('offices') ?? null,
            'work_field' => $request->get('work_field') ?? null,
            'membership' => $request->get('membership') ?? 'no',
            'membership_date' => $request->get('membership_date') ?? null,


        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        $this->confirmationEmail($user);

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User created successfully.'
            ]);
    }

    public function show($id)
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $user = User::find($id);

            $roles = Role::whereNotIn('name', ['SUPER_ADMIN', 'PROSPECT', 'CUSTOMER'])->orderBy('name', 'asc')->get();
            $categories = Category::whereHas('roles', function ($q) use ($user) {
                $q->where('role_id', $user->role_id);
            })->get();
            $serviceProviders = User::where('user_type', 'service-provider')->get();
            return view('backend.users.show', compact('user', 'roles', 'categories', 'serviceProviders'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }


    }

    public function edit($id)
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $user = User::find($id);

            $roles = Role::whereNotIn('name', ['SUPER_ADMIN', 'PROSPECT', 'CUSTOMER'])->orderBy('name', 'asc')->get();
            $categories = Category::whereHas('roles', function ($q) use ($user) {
                $q->where('role_id', $user->role_id);
            })->get();
            $serviceProviders = User::where('user_type', 'service-provider')->get();
            return view('backend.users.edit', compact('user', 'roles', 'categories', 'serviceProviders'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }


    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone_number' => 'required',
            'country' => 'required',
            'company' => 'required',
            'city' => 'required',
            'address' => 'required',
            'status' => 'required',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'address.required' => 'Address  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'status.required' => 'Status  is required.',
        ]);

        $user = User::find($id);

        $role = Role::find((int)$request->get('role_id'));


        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->country = $request->country;
        $user->company = $request->company;
        $user->city = $request->city;
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        $user->status = $request->status;
        $user->postal_code = $request->postal_code;
        $user->language = $request->language;

        $user->contact_person = $request->contact_person;
        $user->practice = $request->practice;
        $user->service_provider = $request->service_provider;
        $user->role_id = $request->role_id;
        $user->category_id = $request->category_id;
        $user->service_provider = $request->service_provider;
        $user->contract_on = $request->contract_on;


        $user->delivery = $request->delivery ?? 'no';
        $user->delivery_date = $request->delivery_date ?? null;
        $user->posters = $request->posters ?? 'no';
        $user->posters_date = $request->posters_date ?? null;
        $user->demo_kit = $request->demo_kit ?? 'no';
        $user->demo_kit_date = $request->demo_kit_date ?? null;
        $user->experience_with_cb = $request->experience_with_cb ?? null;
        $user->experience_with_at = $request->experience_with_at ?? null;
        $user->signed_contract = $request->signed_contract ?? null;
        $user->offices = $request->offices ?? null;
        $user->work_field = $request->work_field ?? null;
        $user->membership = $request->membership ?? 'no';
        $user->membership_date = $request->membership_date ?? null;


        $user->comments = $request->comments;
        if (isset($request->password) && !is_null($request->password)) {
            $user->password = Hash::make($request->get('password'));
        }
        $user->save();

        $userRole = UserRole::where('user_id', $user->id)->first();
        $userRole->role_id = $role->id;
        $userRole->save();


        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => trans('message.user_update')
            ]);
    }

    public function destroy($id)
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $user = User::findOrFail($id);
            $user->sentBy()->delete();
            $user->sentTo()->delete();
            $user->delete();

            return redirect()->route('admin.users.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'User has been deleted'
                ]);
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function confirmationEmail($user)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $activation = $user->activation()->create([
            'token' => $randomString
        ]);
        Mail::to($user->email)->send(new VerifyMail($user, $randomString));
    }

    public function export(Request $request)
    {

        $this->validate($request, [
            'category_id' => 'required',
        ], [
            'category_id.required' => 'Category is required.',
        ]);
        if (!is_null($request->category_id)) {

            return Excel::download(new UsersExportAll($request), 'users.xlsx');

        } else {
            return redirect()->back('admin.users.index')
                ->with([
                    'flash_status' => 'warning',
                    'flash_message' => 'Category is Required.'
                ]);
        }

    }
}
