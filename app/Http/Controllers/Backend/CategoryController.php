<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CategoryController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $categories = Category::orderBy('id', 'DESC')->get();

            return view('backend.categories.index', compact('categories'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function create()
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $roles = Role::where('name', '!=', 'SUPER_ADMIN')->get();
            return view('backend.categories.create', compact('roles'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }

    }

    public function show($id)
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
        ], [
            'name.required' => 'Name is required',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        $category->roles()->sync($request->roles);
        return redirect()->route('admin.categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Category created successfully.'
            ]);

    }

    public function edit($id)
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $category = Category::find($id);
            $roles = Role::where('name', '!=', 'SUPER_ADMIN')->get();
            return view('backend.categories.edit', compact('category', 'roles'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function update(Request $request, $id)
    {


        $this->validate($request, [
            'name' => 'required|string',
        ], [
            'name.required' => 'Name is required',
        ]);
        $category = Category::find($id);
        $category->name = $request->name;
        $category->save();
        $category->roles()->sync($request->roles);

        return redirect()->route('admin.categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Category updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $category = Category::findOrFail($id);
            $category->delete();

            return redirect()->route('admin.categories.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Category has been deleted'
                ]);
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }
}
