<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function index()
    {
        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $users_count = User::whereNotIn('user_type', ['super-admin', 'prospect'])->count();
            $users_count_prospects = User::where('user_type', 'prospect')->count();
            return view('backend.dashboard.index', compact('users_count', 'users_count_prospects'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }
}
