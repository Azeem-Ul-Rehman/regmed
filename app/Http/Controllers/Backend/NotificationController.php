<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mockery\Matcher\Not;


class NotificationController extends Controller
{
    public function index(Request $request)
    {


        $users = User::whereNotIn('user_type', ['super-admin', 'prospect', 'customer'])->where('id', '!=', Auth::id())->get();
        if (isset($request->sent_to) && !is_null($request->sent_to)) {
            $notifications = Notification::where('sent_to', $request->sent_to)->get();

        } else {
            $notifications = Notification::all();
        }

        return view('backend.notification.index', compact('notifications', 'users'));
    }

    public function create()
    {
        $users = User::whereNotIn('user_type', ['super-admin', 'prospect', 'customer'])->where('id', '!=', Auth::id())->get();
        return view('backend.notification.create', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'message' => 'required|string',
        ], [
            'user_id.required' => 'User  is required.',
            'message.required' => 'Message  is required.',
        ]);


        $notification = Notification::create([
            'sent_by' => Auth::id(),
            'sent_to' => $request->get('user_id'),
            'message' => $request->get('message'),
        ]);

        return redirect()->route('admin.notifications.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Notification send successfully.'
            ]);

    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->delete();

        return redirect()->route('admin.notifications.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Notification has been deleted'
            ]);
    }
}
