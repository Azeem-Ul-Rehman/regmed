<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{
    public function roleCategories(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'role_id' => 'required|exists:roles,id'
        ], [
            'role_id.required' => "Role is Required.",
            'role_id.exists' => "Invalid Role Selected."
        ]);

        if (!$validator->fails()) {
            $categories = Category::whereHas('roles', function ($q) use ($request) {
                $q->where('role_id', $request->role_id);
            })->get();

            $response['status'] = 'success';
            $response['data'] = [
                'categories' => $categories,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function categoryServiceProvider(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'category_id' => 'required'
        ], [
            'category_id.required' => "Category is Required.",
        ]);

        if (!$validator->fails()) {
            $serviceProviders = User::where('category_id', $request->category_id)->where('user_type', 'service-provider')->get();

            $response['status'] = 'success';
            $response['data'] = [
                'serviceProviders' => $serviceProviders,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function clickToReadNotification(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ], [
            'id.required' => "Id is Required.",
        ]);

        if (!$validator->fails()) {
            $notification = Notification::where('id', $request->id)->first();

            if ($request->is_read == 0) {
                $notification->is_read = true;
            } else {
                $notification->is_read = false;
            }
            $notification->save();

            $response['status'] = 'success';
            $response['data'] = [
                'notification' => $notification,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

}
