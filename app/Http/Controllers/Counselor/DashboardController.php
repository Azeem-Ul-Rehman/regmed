<?php

namespace App\Http\Controllers\Counselor;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function index()
    {
        if (auth()->user()->hasRole('COUNSELORS')) {
            $users_count_prospects = User::where('user_type', 'prospect')->where('creator_id', Auth::id())->count();
            return view('counselor.dashboard.index', compact('users_count_prospects'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }
}
