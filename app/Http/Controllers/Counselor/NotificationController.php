<?php

namespace App\Http\Controllers\Counselor;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where('user_type', 'admin')->where('id', '!=', Auth::id())->get();
        if (isset($request->sent_to) && !is_null($request->sent_to)) {
            $notifications = Notification::where('sent_to', $request->sent_to)->where('sent_by', Auth::id())->get();
        } else {
            $notifications = Notification::where('sent_to', Auth::id())->orWhere('sent_by', Auth::id())->get();
        }

        return view('counselor.notification.index', compact('notifications', 'users'));
    }

    public function create()
    {
        $users = User::where('user_type', 'admin')->where('id', '!=', Auth::id())->get();
        return view('counselor.notification.create', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'message' => 'required|string',
        ], [
            'user_id.required' => 'User  is required.',
            'message.required' => 'Message  is required.',
        ]);


        $notification = Notification::create([
            'sent_by' => Auth::id(),
            'sent_to' => $request->get('user_id'),
            'message' => $request->get('message'),
        ]);

        return redirect()->route('counselor.notifications.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Notification send successfully.'
            ]);

    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->delete();

        return redirect()->route('counselor.notifications.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Notification has been deleted'
            ]);
    }
}
