<?php

namespace App\Http\Controllers\Counselor;

use App\Exports\ProspectExport;
use App\Exports\ProspectExportAll;
use App\Exports\ProspectExportAt;
use App\Exports\ProspectExportOthers;
use App\Exports\ProspectExportTreatment;
use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use App\Models\Category;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;

class ProspectController extends Controller
{

    public function index(Request $request)
    {
        if (auth()->user()->hasRole('COUNSELORS')) {
            if (!empty($request->all())) {


                $contract_date_from = $request->contract_signed_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->contract_signed_date, '0', '10'));
                $contract_date_to = $request->contract_signed_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->contract_signed_date, '13'));


                $registration_date_from = $request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->registration_date, '0', '10'));
                $registration_date_to = $request->registration_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->registration_date, '13'));

                $kit_sent_date_from = $request->kit_sent_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->kit_sent_date, '0', '10'));
                $kit_sent_date_to = $request->kit_sent_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->kit_sent_date, '13'));


                $date_of_birth_from = $request->date_of_birth == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->date_of_birth, '0', '10'));
                $date_of_birth_to = $request->date_of_birth == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->date_of_birth, '13'));

                $expected_date_of_birth_of_baby_from = $request->expected_date_of_birth_of_baby == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->expected_date_of_birth_of_baby, '0', '10'));
                $expected_date_of_birth_of_baby_to = $request->expected_date_of_birth_of_baby == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->expected_date_of_birth_of_baby, '13'));

                $date_of_liposuction_from = $request->date_of_liposuction == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->date_of_liposuction, '0', '10'));
                $date_of_liposuction_to = $request->date_of_liposuction == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->date_of_liposuction, '13'));

                $prospects = User::where(function ($query) use ($request, $date_of_liposuction_from, $date_of_liposuction_to, $expected_date_of_birth_of_baby_from, $expected_date_of_birth_of_baby_to, $date_of_birth_from, $date_of_birth_to, $contract_date_from, $contract_date_to, $registration_date_from, $registration_date_to, $kit_sent_date_from, $kit_sent_date_to) {


                    if (!is_null($request->category_id)) {

                        if ($request->category_id == 'all') {
                            $query->whereIn('category_id', ['3', '4', '5', '19']);
                        } else {
                            $query->where('category_id', $request->category_id);
                        }

                    }
                    if (!is_null($request->creater_id)) {
                        $query->where('creator_id', $request->creetor_id);
                    }
                    if (!is_null($request->city)) {
                        $query->where('city', $request->city);
                    }
                    if (!is_null($request->country)) {
                        $query->where('country', $request->country);
                    }
                    if (!is_null($request->contract_signed)) {
                        $query->where('contract_signed', $request->contract_signed);
                    }
                    if (!is_null($request->contract_signed_date)) {
                        $query->whereBetween('contract_signed_date', [$contract_date_from->format('Y-m-d'), $contract_date_to->format('Y-m-d')]);
                    }
                    if (!is_null($request->registration_date)) {
                        $query->whereBetween('registration_date', [$registration_date_from->format('Y-m-d'), $registration_date_to->format('Y-m-d')]);
                    }
                    if (!is_null($request->kit_sent)) {
                        $query->where('kit_sent', $request->kit_sent);
                    }
                    if (!is_null($request->kit_sent_date)) {
                        $query->whereBetween('kit_sent_date', [$kit_sent_date_from->format('Y-m-d'), $kit_sent_date_to->format('Y-m-d')]);
                    }
                    if (!is_null($request->process_completed)) {
                        $query->where('process_completed', $request->process_completed);
                    }
                    if (!is_null($request->interested_in)) {
                        $query->where('interested_in', $request->interested_in);
                    }
                    if (!is_null($request->date_of_birth)) {
                        $query->whereBetween('date_of_birth', [$date_of_birth_from->format('Y-m-d'), $date_of_birth_to->format('Y-m-d')]);
                    }
                    if (!is_null($request->expected_date_of_birth_of_baby)) {
                        $query->whereBetween('expected_date_of_birth_of_baby', [$expected_date_of_birth_of_baby_from->format('Y-m-d'), $expected_date_of_birth_of_baby_to->format('Y-m-d')]);
                    }
                    if (!is_null($request->twin_pregnancy)) {
                        $query->where('twin_pregnancy', $request->twin_pregnancy);
                    }
                    if (!is_null($request->date_of_liposuction)) {
                        $query->whereBetween('date_of_liposuction', [$date_of_liposuction_from->format('Y-m-d'), $date_of_liposuction_to->format('Y-m-d')]);
                    }
                    $query->where('user_type', 'prospect')->where('creator_id', Auth::id());
                })->get();


            } else {
                if (auth()->user()->hasRole('COUNSELORS')) {
                    $prospects = User::orderBy('id', 'DESC')->whereIn('user_type', ['prospect'])->where('creator_id', Auth::id())->get();
                }
            }
            $cities = User::orderBy('id', 'DESC')->where('user_type', 'prospect')->groupBy('city')->pluck('city')->toArray();
            $categories = Category::whereHas('roles', function ($q) {
                $q->where('role_id', '6');
            })->get();

            $creaters = User::whereIn('user_type', ['prospect'])->groupBy('creator_id')->get();
            $countries = User::orderBy('id', 'DESC')->where('user_type', 'prospect')->groupBy('country')->pluck('country')->toArray();

            return view('counselor.prospects.index', compact('prospects', 'cities', 'categories', 'creaters', 'countries'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function create()
    {
        if (auth()->user()->hasRole('COUNSELORS')) {
            $roles = Role::whereIn('name', ['PROSPECT'])->orderBy('name', 'asc')->get();
            $serviceProviders = User::where('user_type', 'service-provider')->get();
            return view('counselor.prospects.create', compact('roles', 'serviceProviders'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'role_id' => 'required|integer',
            'category_id' => 'required|integer',
            'phone_number' => 'required|unique:users,phone_number',
            'address' => 'required',
//            'status' => 'required',
            'country' => 'required',
            'city' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'role_id.required' => 'Role is required.',
            'category_id.required' => 'Category is required.',
            'phone_number.required' => 'Phone Number  is required.',
//            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);


        if (!is_null($request->attached_files) && isset($request->attached_files) && $request->has('attached_files')) {
            $image = $request->file('attached_files');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/attachedFiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $attached_files = $name;
        } else {

            $attached_files = null;
        }

        $role = Role::find((int)$request->get('role_id'));


        $prospect = User::create([
            'interested_in' => $request->get('interested_in') ?? 'cb_ct',
            'date_of_birth' => $request->get('date_of_birth') ?? null,
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'address' => $request->get('address'),
            'postal_code' => $request->get('postal_code'),
            'country' => $request->get('country'),
            'city' => $request->get('city'),
            'language' => $request->get('language'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number'),
            'how_did_it_find_us' => $request->get('how_did_it_find_us'),
            'creator_id' => Auth::id(),
            'role_id' => $request->get('role_id'),
            'category_id' => $request->get('category_id'),
            'service_provider' => $request->get('service_provider'),
            'user_type' => 'prospect',
            'status' => 'pending',

            'father_name' => $request->get('father_name') ?? null,
            'expected_date_of_birth_of_baby' => $request->get('expected_date_of_birth_of_baby') ?? null,
            'expected_date_clinic' => $request->get('expected_date_clinic') ?? null,
            'twin_pregnancy' => $request->get('twin_pregnancy') ?? 'no',
            'date_of_liposuction' => $request->get('date_of_liposuction') ?? null,
            'doctor' => $request->get('doctor') ?? null,
            'clinic' => $request->get('clinic') ?? null,
            'area_of_interest' => $request->get('area_of_interest') ?? null,
            'willing_to_travel_inside_eu' => $request->get('willing_to_travel_inside_eu') ?? null,
            'willing_to_travel_outside_eu' => $request->get('willing_to_travel_outside_eu') ?? null,
            'vacations_included' => $request->get('vacations_included') ?? null,
            'comments' => $request->get('comments') ?? null,

            'mother_name' => $request->get('mother_name') ?? null,
            'estimated_due_date' => $request->get('estimated_due_date') ?? null,
            'agreement_signed_on' => $request->get('agreement_signed_on') ?? null,
            'signed_agreement_sent' => $request->get('signed_agreement_sent') ?? null,
            'attached_files' => $attached_files ?? null,

            'contract_signed' => $request->get('contract_signed') ?? null,
            'mild_wife' => $request->get('mild_wife') ?? null,
            'contract_signed_date' => $request->get('contract_signed_date') ?? null,
            'kit_sent' => $request->get('kit_sent') ?? null,
            'kit_sent_date' => $request->get('kit_sent_date') ?? null,
            'process_completed' => $request->get('process_completed') ?? null,


        ]);

        UserRole::create([
            'user_id' => $prospect->id,
            'role_id' => $role->id
        ]);

        $this->confirmationEmail($prospect);

        return redirect()->route('counselor.prospects.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Prospect created successfully.'
            ]);
    }

    public function show($id)
    {
        if (auth()->user()->hasRole('COUNSELORS')) {
            $prospect = User::find($id);
            $roles = Role::whereIn('name', ['PROSPECT'])->orderBy('name', 'asc')->get();
            $categories = Category::whereHas('roles', function ($q) use ($prospect) {
                $q->where('role_id', $prospect->role_id);
            })->get();
            $serviceProviders = User::where('user_type', 'service-provider')->get();
            return view('counselor.prospects.show', compact('prospect', 'roles', 'categories', 'serviceProviders'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }


    }

    public function edit($id)
    {
        if (auth()->user()->hasRole('COUNSELORS')) {
            $prospect = User::find($id);
            $roles = Role::whereIn('name', ['PROSPECT'])->orderBy('name', 'asc')->get();
            $categories = Category::whereHas('roles', function ($q) use ($prospect) {
                $q->where('role_id', $prospect->role_id);
            })->get();
            return view('counselor.prospects.edit', compact('prospect', 'roles', 'categories'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone_number' => 'required',
            'address' => 'required',
            'status' => 'required',
            'country' => 'required',
            'city' => 'required',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'address.required' => 'Address  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'status.required' => 'Status  is required.',
        ]);

        $prospect = User::find($id);


        if (!is_null($request->attached_files) && isset($request->attached_files) && $request->has('attached_files')) {
            $image = $request->file('attached_files');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/attachedFiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $attached_files = $name;
        } else {

            $attached_files = $prospect->attached_files;
        }


        $prospect->update([
            'interested_in' => $request->get('interested_in') ?? 'cb_ct',
            'date_of_birth' => $request->get('date_of_birth') ?? null,
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'address' => $request->get('address'),
            'postal_code' => $request->get('postal_code'),
            'country' => $request->get('country'),
            'city' => $request->get('city'),
            'language' => $request->get('language'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number'),
            'how_did_it_find_us' => $request->get('how_did_it_find_us'),
            'status' => $request->get('status'),

            'father_name' => $request->get('father_name') ?? null,
            'expected_date_of_birth_of_baby' => $request->get('expected_date_of_birth_of_baby') ?? null,
            'expected_date_clinic' => $request->get('expected_date_clinic') ?? null,
            'twin_pregnancy' => $request->get('twin_pregnancy') ?? 'no',
            'date_of_liposuction' => $request->get('date_of_liposuction') ?? null,
            'doctor' => $request->get('doctor') ?? null,
            'clinic' => $request->get('clinic') ?? null,
            'area_of_interest' => $request->get('area_of_interest') ?? null,
            'willing_to_travel_inside_eu' => $request->get('willing_to_travel_inside_eu') ?? null,
            'willing_to_travel_outside_eu' => $request->get('willing_to_travel_outside_eu') ?? null,
            'vacations_included' => $request->get('vacations_included') ?? null,
            'comments' => $request->get('comments') ?? null,

            'contract_signed' => $request->get('contract_signed') ?? null,
            'contract_signed_date' => $request->get('contract_signed_date') ?? null,
            'kit_sent' => $request->get('kit_sent') ?? null,
            'kit_sent_date' => $request->get('kit_sent_date') ?? null,
            'process_completed' => $request->get('process_completed') ?? null,
            'mild_wife' => $request->get('mild_wife') ?? null,

            'mother_name' => $request->get('mother_name') ?? null,
            'estimated_due_date' => $request->get('estimated_due_date') ?? null,
            'agreement_signed_on' => $request->get('agreement_signed_on') ?? null,
            'signed_agreement_sent' => $request->get('signed_agreement_sent') ?? null,
            'attached_files' => $attached_files ?? null,
        ]);
//        $prospect->first_name = $request->first_name;
//        $prospect->last_name = $request->last_name;
//        $prospect->phone_number = $request->phone_number;
//        $prospect->address = $request->address;
//        $prospect->status = $request->status;
//        $prospect->country = $request->country;
//        $prospect->city = $request->city;
//        $prospect->mother_name = $request->get('mother_name');
//        $prospect->father_name = $request->get('father_name');
//        $prospect->estimated_due_date = $request->get('estimated_due_date');
//        $prospect->interested_in = $request->get('interested_in');
//        $prospect->postal_code = $request->get('postal_code');
//        $prospect->language = $request->get('language');
//        $prospect->save();

        return redirect()->route('counselor.prospects.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Prospect updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        if (auth()->user()->hasRole('COUNSELORS')) {
            $prospect = User::findOrFail($id);
            $prospect->delete();

            return redirect()->route('counselor.prospects.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Prospect has been deleted'
                ]);
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function confirmationEmail($prospect)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $activation = $prospect->activation()->create([
            'token' => $randomString
        ]);
        Mail::to($prospect->email)->send(new VerifyMail($prospect, $randomString));
    }

    public function export(Request $request)
    {

        $this->validate($request, [
            'category_id' => 'required',
        ], [
            'category_id.required' => 'Category is required.',
        ]);
        if (!is_null($request->category_id)) {
            if ($request->category_id == 'all') {
                return Excel::download(new ProspectExportAll($request), 'prospect.xlsx');
            } elseif ($request->category_id == '3') {
                return Excel::download(new ProspectExport($request), 'prospect.xlsx');
            } elseif ($request->category_id == '4') {
                return Excel::download(new ProspectExportAt($request), 'prospect.xlsx');
            } elseif ($request->category_id == '5') {
                return Excel::download(new ProspectExportTreatment($request), 'prospect.xlsx');
            } else {
                return Excel::download(new ProspectExportOthers($request), 'prospect.xlsx');
            }

        } else {
            return redirect()->back('counselor.prospects.index')
                ->with([
                    'flash_status' => 'warning',
                    'flash_message' => 'Category is Required.'
                ]);
        }

    }

}
