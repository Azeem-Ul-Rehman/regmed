<?php

namespace App\Http\Controllers\Counselor;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {

        $user = auth()->user();
        return view('counselor.profiles.index', compact('user'));
    }

    public function editProfile()
    {

        $user = auth()->user();

        return view('counselor.profiles.edit-profile', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required',
            'email' => 'required|string|email|max:255',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'email.required' => 'Email  is required.',
        ]);

        $user = User::find(auth()->user()->id);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->country = $request->country;
        $user->company = $request->company;
        $user->city = $request->city;
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        $user->postal_code = $request->postal_code;
        $user->language = $request->language;

        $user->contact_person = $request->contact_person;
        $user->practice = $request->practice;
        $user->contract_on = $request->contract_on;
        $user->delivery = $request->delivery ?? 'no';
        $user->delivery_date = $request->delivery_date ?? null;
        $user->posters = $request->posters ?? 'no';
        $user->posters_date = $request->posters_date ?? null;
        $user->demo_kit = $request->demo_kit ?? 'no';
        $user->demo_kit_date = $request->demo_kit_date ?? null;
        $user->experience_with_cb = $request->experience_with_cb ?? null;
        $user->experience_with_at = $request->experience_with_at ?? null;
        $user->signed_contract = $request->signed_contract ?? null;
        $user->offices = $request->offices ?? null;
        $user->work_field = $request->work_field ?? null;
        $user->membership = $request->membership ?? 'no';
        $user->membership_date = $request->membership_date ?? null;
        $user->comments = $request->comments;
        $user->save();

        return redirect()->route('counselor.profile.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Profile updated successfully.'
            ]);
    }

    public function editProfileImage(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        $user = User::where('id', $request->user_id)->first();

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $user->profile_pic;
        }
        $user->update([
            'profile_pic' => $profile_image,
        ]);

        $response['status'] = 'success';
        $response['message'] = 'Profile Image updated successfully.';

        return $response;


    }

    public function changePassword()
    {

        $user = auth()->user();
        return view('counselor.profiles.change-password', compact('user'));
    }

    public function changeMobileNumber()
    {

        $user = auth()->user();
        return view('counselor.profiles.change-mobile-number', compact('user'));
    }

    public function updateMobileNumber(Request $request)
    {
        $this->validate($request, [
            'phone_number' => 'required',
        ], [
            'phone_number.required' => 'Mobile Number is required.',

        ]);

        $user = User::find(auth()->user()->id);

        $user->update([
            'phone_number' => $request->get('phone_number'),
        ]);

        return redirect()->route('counselor.profile.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Mobile Number updated successfully.'
            ]);
    }

    public function updatePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), auth()->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
        ]);

        if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return redirect()->route('counselor.profile.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Password Changed successfully.'
                ]);

        } else {
            return redirect()->back()->with("error", "New Password must be same as your confirm password.");
        }


    }
}
