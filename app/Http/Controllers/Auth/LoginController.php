<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ContactUsEmail;
use App\Models\Activation;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function username()
    {
        return 'email';
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->status == 'suspended') {
            Auth::logout();
            return redirect()->route('index')->with([
                'flash_status' => 'error',
                'flash_message' => 'Your Account has been suspended.Please Contact Support.'
            ]);
        } elseif ($user->activated == false) {
            Auth::logout();
            return redirect()->route('index')->with([
                'flash_status' => 'error',
                'flash_message' => 'Your Account is not activated yet.Please verify your Account.'
            ]);
        } else {

            $user->last_login = Carbon::now()->toDateTimeString();
            $user->save();

            if ($user->hasRole('ADMIN') || $user->hasRole('SUPER_ADMIN')) {
                return redirect()->route('admin.dashboard.index');
            }
            if ($user->hasRole('COUNSELORS')) {
                return redirect()->route('counselor.dashboard.index');
            }
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }


    }

    public function activation($token)
    {

        $user = Auth::check();
        $activation = Activation::where('token', $token)->first();
        if (isset($activation)) {
            $user = $activation->user;
            if (!$user->activated) {
                $user = $activation->user;
                $user->activated = true;
                $user->registration_date = date('Y-m-d');
                $user->status = 'verified';
                $user->save();
                if ($user->user_type == 'prospect') {
                    $status = "Your Account is Verified";
                    $this->verificationEmail($user);
                } else {
                    $status = "Your Account is Verified. Please check your email for credentials.";
                    $this->credentialsEmail($user);
                }

            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/login')->with([
                'flash_status' => 'warning',
                'flash_message' => 'Sorry your email cannot be identified.'
            ]);
        }

        return redirect('/login')->with([
            'flash_status' => 'success',
            'flash_message' => $status
        ]);
    }


    public function credentialsEmail($user)
    {
        $details = [
            'greeting' => 'Hi ' . $user->first_name . ' ' . $user->last_name,
            'body' => 'Your Account is Verified <br><br> <b>Email: </b>' . $user->email . '<br><br><b>Password: </b>123456',
            'from' => config('app.admin'),
            'subject' => 'Account Verification'

        ];
        Mail::to($user->email)->send(new ContactUsEmail($details));
    }

    public function verificationEmail($user)
    {
        $details = [
            'greeting' => 'Hi ' . $user->first_name . ' ' . $user->last_name,
            'body' => 'Your Account is Verified',
            'from' => config('app.admin'),
            'subject' => 'Account Verification'

        ];
        Mail::to($user->email)->send(new ContactUsEmail($details));
    }

}
