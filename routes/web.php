<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
})->name('index');
Auth::routes(['verify' => true]);

Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
//    \Illuminate\Support\Facades\Artisan::call('migrate');
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    return 'Commands run successfully Cleared.';
});


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/user/verify/{token}', 'Auth\LoginController@activation');
// Registration Routes...
Route::get('/register/{referral_code?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('/dashboard', 'Backend\DashboardController');
        Route::resource('/users', 'Backend\UserController');
        Route::post('users/excel', 'Backend\UserController@export')->name('user.export');
        Route::resource('/categories', 'Backend\CategoryController');
        Route::resource('/prospects', 'Backend\ProspectController');
        Route::get('users/prospects/{id}', 'Backend\ProspectController@usersProspects')->name('users.prospects');
        Route::post('prospect/excel', 'Backend\ProspectController@export')->name('prospect.export');
        Route::resource('/settings', 'Backend\SettingController', [
            'only' => ['index', 'edit', 'update']
        ]);
        Route::resource('/contacts', 'Backend\ContactUsController', [
            'only' => ['index', 'destroy']
        ]);
        Route::resource('/privacypolicy', 'Backend\PrivacyController', [
            'only' => ['index', 'store']
        ]);
        //User Profiles
        Route::get('profile', 'Backend\ProfileController@index')->name('profile.index');

        Route::get('edit/profile', 'Backend\ProfileController@editProfile')->name('profile.edit');
        Route::post('edit/profile/image', 'Backend\ProfileController@editProfileImage')->name('profile.edit.image');
        Route::post('update/profile', 'Backend\ProfileController@updateProfile')->name('update.user.profile');

        Route::get('update/password/', 'Backend\ProfileController@changePassword')->name('update.password');
        Route::post('update/user/password/', 'Backend\ProfileController@updatePassword')->name('update.user.password');

        Route::get('update/phone', 'Backend\ProfileController@changeMobileNumber')->name('update.phone');
        Route::post('update/phone', 'Backend\ProfileController@updateMobileNumber')->name('update.user.phone');

        // Send Notifications

        Route::resource('/notifications', 'Backend\NotificationController');


    });
    Route::prefix('counselor')->name('counselor.')->group(function () {
        Route::resource('/dashboard', 'Counselor\DashboardController');
        Route::resource('/prospects', 'Counselor\ProspectController');
        Route::post('prospect/excel', 'Counselor\ProspectController@export')->name('prospect.export');

        //User Profiles
        Route::get('profile', 'Counselor\ProfileController@index')->name('profile.index');
        Route::get('edit/profile', 'Counselor\ProfileController@editProfile')->name('profile.edit');
        Route::post('edit/profile/image', 'Counselor\ProfileController@editProfileImage')->name('profile.edit.image');
        Route::post('update/profile', 'Counselor\ProfileController@updateProfile')->name('update.user.profile');
        Route::get('update/password/', 'Counselor\ProfileController@changePassword')->name('update.password');
        Route::post('update/user/password/', 'Counselor\ProfileController@updatePassword')->name('update.user.password');
        Route::get('update/phone', 'Counselor\ProfileController@changeMobileNumber')->name('update.phone');
        Route::post('update/phone', 'Counselor\ProfileController@updateMobileNumber')->name('update.user.phone');

        // Send Notifications

        Route::resource('/notifications', 'Counselor\NotificationController');


    });

});


Route::get('privacy-policy', function () {
    return view('frontend.pages.privacy-policy');
})->name('privacy.policy');
Route::get('terms-and-conditions', function () {
    return view('frontend.pages.terms-and-conditions');
})->name('terms-and-conditions');

//Ajax Routes
Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::get('role-categories', 'Backend\AjaxController@roleCategories')->name('role.categories');
    Route::get('category-service-provider', 'Backend\AjaxController@categoryServiceProvider')->name('category.service.providers');
    Route::post('click-to-read-notification', 'Backend\AjaxController@clickToReadNotification')->name('click.to.read.notification');
});

