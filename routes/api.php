<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//    Auth Controller
Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');
Route::post('otp', 'Api\AuthController@otp');
Route::post('verify/otp', 'Api\AuthController@verifyOTP');
Route::post('forgot-password-request', 'Api\AuthController@forgotPasswordRequest');


//    General Controller
Route::get('roles', 'Api\GeneralController@roles');
Route::get('categories/{role_id}', 'Api\GeneralController@categories');
Route::get('service-providers/{category_id}', 'Api\GeneralController@serviceProviders');
Route::get('settings', 'Api\GeneralController@settings');
Route::get('service-providers', 'Api\GeneralController@serviceProvidersWithoutCategory');

Route::group(['middleware' => 'auth:api'], function () {

    //    Auth Controller
    Route::post('logout', 'Api\AuthController@logout');
    Route::get('user', 'Api\AuthController@getUser');
    Route::post('fcm', 'Api\AuthController@updateFcmToken');


    //    General Controller
    Route::post('update-profile-image', 'Api\GeneralController@updateProfileImage');
    Route::post('update-profile', 'Api\GeneralController@updateProfile');
    Route::post('update-password', 'Api\GeneralController@updatePassword');


    //    User Creation
    Route::post('create/user', 'Api\GeneralController@createUser');
    Route::post('update/user', 'Api\GeneralController@updateUser');
    Route::post('delete/user', 'Api\GeneralController@deleteUser');
    Route::get('get/users', 'Api\GeneralController@getAllUsers');
    Route::get('get/prospects', 'Api\GeneralController@getAllProspects');


    //Notifications
    Route::get('get/notification/{user_id}', 'Api\GeneralController@getNotificationByUser');
    Route::get('get/user/if/notification', 'Api\GeneralController@getAllUsersIfNotification');
    Route::post('send/notification', 'Api\GeneralController@sendNotification');
    Route::post('isread/notification', 'Api\GeneralController@isReadNotification');
    Route::get('get/latest/notifications', 'Api\GeneralController@getLatestNotifications');






    //Service Providers
    Route::get('get/service-provider/counselors', 'Api\GeneralController@getAllCounselorsOfServiceProvider');
    Route::get('get/service-provider/prospects', 'Api\GeneralController@getAllProspectsOfServiceProvider');

    //Counselors
    Route::get('get/counselors/prospects', 'Api\GeneralController@getAllProspectsOfCounselors');

    //Contact Us
    Route::post('contact-us', 'Api\GeneralController@contactUs');



});

Route::fallback(function () {
    return response()->json(['message' => 'URL Not Found'], 404);
});
