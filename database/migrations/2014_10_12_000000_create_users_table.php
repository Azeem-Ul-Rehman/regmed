<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            //general fields

            $table->string('company')->nullable();
            $table->string('first_name')->nullable(); //required
            $table->string('last_name')->nullable();  //required
            $table->text('address')->nullable();  //required
            $table->string('postal_code')->nullable();
            $table->string('city')->nullable();  //required
            $table->string('country')->nullable(); //required
            $table->string('phone_number')->nullable();  //required
            $table->string('email')->unique()->nullable();  //required
            $table->string('password')->nullable();  //required
            $table->enum('membership', ['yes', 'no'])->default('no');
            $table->date('membership_date')->nullable();
            $table->string('contact_person')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->date('registration_date')->nullable();
            $table->longText('comments')->nullable();
            $table->integer('role_id')->nullable(); //required 1
            $table->integer('category_id')->nullable(); //required 2
            $table->string('gender')->nullable();  //required
            $table->enum('user_type', ['super-admin', 'admin', 'counselors', 'service-provider', 'contact', 'prospect', 'customer'])->nullable();
            $table->enum('status', ['pending', 'suspended', 'verified'])->default('pending');
            $table->string('profile_pic')->default('default.png');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->boolean('activated')->default(false);
            $table->boolean('is_login')->default(false);
            $table->string('language')->nullable();
            $table->string('otp_code')->nullable();
            $table->enum('otp_status', ['sent', 'verified'])->default('sent');


            $table->unsignedInteger('service_provider')->nullable();
            $table->unsignedInteger('creator_id')->nullable();



            //new field
            $table->text('offices')->nullable();
            $table->text('work_field')->nullable();

            $table->enum('delivery', ['yes', 'no'])->default('no');
            $table->date('delivery_date')->nullable();
            $table->enum('posters', ['yes', 'no'])->default('no');
            $table->date('posters_date')->nullable();
            $table->enum('demo_kit', ['yes', 'no'])->default('no');
            $table->date('demo_kit_date')->nullable();
            $table->text('experience_with_cb')->nullable();
            $table->text('experience_with_at')->nullable();
            $table->text('signed_contract')->nullable();
            //end new fields

            $table->enum('interested_in', ['cb_ct', 'at', 'treatments','others'])->default('cb_ct');
            $table->date('date_of_birth')->nullable();
            $table->text('how_did_it_find_us')->nullable();
            $table->text('father_name')->nullable();
            $table->date('expected_date_of_birth_of_baby')->nullable();
            $table->text('expected_date_clinic')->nullable();
            $table->enum('twin_pregnancy', ['yes', 'no'])->default('no');
            $table->date('date_of_liposuction')->nullable();
            $table->text('doctor')->nullable();
            $table->text('clinic')->nullable();
            $table->text('area_of_interest')->nullable();
            $table->text('willing_to_travel_inside_eu')->nullable();
            $table->text('willing_to_travel_outside_eu')->nullable();
            $table->text('vacations_included')->nullable();

            $table->string('practice')->nullable();
            $table->string('contract_on')->nullable();
            $table->string('mother_name')->nullable();
            $table->date('estimated_due_date')->nullable();

            $table->date('agreement_signed_on')->nullable();
            $table->date('signed_agreement_sent')->nullable();
            $table->string('attached_files')->nullable();



            $table->enum('contract_signed', ['yes', 'no'])->default('no');
            $table->date('contract_signed_date')->nullable();
            $table->enum('kit_sent', ['yes', 'no'])->default('no');
            $table->date('kit_sent_date')->nullable();
            $table->enum('process_completed', ['yes', 'no'])->default('no');

            $table->text('mild_wife')->nullable();


            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
