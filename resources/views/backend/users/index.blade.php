@extends('layouts.master')
@section('title','Users')
@push('css')


    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <style>
        .responsiveTable {
            overflow-x: scroll;
            margin-top: 10px;
        }

        @media screen and (max-width: 650px) {
            .responsiveTable {
                overflow-x: scroll;
                margin-top: 10px;
            }

            .responsiveTable a {
                width: 100%;
                margin: 5px 0;
            }

            #show_list, #download_list, #refresh {
                margin-bottom: 10px;
            }

        }

        .daterangepicker .ranges .input-mini {
            width: 100% !important;
        }

        .daterangepicker .ranges .range_inputs > div:nth-child(2) {
            padding-left: 0px !important;
        }

        .daterangepicker .ranges .range_inputs .btn {
            padding: 7px !important;
            margin-right: 4px !important;
        }

    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Users
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.users.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add User</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="box">
                    <div class="col-md-12">
                        <div class="box-body">
                            <form action="#" method="get" id="form-data">
                                @csrf
                                <div class="row">

                                    <div class="col-md-3 {{ $errors->has('category_id') ? 'has-error' : ''}}">
                                        <label for="category_id">Category</label>
                                        <select class="form-control"
                                                name="category_id"
                                                id="category_id">
                                            <option value="">Select Category
                                            </option>
                                            @if(!empty($categories) && count($categories) > 0)
                                                @foreach ($categories as $category)
                                                    @if(!is_null($category))
                                                        <option
                                                            value="{{$category->id}}" {{ $category->id == old('category_id') ? 'selected' : ''}}>{{$category->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <option value="all">All
                                            </option>
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('category_id') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{ $errors->has('creater_id') ? 'has-error' : ''}}">
                                        <label for="creater_id">Created By</label>
                                        <select class="form-control"
                                                name="creater_id"
                                                id="creater_id">
                                            <option value="">Select Creater
                                            </option>
                                            @if(!empty($creaters) && count($creaters) > 0)
                                                @foreach ($creaters as $user)
                                                    @if(!is_null($user->creator))
                                                        <option
                                                            value="{{$user->id}}" {{ $user->id == old('creater_id') ? 'selected' : ''}}>{{$user->creator->first_name . " ". $user->creator->last_name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('creater_id'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('creater_id') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{ $errors->has('city') ? 'has-error' : ''}}">
                                        <label for="city">City</label>
                                        <select class="form-control"
                                                name="city"
                                                id="city">
                                            <option value="">Select City
                                            </option>
                                            @foreach ($cities as $city)
                                                @if(!is_null($city))
                                                    <option
                                                        value="{{$city}}" {{ $city == old('city') ? 'selected' : ''}}>{{ucfirst($city) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('city'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('city') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{ $errors->has('country') ? 'has-error' : ''}}">
                                        <label for="country">Country</label>
                                        <select class="form-control"
                                                name="country"
                                                id="country">
                                            <option value="">Select Country
                                            </option>
                                            @foreach ($countries as $country)
                                                @if(!is_null($country))
                                                    <option
                                                        value="{{$country}}" {{ $country == old('country') ? 'selected' : ''}}>{{ucfirst($country) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('country') }}</span>
                                        @endif
                                    </div>

                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4 {{ $errors->has('registration_date') ? 'has-error' : ''}}">
                                        <label for="registration_date">Registration Date</label>
                                        <input type="text" class="form-control" name="registration_date"
                                               id="registration_date"
                                               placeholder="DD-MM-YYYY"
                                               value="{{ old('registration_date') }}">
                                        @if ($errors->has('registration_date'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('registration_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <a href="{{ route('admin.users.index') }}" id="refresh"
                                               class="btn btn-block btn-primary float-right"> <i
                                                    class="la la-refresh"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" id="show_list"
                                                class="btn btn-block btn-primary float-right">Show Lists
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" id="download_list"
                                                class="btn btn-block btn-primary float-right">Download
                                            Report
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="responsiveTable">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                        <tr>

                            <th style="width: 10%;"> Registration Complete</th>
                            {{--                            <th> Role</th>--}}
                            <th> Creator</th>
                            <th> Category</th>
                            <th> Name</th>
                            <th> Email</th>
                            {{--                            <th> City</th>--}}
                            <th> Country</th>
                            <th> Status</th>
                            <th> Last Login</th>
                            <th> View Prospects</th>
                            <th> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($users))
                            @foreach($users as $user)
                                @if($user->user_type != 'super-admin')
                                    <tr>
                                        <td>{{$user->registration_date ? date('d-m-Y',strtotime($user->registration_date)) : '--'}}</td>
                                        {{--                                        <td>{{ucwords(str_replace('-',' ',$user->user_type))}}</td>--}}
                                        <td>{{$user->creator ? $user->creator->first_name . " ". $user->creator->last_name : ''}}</td>
                                        <td>{{$user->category ?  $user->category->name : 'Not Found'}}</td>
                                        <td>{{ucfirst($user->last_name)}} {{ ucfirst($user->first_name) }}</td>
                                        <td>{{$user->email }}</td>
                                        {{--                                        <td>{{$user->city ?? '--'}}</td>--}}
                                        <td>{{$user->country ?? '--'}}</td>
                                        <td style="width: 10%;">{{ucfirst($user->status)}}</td>
                                        <td>{{$user->last_login ? date('d-m-Y H:i a',strtotime($user->last_login)): '--'}}</td>
                                        <td>
                                            <a href="{{route('admin.users.prospects',$user->id)}}"
                                               class="btn btn-sm btn-info pull-left ">Prospets</a>
                                        </td>
                                        <td style="display: flex;">
                                            <a href="{{route('admin.users.show',$user->id)}}"
                                               class="btn btn-sm btn-info pull-left ">View</a>
                                            <a href="{{route('admin.users.edit',$user->id)}}"
                                               class="btn btn-sm btn-success pull-left ">Edit</a>
                                            <form method="post" action="{{ route('admin.users.destroy', $user->id) }}"
                                                  id="delete_{{ $user->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <a style="" class="btn btn-sm btn-danger m-left"
                                                   href="javascript:void(0)"
                                                   onclick="if(confirmDelete()){ document.getElementById('delete_<?=$user->id?>').submit(); }">
                                                    Delete
                                                </a>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>


    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

    <script>
        $('#registration_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $("#m_table_1").dataTable({

            "columnDefs": [
                {orderable: false, targets: [8]}
            ],
        });
        $('#show_list').click(function () {
            $('#form-data').attr('action', '{{ route('admin.users.index') }}');
            $('#form-data').attr('method', 'GET');
            $("#form-data").submit();
        });
        $('#download_list').click(function () {
            $('#form-data').attr('action', '{{ route('admin.user.export') }}');
            $('#form-data').attr('method', 'POST');
            $("#form-data").submit();
        });


    </script>
@endpush
