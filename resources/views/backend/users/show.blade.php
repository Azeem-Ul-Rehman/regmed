@extends('layouts.master')
@section('title','Users')
@push('css')
    <style>
        .show-shifts, .show-services {
            display: none;
        }

        .select2-container {
            width: 100% !important;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            View {{ __('User') }} Details
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                <div class="m-form__actions m-form__actions">
                    <a href="{{ route('admin.users.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">

                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                            </tr>
                            <tr>
                                <th>Role</th>
                                <td>{{ucwords(str_replace('-',' ',$user->user_type))}}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>{{$user->category ?  $user->category->name : 'Not Found'}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <th>Phone Number</th>
                                <td>{{ $user->phone_number ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Company</th>
                                <td>{{ $user->company ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{ $user->address ?? '-' }}</td>
                            </tr>

                            <tr>
                                <th>City</th>
                                <td>{{ $user->city ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>{{ $user->country ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Postal Code</th>
                                <td>{{ $user->postal_code ?? '-' }}</td>
                            </tr>

                            <tr>
                                <th>Status</th>
                                <td>{{ $user->status ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Last Login</th>
                                <td>{{ $user->last_login ? date('Y-m-d H:i A', strtotime($user->last_login)) : '-' }}</td>
                            </tr>


                            <tr>
                                <th>Language</th>
                                <td>{{ $user->language ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Contact Person</th>
                                <td>{{ $user->contact_person ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Practice</th>
                                <td>{{ $user->practice ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Contract On</th>
                                <td>{{ $user->contract_on ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Comments</th>
                                <td>{{ $user->comments ?? '-' }}</td>
                            </tr>
                            @if($user->role_id == 3)
                                <tr>
                                    <th>Membership</th>
                                    <td>{{ $user->membership ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Membership Date</th>
                                    <td>{{ $user->membership_date ? date('Y-m-d', strtotime($user->membership_date)) : '-' }}</td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <th>Material Delivery</th>--}}
{{--                                    <td>{{ $user->delivery ?? '-' }}</td>--}}
{{--                                </tr>--}}
                                <tr>
                                    <th>Material Delivery Date</th>
                                    <td>{{ $user->delivery_date ? date('Y-m-d', strtotime($user->delivery_date)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Posters</th>
                                    <td>{{ $user->posters ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Posters Date</th>
                                    <td>{{ $user->posters_date ? date('Y-m-d', strtotime($user->posters_date)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Demo Kit</th>
                                    <td>{{ $user->demo_kit ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Demo Kit Date</th>
                                    <td>{{ $user->demo_kit_date ? date('Y-m-d', strtotime($user->demo_kit_date)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Offices</th>
                                    <td>{{ $user->offices ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Work Field</th>
                                    <td>{{ $user->work_field ?? '-' }}</td>
                                </tr>
                                @if($user->category_id == 14 || $user->category_id == 13 || $user->category_id == 12)
                                    <tr>
                                        <th>Experience With CB/CT</th>
                                        <td>{{ $user->experience_with_cb ?? '-' }}</td>
                                    </tr>
                                @endif
                                @if($user->category_id == 15)
                                    <tr>
                                        <th>Experience With AT</th>
                                        <td>{{ $user->experience_with_at ?? '-' }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Signed Contract</th>
                                    <td>{{ $user->signed_contract ?? '-' }}</td>
                                </tr>
                                @if($user->category_id == 15 || $user->category_id == 14)
                                    <tr>
                                        <th>Work Field</th>
                                        <td>{{ $user->work_field ?? '-' }}</td>
                                    </tr>
                                @endif
                            @endif
                            @if($user->role_id == 4)
                                <tr>
                                    <th>Offices</th>
                                    <td>{{ $user->offices ?? '-' }}</td>
                                </tr>
                            @endif




                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
