@extends('layouts.master')
@section('title','Users')
@push('css')
    <style>
        .show-shifts, .show-services {
            display: none;
        }

        .select2-container {
            width: 100% !important;
        }
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('User') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.users.store') }}" id="create"
                              enctype="multipart/form-data" role="form">

                            @csrf
                            <input type="hidden" name="fcm_token" id="device_token" value="">
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="role_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Role') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="role_id"
                                                    class="form-control roles @error('role_id') is-invalid @enderror"
                                                    name="role_id" autocomplete="role_id">
                                                <option value="">Select a role</option>
                                                @if(!empty($roles))
                                                    @foreach($roles as $role)
                                                        @if($role->id != '1')
                                                            <option
                                                                value="{{$role->id}}" {{ (old('role_id') == $role->id) ? 'selected' : '' }}>{{ucfirst($role->name)}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('role_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Category') }}
                                                <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="category_id"
                                                    class="form-control categories @error('category_id') is-invalid @enderror"
                                                    name="category_id" autocomplete="category_id">
                                                <option value="">Select a Category</option>
                                            </select>

                                            @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="first_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('First Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="first_name" type="text"
                                                   class="form-control @error('first_name') is-invalid @enderror"
                                                   name="first_name" value="{{ old('first_name') }}"
                                                   autocomplete="first_name" autofocus>

                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="last_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Last Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="last_name" type="text"
                                                   class="form-control @error('last_name') is-invalid @enderror"
                                                   name="last_name" value="{{ old('last_name') }}"
                                                   autocomplete="last_name" autofocus>

                                            @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="email"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email"
                                                   value="{{ old('email') }}" autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="company"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Company') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="company" type="text"
                                                   class="form-control @error('company') is-invalid @enderror"
                                                   name="company" value="{{ old('company') }}"
                                                   autocomplete="company" autofocus>

                                            @error('company')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="city"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('City') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="city" type="text"
                                                   class="form-control @error('city') is-invalid @enderror"
                                                   name="city"
                                                   value="{{ old('city') }}" autocomplete="city">

                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="country"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Country') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="country" type="text"
                                                   class="form-control @error('country') is-invalid @enderror"
                                                   name="country" value="{{ old('country') }}"
                                                   autocomplete="country" autofocus>

                                            @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="address"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   value="{{ old('address') }}" autocomplete="address">

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="postal_code"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Postal Code') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="postal_code" type="text"
                                                   class="form-control @error('postal_code') is-invalid @enderror"
                                                   name="postal_code"
                                                   value="{{ old('postal_code') }}" autocomplete="postal_code">

                                            @error('postal_code')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="language"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Language') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="language" type="text"
                                                   class="form-control @error('language') is-invalid @enderror"
                                                   name="language" value="{{ old('language') }}"
                                                   autocomplete="language" autofocus placeholder="language">

                                            @error('language')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="comments"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Comments') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="comments"
                                                      class="form-control @error('comments') is-invalid @enderror"
                                                      name="comments"
                                                      autocomplete="comments">{{ old('comments') }}</textarea>

                                            @error('comments')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="contact_person"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Contact Person') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="contact_person" type="text"
                                                   class="form-control @error('contact_person') is-invalid @enderror"
                                                   name="contact_person" value="{{ old('contact_person') }}"
                                                   autocomplete="contact_person" autofocus placeholder="03001234567">

                                            @error('contact_person')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="phone_number"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Mobile Number') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="phone_number" type="text"
                                                   class="form-control @error('phone_number') is-invalid @enderror"
                                                   name="phone_number" value="{{ old('phone_number') }}"
                                                   autocomplete="phone_number" autofocus placeholder="03001234567">

                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <hr>


                                    <div id="counselor" style="display: none">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="membership"
                                                       class="col-md-4 col-form-label text-md-left">{{ __('Membership') }}
                                                    <span class="mandatorySign">*</span></label>

                                                <select id="membership"
                                                        class="form-control @error('membership') is-invalid @enderror"
                                                        name="membership" autocomplete="membership">
                                                    <option value="">Select an membership</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>


                                                @error('membership')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="membership_date"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Membership Date') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="membership_date" type="date"
                                                       class="form-control @error('membership_date') is-invalid @enderror"
                                                       name="membership_date"
                                                       value="{{ old('membership_date') }}"
                                                       autocomplete="membership_date" autofocus
                                                       placeholder="">

                                                @error('membership_date')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>

                                        </div>
                                        <div class="form-group row">
{{--                                            <div class="col-md-6">--}}
{{--                                                <label for="delivery"--}}
{{--                                                       class="col-md-6 col-form-label text-md-left">{{ __('Material Delivery') }}--}}
{{--                                                    <span class="delivery">*</span></label>--}}

{{--                                                <select id="delivery"--}}
{{--                                                        class="form-control @error('delivery') is-invalid @enderror"--}}
{{--                                                        name="delivery" autocomplete="delivery">--}}
{{--                                                    <option value="">Select an Material Delivery</option>--}}
{{--                                                    <option value="yes">Yes</option>--}}
{{--                                                    <option value="no">No</option>--}}
{{--                                                </select>--}}

{{--                                                @error('delivery')--}}
{{--                                                <span class="invalid-feedback" role="alert">--}}
{{--                                                <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                                @enderror--}}
{{--                                            </div>--}}
                                            <div class="col-md-12">
                                                <label for="delivery_date"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Material Delivery Date') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="delivery_date" type="date"
                                                       class="form-control @error('delivery_date') is-invalid @enderror"
                                                       name="delivery_date"
                                                       value="{{ old('delivery_date') }}"
                                                       autocomplete="delivery_date">

                                                @error('delivery_date')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="posters"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Posters') }}
                                                    <span class="posters">*</span></label>

                                                <select id="posters"
                                                        class="form-control @error('posters') is-invalid @enderror"
                                                        name="posters" autocomplete="posters">
                                                    <option value="">Select an Posters</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>

                                                @error('posters')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="posters_date"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Poster Date') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="posters_date" type="date"
                                                       class="form-control @error('posters_date') is-invalid @enderror"
                                                       name="posters_date"
                                                       value="{{ old('posters_date') }}"
                                                       autocomplete="posters_date">

                                                @error('posters_date')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="demo_kit"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Demo Kit') }}
                                                    <span class="demo_kit">*</span></label>

                                                <select id="demo_kit"
                                                        class="form-control @error('demo_kit') is-invalid @enderror"
                                                        name="demo_kit" autocomplete="demo_kit">
                                                    <option value="">Select an DemoKit</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>

                                                @error('demo_kit')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="demo_kit_date"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Demo Kit Date') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="demo_kit_date" type="date"
                                                       class="form-control @error('demo_kit_date') is-invalid @enderror"
                                                       name="demo_kit_date"
                                                       value="{{ old('demo_kit_date') }}"
                                                       autocomplete="demo_kit_date">

                                                @error('demo_kit_date')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="offices"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Office') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="offices" type="text"
                                                       class="form-control @error('offices') is-invalid @enderror"
                                                       name="offices" value="{{ old('offices') }}"
                                                       autocomplete="offices" autofocus
                                                >

                                                @error('offices')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="signed_contract"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Signed Contract') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="signed_contract" type="text"
                                                       class="form-control @error('signed_contract') is-invalid @enderror"
                                                       name="signed_contract" value="{{ old('signed_contract') }}"
                                                       autocomplete="signed_contract" autofocus
                                                >

                                                @error('signed_contract')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="service_provider"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Service Provider') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <select id="service_provider"
                                                        class="form-control serviceproviders @error('service_provider') is-invalid @enderror"
                                                        name="service_provider" autocomplete="service_provider">
                                                    @if(!empty($serviceProviders))
                                                        <option value="">Select Service Provider</option>
                                                        @foreach($serviceProviders as $sp)
                                                            <option
                                                                value="{{$sp->id}}" {{ (old('service_provider') == $sp->service_provider) ? 'selected' : '' }}>{{ucfirst($sp->first_name)}} {{ucfirst($sp->last_name)}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                @error('service_provider')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6" id="dr_gyn" style="display: none">
                                                <label for="experience_with_cb"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Experience With CB/CT') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="experience_with_cb" type="text"
                                                       class="form-control @error('experience_with_cb') is-invalid @enderror"
                                                       name="experience_with_cb" value="{{ old('experience_with_cb') }}"
                                                       autocomplete="experience_with_cb" autofocus
                                                >

                                                @error('experience_with_cb')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6" id="work_title_at" style="display: none">
                                                <label for="work_field"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Work Field') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="work_field" type="text"
                                                       class="form-control @error('work_field') is-invalid @enderror"
                                                       name="work_field" value="{{ old('work_field') }}"
                                                       autocomplete="work_field" autofocus
                                                >

                                                @error('work_field')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6" id="gen_at" style="display: none">
                                                <label for="experience_with_at"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Experience With AT') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="experience_with_at" type="text"
                                                       class="form-control @error('experience_with_at') is-invalid @enderror"
                                                       name="experience_with_at" value="{{ old('experience_with_at') }}"
                                                       autocomplete="experience_with_at" autofocus
                                                >

                                                @error('experience_with_at')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div id="serviceProviderss" style="display: none">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="offices"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Office') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="offices" type="text"
                                                       class="form-control @error('offices') is-invalid @enderror"
                                                       name="offices" value="{{ old('offices') }}"
                                                       autocomplete="offices" autofocus
                                                >

                                                @error('offices')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>

                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="password"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Password') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="password" type="text"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password" value="123456" readonly
                                                   autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <input type="hidden" name="status" value="pending">
                                        <input type="hidden" name="password_confirmation" value="123456">
                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.users.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>

        $('.roles').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.categories';
            var role_id = $(this).val();
            var request = "role_id=" + role_id;

            if (role_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.role.categories') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.categories, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Category</option>");


                            $('.categories').find('option[value="{{ old('category_id') }}"]').attr('selected', 'selected');
                            $('.categories').trigger('change');


                            if (role_id == '3') {
                                $('#counselor').css('display', '');
                                $('#serviceProviderss').css('display', 'none');
                            } else if (role_id == '4') {
                                $('#counselor').css('display', 'none');
                                $('#serviceProviderss').css('display', '');
                            } else {
                                $('#counselor').css('display', 'none');
                                $('#serviceProviderss').css('display', 'none');

                            }
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select a Category</option>");
            }
        });
        $('.categories').change(function () {
            var cat = $("#category_id option:selected").val();

            if (cat == '15') {
                $('#dr_gyn').css('display', 'none');
                $('#gen_at').css('display', '');
            } else if (cat == '14' || cat == '13' || cat == '12') {
                $('#dr_gyn').css('display', '');
                $('#gen_at').css('display', 'none');
            } else {
                $('#dr_gyn').css('display', 'none');
                $('#gen_at').css('display', 'none');
            }


            if (cat == '15' || cat == '14') {
                $('#work_title_at').css('display', '');
            } else {
                $('#work_title_at').css('display', 'none');
            }
        });
        {{--    form = $(this).closest('form');--}}
        {{--    node = $(this);--}}
        {{--    node_to_modify = '.serviceproviders';--}}
        {{--    var category_id = $(this).val();--}}
        {{--    var request = "category_id=" + category_id;--}}

        {{--    if (category_id !== '') {--}}
        {{--        $.ajax({--}}
        {{--            type: "GET",--}}
        {{--            url: "{{ route('ajax.category.service.providers') }}",--}}
        {{--            data: request,--}}
        {{--            dataType: "json",--}}
        {{--            cache: true,--}}
        {{--            success: function (response) {--}}
        {{--                if (response.status == "success") {--}}
        {{--                    var html = "";--}}
        {{--                    $.each(response.data.serviceProviders, function (i, obj) {--}}
        {{--                        html += '<option value="' + obj.id + '">' + obj.first_name + ' ' + obj.last_name + '</option>';--}}
        {{--                    });--}}
        {{--                    $(node_to_modify).html(html);--}}
        {{--                    $(node_to_modify).prepend("<option value='' selected>Select Service Provider</option>");--}}


        {{--                    $('.serviceproviders').find('option[value="{{ old('service_provider') }}"]').attr('selected', 'selected');--}}
        {{--                    $('.serviceproviders').trigger('change');--}}
        {{--                }--}}
        {{--            },--}}
        {{--            error: function () {--}}
        {{--                toastr['error']("Something Went Wrong.");--}}
        {{--            }--}}
        {{--        });--}}
        {{--    } else {--}}
        {{--        $(node_to_modify).html("<option value='' selected>Select Service Provider</option>");--}}
        {{--    }--}}
        {{--});--}}
    </script>
@endpush
