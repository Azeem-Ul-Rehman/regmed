@extends('layouts.master')
@section('title','Notifications')
@push('css')
    <style>
        .show-shifts, .show-services {
            display: none;
        }

        .select2-container {
            width: 100% !important;
        }
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Send {{ __('Notifications') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.notifications.store') }}" id="create"
                              enctype="multipart/form-data" role="form">

                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="user_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('User') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="user_id"
                                                    class="form-control roles @error('user_id') is-invalid @enderror"
                                                    name="user_id" autocomplete="user_id">
                                                <option value="">Select an User</option>
                                                @if(!empty($users))
                                                    @foreach($users as $user)

                                                        <option
                                                            value="{{$user->id}}" {{ (old('user_id') == $user->id) ? 'selected' : '' }}>{{ucfirst($user->first_name)}} {{ucfirst($user->last_name)}}</option>

                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('user_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <label for="message"
                                                   class="col-md-4 col-form-label text-md-left">Message
                                                <span class="mandatorySign">*</span></label>

                                            <input id="message" type="text"
                                                   class="form-control @error('message') is-invalid @enderror"
                                                   name="message"
                                                   value="{{ old('message') }}" autocomplete="message">

                                            @error('message')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">


                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.notifications.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

