@extends('frontend.layout.master')
@section('title','Privacy Policy')
@section('description','Regmed App')
@section('keywords', 'Regmed App')
@push('css')
@endpush
@section('content')
    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Privacy Policy</h3>
            </div>
        </div>
    </div>
    <div class="loginArea">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="formArea loginFormArea">
                    <p><b>AGBs (Term&Conditions)</b></p>
                    <p>Mit der Registrierung auf RegMedApp akzeptiert der Nutzer vorbehaltlos die folgenden Allgemeinen
                        Geschäftsbedingungen (AGB).</p>

                    <p><b>GELTUNGSBEREICH</b></p>

                    <p>Die Applikation RegMedApp (nachfolgend App genannt) ist eine Online Dienstleistung. Diese AGB
                        regeln das Vertragsverhältnis zwischen RegMedApp und dem Nutzer (natürliche oder juristische
                        Person, die bei der Applikation registriert ist) bezüglich Nutzung der App.
                    </p>
                    <p>Mit der Registrierung auf RegMedApp und der Nutzung der damit verbundenen Dienstleistungen
                        erklärt sich der Nutzer mit den AGB einverstanden. Die App behält sich das Recht vor, diese AGB
                        jederzeit und ohne Nennung von Gründen zu ändern. Änderungen werden dem Nutzer frühzeitig vor
                        Inkrafttreten bekannt gegeben. Durch die Weiterbenutzung der App nach Inkrafttreten der
                        geänderten AGB erklärt sich der Nutzer mit diesen einverstanden.
                    </p>


                    <p><b>SOFTWARENUTZUNG</b></p>
                    <p> RegMedApp stellt dem Nutzer die aktuellste Version der App als Dienstleistung zu den auf der
                        Website aufgeführten Konditionen zur Verfügung. Die Dienstleistung bleibt auf dem Server von
                        RegMedApp. Wartung und Aktualisierungen der Applikation haben für den Nutzer keine Kosten zur
                        Folge.
                    </p>
                    <p>RegMedApp kann jederzeit Wartungsarbeiten an der App durchführen. Während dieser Zeit können die
                        Leistungen allenfalls nicht zur Verfügung gestellt werden.
                    </p>
                    <p><b>REGISTRIERUNG UND AUTHENTISIERUNG</b></p>
                    <p> Der Nutzer erhält nach der Registrierung auf RegMedApp ein persönliches Benutzerkonto bestehend
                        aus Benutzername und Passwort. Er sichert zu, dass alle bei der Registrierung angegebenen Daten
                        richtig sind. Die Registrierung der User unter falschem Namen ist nicht gestattet. Im Falle von
                        offensichtlich fiktiven Angaben behält sich RegMedApp vor, das Konto zu löschen.
                    </p>
                    <p><b>PFLICHTEN DES NUTZERS</b></p>
                    <p>Die Zugangsdaten sind persönlich und der Nutzer verpflichtet sich, diese geheim zu halten, vor
                        dem Zugriff durch Dritte zu schützen und diese nicht an Dritte weiter zu geben. Der Provider
                        lehnt jegliche Haftung für Konsequenzen aus dem Bekanntwerden der Zugangsdaten ab.
                    </p>
                    <p>Der Nutzer verpflichtet sich, die Dienstleitungsplattform RegMedApp nicht zweckentfremdet zu
                        nutzen, sich nicht missbräuchlich zu verhalten und keine strafbaren oder rechtswidrigen Inhalte
                        jeglicher Art auf der Applikation zu speichern, zu verbreiten oder zugänglich zu machen. Des
                        Weiteren verpflichtet sich der Nutzer die Applikation nicht in einer Art und Weise zu benutzen,
                        welche die Verfügbarkeit für andere Nutzer negativ beeinflusst.
                    </p>
                    <p>Der Nutzer darf ausschliesslich die eigene Email Adresse in der Software zu benutzen.
                    </p>
                    <p>Bei Verstoss einer der in Ziffer 3 oder 4 aufgeführten Bestimmungen ist der Provider berechtigt,
                        den Zugriff des Nutzers zu sperren und zu löschen.
                    </p>
                    <p>Das Vertragsverhältnis beginnt mit der Registrierung auf der Applikation.
                    </p>
                    <p>Jeder Nutzer kann die Applikation jederzeit kostenlos nutzen.
                    </p>
                    <p>Wird das Konto über eine längere Zeitdauer nicht genutzt behält sich RegMedApp vor, das Konto zu
                        löschen. Der Nutzer wird in diesem Fall frühzeitig informiert.
                    </p>


                    <p><b>KÜNDIGUNG</b></p>
                    <p>Der Nutzer kann die Nutzung von RegMedApp jederzeit künden. Die Kündigung muss durch den
                        Eigentümer des Kontos per Email an die Swiss RegMed AG (data@regmedapp.ch) erfolgen. Die
                        Kündigung entspricht der Löschung des Accounts inklusive sämtlicher Daten.
                    </p>
                    <p>Der Provider kann aus wichtigem Grund das Vertragsverhältnis kündigen. Als wichtige Gründe gelten
                        insbesondere:
                    </p>
                    <ul>
                        <li>Nichteinhaltung gesetzlicher Vorschriften durch den Nutzer</li>
                        <li>Verstoss des Nutzers gegen vertragliche Pflichten (z.B. missbräuchliche Nutzung, Verwendung
                            falscher Nutzerangaben usw.)
                        </li>
                        <li>Bei Nutzung der Dienstleistung zum Zweck von kriminellen, gesetzwidrigen oder ethisch
                            bedenklichen Handlungen
                        </li>
                    </ul>

                    <p><b>SUPPORT</b></p>
                    <p>Der Provider stellt dem Nutzer zur Unterstützung in technischen Fragen betreffend der Applikation
                        einen Supportdienst via E-Mail (support@regmedapp.ch).
                    </p>
                    <p><b>DATENSCHUTZ</b></p>
                    <p>Sämtliche sensible Daten (z.B. Prospektedaten) sind sowie gegen über Dritten verschlüsselt.
                    </p>
                    <p>Ausnahme betrifft Daten des Nutzers selbst für die Weitergabe von persönlichen Daten sind für den
                        rechtlichen Schutz des Nutzers oder des Providers oder auf Erfüllung richterlicher/behördlicher
                        Anforderungen.
                    </p>
                    <p>Der Provider ist im Rahmen der gesetzlichen Bestimmungen ermächtigt, Nutzerdaten für
                        betriebliche Zwecke (insbesondere Marktforschung) zu speichern und auszuwerten. Die Auswertungen
                        werden ausschliesslich innerbetrieblich (inkl. Partner für die angebotenen Dienstleistungen)
                        verwendet und nicht an Dritte weitergegeben. Der Nutzer erklärt sich hiermit ausdrücklich
                        einverstanden.
                    </p>

                    <p><b>GEWÄHRLEISTUNG UND HAFTUNG</b></p>
                    <p>Der Provider ist für einen ordnungsgemässen Betrieb der Applikation bemüht. Aus nicht
                        beeinflussbaren Gründen des Providers, kann es zu einer Störung der Verfügbarkeit der
                        Applikation kommen. Der Provider bemüht sich im Rahmen seiner Möglichkeiten, die Verfügbarkeit
                        so rasch wie möglich wieder zu gewähren.
                    </p>
                    <p>Der Nutzer ist alleine für Inhalte und Daten, die auf der Applikation gespeichert sind,
                        verantwortlich. Die App lehnt jegliche Haftung für allfällige direkte oder indirekte Schäden
                        durch die Nutzung der Applikation ab. Die App haftet nicht für den Verlust von Daten sowie für
                        rechtswidrige Handlungen Dritter. Die App ist zur sofortigen Sperre des Accounts des Nutzers
                        befugt, wenn der begründete Verdacht von rechtswidrig gespeicherten Daten und/oder die
                        Verletzung der Rechte Dritter besteht. Ein begründeter Verdacht liegt insbesondere dann vor,
                        wenn Gerichte, Behörden und/oder sonstige Dritte den Provider davon in Kenntnis setzen.
                    </p>
                    <p>Der Provider hat den Nutzer von der Sperre und dem Grund dafür unverzüglich zu verständigen. Die
                        Sperre ist aufzuheben, sobald der Verdacht vollumfänglich entkräftet ist. Der Nutzer
                        verpflichtet sich, den Provider für allfällige Schäden einschliesslich allen Ansprüchen Dritter,
                        die auf den von ihm gespeicherten Daten beruhen, freizustellen. Wenn der Nutzer seine Daten
                        selbst Dritten zugänglich gemacht hat, haftet der Provider ebenso nicht dafür.
                    </p>


                    <p><b>SCHLUSSBESTIMMUNGEN</b></p>
                    <p>Sämtliche Mitteilungen sind schriftlich an die in der App registrierten Kontaktdaten des Nutzers
                        bzw. auf der Website des Providers angegebenen Adresse zu richten. Gesendete Mitteilungen der
                        App an die Kontaktdaten des Nutzers gelten in jedem Fall als schriftliche Mitteilung und mit
                        deren Absendung als erfolgt und zugestellt.
                    </p>
                    <p>Der Nutzer verpflichtet sich Adressänderungen (Postadresse sowie E-Mail-Adresse) in der
                        Applikation sofort zu aktualisieren. Die zuletzt in der Applikation registrierte Adresse gilt
                        als rechtswirksam.
                    </p>
                    <p><b>GELTENDES RECHT UND GERICHTSSTAND</b></p>
                    <p>Es gilt ausschliesslich schweizerisches Recht. Der Gerichtsstand für alle Verfahren ist der Sitz
                        der App. Sprache ist in Deutsch.
                    </p>
                    <p><b>DATUM</b></p>
                    <p>Diese AGB sind gültig ab dem 21.01.2021.</p>
                    <p>Swiss RegMed Society AG</p>
                    <p>Pfadackerstrasse 9</p>
                    <p>CH, 8957, Spreitenbach AG</p>


                </div>
            </div>
        </div>
    </div>





@endsection







