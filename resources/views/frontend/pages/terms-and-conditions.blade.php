@extends('frontend.layout.master')
@section('title','Terms & Conditions')
@section('description','Regmed App')
@section('keywords', 'Regmed App')
@push('css')
@endpush
@section('content')
    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Terms & Conditions</h3>
            </div>
        </div>
    </div>
    <div class="loginArea">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="formArea loginFormArea">
                    <p><strong>Datenschutzerkl&auml;rung</strong></p>

                    <p><strong>Einleitung</strong></p>

                    <p>Mit der folgenden Datenschutzerkl&auml;rung m&ouml;chten wir Sie dar&uuml;ber aufkl&auml;ren, welche Arten Ihrer personenbezogenen Daten (nachfolgend auch kurz als &quot;Daten&ldquo; bezeichnet) wir zu welchen Zwecken und in welchem Umfang verarbeiten. Die Datenschutzerkl&auml;rung gilt f&uuml;r alle von uns durchgef&uuml;hrten Verarbeitungen personenbezogener Daten, sowohl im Rahmen der Erbringung unserer Leistungen als auch insbesondere auf unseren Webseiten, in mobilen Applikationen sowie innerhalb externer Onlinepr&auml;senzen, wie z.B. unserer Social-Media-Profile (nachfolgend zusammenfassend bezeichnet als &quot;Onlineangebot&ldquo;).</p>

                    <p>Die verwendeten Begriffe sind nicht geschlechtsspezifisch.</p>

                    <p>Stand: 27. Januar 2021</p>

                    <p><strong>Inhalts&uuml;bersicht</strong></p>

                    <ul>
                        <li><a href="#m14">Einleitung</a></li>
                        <li><a href="#m3">Verantwortlicher</a></li>
                        <li><a href="#mOverview">&Uuml;bersicht der Verarbeitungen</a></li>
                        <li><a href="#m13">Massgebliche Rechtsgrundlagen</a></li>
                        <li><a href="#m27">Sicherheitsmassnahmen</a></li>
                        <li><a href="#m25">&Uuml;bermittlung und Offenbarung von personenbezogenen Daten</a></li>
                        <li><a href="#m24">Datenverarbeitung in Drittl&auml;ndern</a></li>
                        <li><a href="#m134">Einsatz von Cookies</a></li>
                        <li><a href="#m225">Bereitstellung des Onlineangebotes und Webhosting</a></li>
                        <li><a href="#m469">Besondere Hinweise zu Applikationen (Apps)</a></li>
                        <li><a href="#m1466">Bezug von Applikationen &uuml;ber Appstores</a></li>
                        <li><a href="#m367">Registrierung, Anmeldung und Nutzerkonto</a></li>
                        <li><a href="#m104">Blogs und Publikationsmedien</a></li>
                        <li><a href="#m182">Kontaktaufnahme</a></li>
                        <li><a href="#m391">Kommunikation via Messenger</a></li>
                        <li><a href="#m17">Newsletter und elektronische Benachrichtigungen</a></li>
                        <li><a href="#m638">Werbliche Kommunikation via E-Mail, Post, Fax oder Telefon</a></li>
                        <li><a href="#m136">Pr&auml;senzen in sozialen Netzwerken (Social Media)</a></li>
                        <li><a href="#m328">Plugins und eingebettete Funktionen sowie Inhalte</a></li>
                        <li><a href="#m723">Planung, Organisation und Hilfswerkzeuge</a></li>
                        <li><a href="#m12">L&ouml;schung von Daten</a></li>
                        <li><a href="#m15">&Auml;nderung und Aktualisierung der Datenschutzerkl&auml;rung</a></li>
                        <li><a href="#m10">Rechte der betroffenen Personen</a></li>
                        <li><a href="#m42">Begriffsdefinitionen</a></li>
                        <li><a href="#m830">Kontaktieren Sie Uns</a></li>
                    </ul>

                    <p><strong>Verantwortlicher</strong></p>

                    <p><strong>&Uuml;bersicht der Verarbeitungen</strong></p>

                    <p>Die nachfolgende &Uuml;bersicht fasst die Arten der verarbeiteten Daten und die Zwecke ihrer Verarbeitung zusammen und verweist auf die betroffenen Personen.</p>

                    <p><strong>Arten der verarbeiteten Daten - Definitionen</strong></p>

                    <ul>
                        <li>Bestandsdaten (z.B. Namen, Adressen).</li>
                        <li>Inhaltsdaten (z.B. Eingaben in Onlineformularen).</li>
                        <li>Kontaktdaten (z.B. E-Mail, Telefonnummern).</li>
                        <li>Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li>Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten).</li>
                        <li>Standortdaten (Angaben zur geografischen Position eines Ger&auml;tes oder einer Person).</li>
                        <li>Vertragsdaten (z.B. Vertragsgegenstand, Laufzeit, Kundenkategorie).</li>
                        <li>Zahlungsdaten (z.B. Bankverbindungen, Rechnungen, Zahlungshistorie).</li>
                    </ul>

                    <p><strong>Kategorien betroffener Personen</strong></p>

                    <ul>
                        <li>Kommunikationspartner.</li>
                        <li>Kunden.</li>
                        <li>Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                    </ul>

                    <p><strong>Zwecke der Verarbeitung</strong></p>

                    <ul>
                        <li>Erbringung vertragliche Leistungen und Kundenservice.</li>
                        <li>Verwaltung und Beantwortung von Anfragen.</li>
                        <li>Bereitstellung unseres Onlineangebotes und Nutzerfreundlichkeit.</li>
                        <li>B&uuml;ro- und Organisationsverfahren.</li>
                        <li>Direktmarketing (z.B. per E-Mail oder postalisch).</li>
                        <li>Feedback (z.B. Sammeln von Feedback via Online-Formular).</li>
                        <li>Interessenbasiertes und verhaltensbezogenes Marketing.</li>
                        <li>Kontaktanfragen und Kommunikation.</li>
                        <li>Profiling (Erstellen von Nutzerprofilen).</li>
                        <li>Remarketing.</li>
                        <li>Reichweitenmessung (z.B. Zugriffsstatistiken, Erkennung wiederkehrender Besucher).</li>
                        <li>Sicherheitsmassnahmen.</li>
                        <li>Tracking (z.B. interessens-/verhaltensbezogenes Profiling, Nutzung von Cookies).</li>
                        <li>Umfragen und Frageb&ouml;gen (z.B. Umfragen mit Eingabem&ouml;glichkeiten, Multiple-Choice-Fragen).</li>
                        <li>Zielgruppenbildung (Bestimmung von f&uuml;r Marketingzwecke relevanten Zielgruppen oder sonstige Ausgabe von Inhalten).</li>
                    </ul>

                    <p><strong>Massgebliche Rechtsgrundlagen</strong></p>

                    <p><strong>Nationale Datenschutzregelungen in der Schweiz</strong>: <strong>Nationale Datenschutzregelungen in der Schweiz</strong>: Zus&auml;tzlich zu den Datenschutzregelungen der Datenschutz-Grundverordnung gelten nationale Regelungen zum Datenschutz in der Schweiz. Hierzu geh&ouml;rt insbesondere das Bundesgesetz zum Datenschutz (DSG). Das DSG gilt insbesondere dann, wenn keine EU/EWG-B&uuml;rger betroffen sind und z.B. nur Daten von Schweizer B&uuml;rgern verarbeitet werden.</p>

                    <p><strong>Datenschutzregelungen der EU</strong></p>

                    <p>Im Folgenden teilen wir die Rechtsgrundlagen der Datenschutzgrundverordnung (DSGVO), auf deren Basis wir die personenbezogenen Daten verarbeiten, mit. Bitte beachten Sie, dass zus&auml;tzlich zu den Regelungen der DSGVO die nationalen Datenschutzvorgaben in Ihrem bzw. unserem Wohn- und Sitzland gelten k&ouml;nnen. Sollten ferner im Einzelfall speziellere Rechtsgrundlagen massgeblich sein, teilen wir Ihnen diese in der Datenschutzerkl&auml;rung mit.</p>

                    <ul>
                        <li><strong>Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO)</strong> - Die betroffene Person hat ihre Einwilligung in die Verarbeitung der sie betreffenden personenbezogenen Daten f&uuml;r einen spezifischen Zweck oder mehrere bestimmte Zwecke gegeben.</li>
                        <li><strong>Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO)</strong> - Die Verarbeitung ist f&uuml;r die Erf&uuml;llung eines Vertrags, dessen Vertragspartei die betroffene Person ist, oder zur Durchf&uuml;hrung vorvertraglicher Massnahmen erforderlich, die auf Anfrage der betroffenen Person erfolgen.</li>
                        <li><strong>Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO)</strong> - Die Verarbeitung ist zur Wahrung der berechtigten Interessen des Verantwortlichen oder eines Dritten erforderlich, sofern nicht die Interessen oder Grundrechte und Grundfreiheiten der betroffenen Person, die den Schutz personenbezogener Daten erfordern, &uuml;berwiegen.</li>
                    </ul>

                    <p><strong>Sicherheitsmassnahmen</strong></p>

                    <p>Wir treffen nach Massgabe der gesetzlichen Vorgaben unter Ber&uuml;cksichtigung des Stands der Technik, der Implementierungskosten und der Art, des Umfangs, der Umst&auml;nde und der Zwecke der Verarbeitung sowie der unterschiedlichen Eintrittswahrscheinlichkeiten und des Ausmasses der Bedrohung der Rechte und Freiheiten nat&uuml;rlicher Personen geeignete technische und organisatorische Massnahmen, um ein dem Risiko angemessenes Schutzniveau zu gew&auml;hrleisten.</p>

                    <p>Zu den Massnahmen geh&ouml;ren insbesondere die Sicherung der Vertraulichkeit, Integrit&auml;t und Verf&uuml;gbarkeit von Daten durch Kontrolle des physischen und elektronischen Zugangs zu den Daten als auch des sie betreffenden Zugriffs, der Eingabe, der Weitergabe, der Sicherung der Verf&uuml;gbarkeit und ihrer Trennung. Des Weiteren haben wir Verfahren eingerichtet, die eine Wahrnehmung von Betroffenenrechten, die L&ouml;schung von Daten und Reaktionen auf die Gef&auml;hrdung der Daten gew&auml;hrleisten. Ferner ber&uuml;cksichtigen wir den Schutz personenbezogener Daten bereits bei der Entwicklung bzw. Auswahl von Hardware, Software sowie Verfahren entsprechend dem Prinzip des Datenschutzes, durch Technikgestaltung und durch datenschutzfreundliche Voreinstellungen.</p>

                    <p><strong>SSL-Verschl&uuml;sselung (https)</strong>: <strong>SSL-Verschl&uuml;sselung (https)</strong>: Um Ihre via unser Online-Angebot &uuml;bermittelten Daten zu sch&uuml;tzen, nutzen wir eine SSL-Verschl&uuml;sselung. Sie erkennen derart verschl&uuml;sselte Verbindungen an dem Pr&auml;fix https:// in der Adresszeile Ihres Browsers. Die Verbindung wird mit einem <a href="https://letsencrypt.org/" target="_blank">Let&#39;s Encrypt Zertifikat</a> verschl&uuml;sselt.</p>

                    <p>&nbsp;</p>

                    <p><strong>&Uuml;bermittlung und Offenbarung von personenbezogenen Daten</strong></p>

                    <p>Im Rahmen unserer Verarbeitung von personenbezogenen Daten kommt es vor, dass die Daten an andere Stellen, Unternehmen, rechtlich selbstst&auml;ndige Organisationseinheiten oder Personen &uuml;bermittelt oder sie ihnen gegen&uuml;ber offengelegt werden. Zu den Empf&auml;ngern dieser Daten k&ouml;nnen z.B. mit IT-Aufgaben beauftragte Dienstleister oder Anbieter von Diensten und Inhalten, die in eine Webseite eingebunden werden, geh&ouml;ren. In solchen Fall beachten wir die gesetzlichen Vorgaben und schliessen insbesondere entsprechende Vertr&auml;ge bzw. Vereinbarungen, die dem Schutz Ihrer Daten dienen, mit den Empf&auml;ngern Ihrer Daten ab.</p>

                    <p><strong>Daten&uuml;bermittlung innerhalb der Unternehmensgruppe</strong>: <strong>Daten&uuml;bermittlung innerhalb der Unternehmensgruppe</strong>:</p>

                    <p>Wir k&ouml;nnen personenbezogene Daten an andere Unternehmen innerhalb unserer Unternehmensgruppe &uuml;bermitteln oder ihnen den Zugriff auf diese Daten gew&auml;hren, sofern diese Weitergabe zu administrativen Zwecken erfolgt.</p>

                    <p><strong>Datenverarbeitung in Drittl&auml;ndern</strong></p>

                    <p>Sofern wir Daten in einem Drittland (d.h., ausserhalb der Europ&auml;ischen Union (EU), des Europ&auml;ischen Wirtschaftsraums (EWR)) verarbeiten oder die Verarbeitung im Rahmen der Inanspruchnahme von Diensten Dritter oder der Offenlegung bzw. &Uuml;bermittlung von Daten an andere Personen, Stellen oder Unternehmen stattfindet, erfolgt dies nur im Einklang mit den gesetzlichen Vorgaben.</p>

                    <p>Vorbehaltlich ausdr&uuml;cklicher Einwilligung oder vertraglich oder gesetzlich erforderlicher &Uuml;bermittlung verarbeiten oder lassen wir die Daten nur in Drittl&auml;ndern mit einem anerkannten Datenschutzniveau, vertraglichen Verpflichtung durch sogenannte Standardschutzklauseln der EU-Kommission, beim Vorliegen von Zertifizierungen oder verbindlicher internen Datenschutzvorschriften verarbeiten (Art. 44 bis 49 DSGVO, Informationsseite der EU-Kommission: <a href="https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection_de" target="_blank">https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection_de</a> ).</p>

                    <p><strong>Einsatz von Cookies</strong></p>

                    <p>Cookies sind Textdateien, die Daten von besuchten Websites oder Domains enthalten und von einem Browser auf dem Computer des Benutzers gespeichert werden. Ein Cookie dient in erster Linie dazu, die Informationen &uuml;ber einen Benutzer w&auml;hrend oder nach seinem Besuch innerhalb eines Onlineangebotes zu speichern. Zu den gespeicherten Angaben k&ouml;nnen z.B. die Spracheinstellungen auf einer Webseite, der Loginstatus, ein Warenkorb oder die Stelle, an der ein Video geschaut wurde, geh&ouml;ren. Zu dem Begriff der Cookies z&auml;hlen wir ferner andere Technologien, die die gleichen Funktionen wie Cookies erf&uuml;llen (z.B., wenn Angaben der Nutzer anhand pseudonymer Onlinekennzeichnungen gespeichert werden, auch als &quot;Nutzer-IDs&quot; bezeichnet)</p>

                    <p><strong>Die folgenden Cookie-Typen und Funktionen werden unterschieden:</strong></p>

                    <ul>
                        <li><strong>Tempor&auml;re Cookies (auch: Session- oder Sitzungs-Cookies):</strong>&nbsp;Tempor&auml;re Cookies werden sp&auml;testens gel&ouml;scht, nachdem ein Nutzer ein Online-Angebot verlassen und seinen Browser geschlossen hat.</li>
                        <li><strong>Permanente Cookies:</strong>&nbsp;Permanente Cookies bleiben auch nach dem Schliessen des Browsers gespeichert. So kann beispielsweise der Login-Status gespeichert oder bevorzugte Inhalte direkt angezeigt werden, wenn der Nutzer eine Website erneut besucht. Ebenso k&ouml;nnen die Interessen von Nutzern, die zur Reichweitenmessung oder zu Marketingzwecken verwendet werden, in einem solchen Cookie gespeichert werden.</li>
                        <li><strong>First-Party-Cookies:</strong>&nbsp;First-Party-Cookies werden von uns selbst gesetzt.</li>
                        <li><strong>Third-Party-Cookies (auch: Drittanbieter-Cookies)</strong>: Drittanbieter-Cookies werden haupts&auml;chlich von Werbetreibenden (sog. Dritten) verwendet, um Benutzerinformationen zu verarbeiten.</li>
                        <li><strong>Notwendige (auch: essentielle oder unbedingt erforderliche) Cookies:</strong> Cookies k&ouml;nnen zum einen f&uuml;r den Betrieb einer Webseite unbedingt erforderlich sein (z.B. um Logins oder andere Nutzereingaben zu speichern oder aus Gr&uuml;nden der Sicherheit).</li>
                        <li><strong>Statistik-, Marketing- und Personalisierungs-Cookies</strong>: Ferner werden Cookies im Regelfall auch im Rahmen der Reichweitenmessung eingesetzt sowie dann, wenn die Interessen eines Nutzers oder sein Verhalten (z.B. Betrachten bestimmter Inhalte, Nutzen von Funktionen etc.) auf einzelnen Webseiten in einem Nutzerprofil gespeichert werden. Solche Profile dienen dazu, den Nutzern z.B. Inhalte anzuzeigen, die ihren potentiellen Interessen entsprechen. Dieses Verfahren wird auch als &quot;Tracking&quot;, d.h., Nachverfolgung der potentiellen Interessen der Nutzer bezeichnet. Soweit wir Cookies oder &quot;Tracking&quot;-Technologien einsetzen, informieren wir Sie gesondert in unserer Datenschutzerkl&auml;rung oder im Rahmen der Einholung einer Einwilligung.</li>
                    </ul>

                    <p><strong>Hinweise zu Rechtsgrundlagen: </strong>Auf welcher Rechtsgrundlage wir Ihre personenbezogenen Daten mit Hilfe von Cookies verarbeiten, h&auml;ngt davon ab, ob wir Sie um eine Einwilligung bitten. Falls dies zutrifft und Sie in die Nutzung von Cookies einwilligen, ist die Rechtsgrundlage der Verarbeitung Ihrer Daten die erkl&auml;rte Einwilligung. Andernfalls werden die mithilfe von Cookies verarbeiteten Daten auf Grundlage unserer berechtigten Interessen (z.B. an einem betriebswirtschaftlichen Betrieb unseres Onlineangebotes und dessen Verbesserung) verarbeitet oder, wenn der Einsatz von Cookies erforderlich ist, um unsere vertraglichen Verpflichtungen zu erf&uuml;llen.</p>

                    <p><strong>Speicherdauer: </strong>Sofern wir Ihnen keine expliziten Angaben zur Speicherdauer von permanenten Cookies mitteilen (z. B. im Rahmen eines sog. Cookie-Opt-Ins), gehen Sie bitte davon aus, dass die Speicherdauer bis zu zwei Jahre betragen kann.</p>

                    <p><strong>Allgemeine Hinweise zum Widerruf und Widerspruch (Opt-Out): </strong>Abh&auml;ngig davon, ob die Verarbeitung auf Grundlage einer Einwilligung oder gesetzlichen Erlaubnis erfolgt, haben Sie jederzeit die M&ouml;glichkeit, eine erteilte Einwilligung zu widerrufen oder der Verarbeitung Ihrer Daten durch Cookie-Technologien zu widersprechen (zusammenfassend als &quot;Opt-Out&quot; bezeichnet). Sie k&ouml;nnen Ihren Widerspruch zun&auml;chst mittels der Einstellungen Ihres Browsers erkl&auml;ren, z.B., indem Sie die Nutzung von Cookies deaktivieren (wobei hierdurch auch die Funktionsf&auml;higkeit unseres Onlineangebotes eingeschr&auml;nkt werden kann). Ein Widerspruch gegen den Einsatz von Cookies zu Zwecken des Onlinemarketings kann auch mittels einer Vielzahl von Diensten, vor allem im Fall des Trackings, &uuml;ber die Webseiten <a href="https://optout.aboutads.info" target="_blank">https://optout.aboutads.info</a> und <a href="https://www.youronlinechoices.com/" target="_blank">https://www.youronlinechoices.com/</a> erkl&auml;rt werden. Daneben k&ouml;nnen Sie weitere Widerspruchshinweise im Rahmen der Angaben zu den eingesetzten Dienstleistern und Cookies erhalten.</p>

                    <p><strong>Verarbeitung von Cookie-Daten auf Grundlage einer Einwilligung</strong>: Wir setzen ein Verfahren zum Cookie-Einwilligungs-Management ein, in dessen Rahmen die Einwilligungen der Nutzer in den Einsatz von Cookies, bzw. der im Rahmen des Cookie-Einwilligungs-Management-Verfahrens genannten Verarbeitungen und Anbieter eingeholt sowie von den Nutzern verwaltet und widerrufen werden k&ouml;nnen. Hierbei wird die Einwilligungserkl&auml;rung gespeichert, um deren Abfrage nicht erneut wiederholen zum m&uuml;ssen und die Einwilligung entsprechend der gesetzlichen Verpflichtung nachweisen zu k&ouml;nnen. Die Speicherung kann serverseitig und/oder in einem Cookie (sogenanntes Opt-In-Cookie, bzw. mithilfe vergleichbarer Technologien) erfolgen, um die Einwilligung einem Nutzer, bzw. dessen Ger&auml;t zuordnen zu k&ouml;nnen. Vorbehaltlich individueller Angaben zu den Anbietern von Cookie-Management-Diensten, gelten die folgenden Hinweise: Die Dauer der Speicherung der Einwilligung kann bis zu zwei Jahren betragen. Hierbei wird ein pseudonymer Nutzer-Identifikator gebildet und mit dem Zeitpunkt der Einwilligung, Angaben zur Reichweite der Einwilligung (z. B. welche Kategorien von Cookies und/oder Diensteanbieter) sowie dem Browser, System und verwendeten Endger&auml;t gespeichert.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p>Die zur Erstellung eines Dienstleistungsangebots notwendigen Daten werden an einen Kooperationspartner (Dienstleister) weitergegeben. Die Unterzeichnenden (durch Email verifizierung) stimmen dem Gebrauch ihrer pers&ouml;nlichen Daten zu den hier vereinbarten Zwecken zu.</p>

                    <p>Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und f&uuml;r den Fall von Anschlussfragen bei uns gespeichert.</p>

                    <p>Diese Daten geben wir nicht ohne Ihre Einwilligung weiter. Die erteilte Einwilligung zur Speicherung der Daten, der E-Mail-Adresse sowie deren Nutzung zum Versand des Newsletters k&ouml;nnen Sie jederzeit widerrufen.</p>

                    <p>Allgemeines: Wir bearbeiten Personendaten (Personendaten sind alle Angaben, die sich auf eine bestimmte oder bestimmbare Person beziehen) f&uuml;r jene Dauer, die f&uuml;r den jeweiligen Zweck oder die jeweiligen Zwecke erforderlich ist, und im Einklang mit dem schweizerischen Datenschutzrecht. Im &Uuml;brigen bearbeiten wir &ndash; soweit und sofern die EU-DSGVO anwendbar ist &ndash; Personendaten gem&auml;ss Rechtsgrundlagen im Zusammenhang mit Art. 6 Abs. 1 DSGVO.</p>

                    <p><strong>Bereitstellung des Onlineangebotes und Webhosting</strong></p>

                    <p>Um unser Onlineangebot sicher und effizient bereitstellen zu k&ouml;nnen, nehmen wir die Leistungen von einem oder mehreren Webhosting-Anbietern in Anspruch, von deren Servern (bzw. von ihnen verwalteten Servern) das Onlineangebot abgerufen werden kann. Zu diesen Zwecken k&ouml;nnen wir Infrastruktur- und Plattformdienstleistungen, Rechenkapazit&auml;t, Speicherplatz und Datenbankdienste sowie Sicherheitsleistungen und technische Wartungsleistungen in Anspruch nehmen.</p>

                    <p>Zu den im Rahmen der Bereitstellung des Hostingangebotes verarbeiteten Daten k&ouml;nnen alle die Nutzer unseres Onlineangebotes betreffenden Angaben geh&ouml;ren, die im Rahmen der Nutzung und der Kommunikation anfallen. Hierzu geh&ouml;ren regelm&auml;ssig die IP-Adresse, die notwendig ist, um die Inhalte von Onlineangeboten an Browser ausliefern zu k&ouml;nnen, und alle innerhalb unseres Onlineangebotes oder von Webseiten get&auml;tigten Eingaben.</p>

                    <p><strong>Erhebung von Zugriffsdaten und Logfiles</strong>: Wir selbst (bzw. unser Webhostinganbieter) erheben Daten zu jedem Zugriff auf den Server (sogenannte Serverlogfiles). Zu den Serverlogfiles k&ouml;nnen die Adresse und Name der abgerufenen Webseiten und Dateien, Datum und Uhrzeit des Abrufs, &uuml;bertragene Datenmengen, Meldung &uuml;ber erfolgreichen Abruf, Browsertyp nebst Version, das Betriebssystem des Nutzers, Referrer URL (die zuvor besuchte Seite) und im Regelfall IP-Adressen und der anfragende Provider geh&ouml;ren.</p>

                    <p>Die Serverlogfiles k&ouml;nnen zum einen zu Zwecken der Sicherheit eingesetzt werden, z.B., um eine &Uuml;berlastung der Server zu vermeiden (insbesondere im Fall von missbr&auml;uchlichen Angriffen, sogenannten DDoS-Attacken) und zum anderen, um die Auslastung der Server und ihre Stabilit&auml;t sicherzustellen.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Inhaltsdaten (z.B. Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Rechtsgrundlagen:</strong> Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Besondere Hinweise zu Applikationen (Apps)</strong></p>

                    <p>Wir verarbeiten die Daten der Nutzer unserer Applikation, soweit diese erforderlich sind, um den Nutzern die Applikation sowie deren Funktionalit&auml;ten bereitstellen, deren Sicherheit &uuml;berwachen und sie weiterentwickeln zu k&ouml;nnen. Wir k&ouml;nnen ferner Nutzer unter Beachtung der gesetzlichen Vorgaben kontaktieren, sofern die Kommunikation zu Zwecken der Administration oder Nutzung der Applikation erforderlich ist. Im &Uuml;brigen verweisen wir im Hinblick auf die Verarbeitung der Daten der Nutzer auf die Datenschutzhinweise in dieser Datenschutzerkl&auml;rung.</p>

                    <p><strong>Rechtsgrundlagen: </strong>Die Verarbeitung von Daten, die f&uuml;r die Bereitstellung der Funktionalit&auml;ten der Applikation erforderlich ist, dient der Erf&uuml;llung von vertraglichen Pflichten.</p>

                    <p><strong>Kommerzielle Nutzung</strong>:</p>

                    <p>Wir verarbeiten die Daten der Nutzer unserer Applikation, angemeldeter und etwaiger Testnutzer (nachfolgend einheitlich als &quot;Nutzer&quot; bezeichnet), um ihnen gegen&uuml;ber unseren vertraglichen Leistungen erbringen zu k&ouml;nnen sowie auf Grundlage berechtigter Interessen, um die Sicherheit unserer Applikation gew&auml;hrleisten und sie weiterzuentwickeln</p>

                    <p><strong>Speicherung eines pseudonymen Identifikators</strong>:</p>

                    <p>Damit wir die Applikation bereitstellen und ihre Funktionsf&auml;higkeit sicherstellen k&ouml;nnen, verwenden wir einen pseudonymen Identifikator.</p>

                    <p><strong>Kein Standortverlauf und keine Bewegungsprofile</strong>:</p>

                    <p>Die Standortdaten werden lediglich punktuell eingesetzt und nicht zur Bildung eines Standortverlaufs benutzt.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen), Zahlungsdaten (z.B. Bankverbindungen, Rechnungen, Zahlungshistorie), Vertragsdaten (z.B. Vertragsgegenstand, Laufzeit, Kundenkategorie), Standortdaten (Angaben zur geografischen Position eines Ger&auml;tes oder einer Person).</li>
                        <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertragliche Leistungen und Kundenservice.</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Bezug von Applikationen &uuml;ber Appstores</strong></p>

                    <p>Der Bezug unserer Applikation erfolgt &uuml;ber &uuml;ber spezielle Online-Plattformen, die von anderen Dienstanbietern betrieben werden, an (so genannte &quot;Appstores&quot;). In diesem Zusammenhang gelten zus&auml;tzlich zu unseren Datenschutzhinweisen die Datenschutzhinweisen der Appstores.</p>

                    <p><strong>Apple App Store</strong>:</p>

                    <p><strong>Google Play</strong>:</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Zahlungsdaten (z.B. Bankverbindungen, Rechnungen, Zahlungshistorie), Kontaktdaten (z.B. E-Mail, Telefonnummern), Vertragsdaten (z.B. Vertragsgegenstand, Laufzeit, Kundenkategorie), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Kunden.</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertragliche Leistungen und Kundenservice.</li>
                        <li><strong>Rechtsgrundlagen:</strong> Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>

                    <ul>
                        <li>602802552.0531915 876157429.8758916 53619614217.5 310889825.68876404 514286515.21428573 241135564.85714287 1347946812.5334685 1920759556.421941 397771031.22689074 3188476027.435 552006382.1286057 994674363.7183099 354408157.0462185 164410306.11670762 8592289988.857143 774022600.4541231 130139553.31856288 1961216793.640244 907407277.2732626 2051934973.4068966</li>
                        <li>1813123073.6285048 12093441330.088236 1518676477.0723684 566749675.2961276 1209488488.996016 1294411139.6706507 2005133080.751613 354532477.0055617 316497166.4122042 1394342009.1283123 1051379993.6243523 2564594000.4863386 357420440.01960784 431079505.56947607 254687374.25833333 1510971628.4309392 608978526.6825558 3007031651.4119496 857298247.7755308 258528888.7477314</li>
                    </ul>

                    <p><strong>Registrierung, Anmeldung und Nutzerkonto</strong></p>

                    <p>Nutzer k&ouml;nnen ein Nutzerkonto anlegen. Im Rahmen der Registrierung werden den Nutzern die erforderlichen Pflichtangaben mitgeteilt und zu Zwecken der Bereitstellung des Nutzerkontos auf Grundlage vertraglicher Pflichterf&uuml;llung verarbeitet. Zu den verarbeiteten Daten geh&ouml;ren insbesondere die Login-Informationen (Name, Passwort sowie eine E-Mail-Adresse). Die im Rahmen der Registrierung eingegebenen Daten werden f&uuml;r die Zwecke der Nutzung des Nutzerkontos und dessen Zwecks verwendet.</p>

                    <p>Die Nutzer k&ouml;nnen &uuml;ber Vorg&auml;nge, die f&uuml;r deren Nutzerkonto relevant sind, wie z.B. technische &Auml;nderungen, per E-Mail informiert werden. Wenn Nutzer ihr Nutzerkonto gek&uuml;ndigt haben, werden deren Daten im Hinblick auf das Nutzerkonto, vorbehaltlich einer gesetzlichen Aufbewahrungspflicht, gel&ouml;scht. Es obliegt den Nutzern, ihre Daten bei erfolgter K&uuml;ndigung vor dem Vertragsende zu sichern. Wir sind berechtigt, s&auml;mtliche w&auml;hrend der Vertragsdauer gespeicherte Daten des Nutzers unwiederbringlich zu l&ouml;schen.</p>

                    <p>Im Rahmen der Inanspruchnahme unserer Registrierungs- und Anmeldefunktionen sowie der Nutzung des Nutzerkontos speichern wir die IP-Adresse und den Zeitpunkt der jeweiligen Nutzerhandlung. Die Speicherung erfolgt auf Grundlage unserer berechtigten Interessen als auch jener der Nutzer an einem Schutz vor Missbrauch und sonstiger unbefugter Nutzung. Eine Weitergabe dieser Daten an Dritte erfolgt grunds&auml;tzlich nicht, es sei denn, sie ist zur Verfolgung unserer Anspr&uuml;che erforderlich oder es besteht eine gesetzliche Verpflichtung hierzu.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertragliche Leistungen und Kundenservice, Sicherheitsmassnahmen, Verwaltung und Beantwortung von Anfragen.</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Blogs und Publikationsmedien</strong></p>

                    <p>Wir nutzen Blogs oder vergleichbare Mittel der Onlinekommunikation und Publikation (nachfolgend &quot;Publikationsmedium&quot;). Die Daten der Leser werden f&uuml;r die Zwecke des Publikationsmediums nur insoweit verarbeitet, als es f&uuml;r dessen Darstellung und die Kommunikation zwischen Autoren und Lesern oder aus Gr&uuml;nden der Sicherheit erforderlich ist. Im &Uuml;brigen verweisen wir auf die Informationen zur Verarbeitung der Besucher unseres Publikationsmediums im Rahmen dieser Datenschutzhinweise.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertragliche Leistungen und Kundenservice, Feedback (z.B. Sammeln von Feedback via Online-Formular), Sicherheitsmassnahmen.</li>
                        <li><strong>Rechtsgrundlagen:</strong> Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>

                    <ul>
                        <li><strong>Wordfence:</strong> Firewall und Sicherheits- sowie Fehlererkennungsfunktionen; Dienstanbieter: Defiant, Inc., 800 5th Ave Ste 4100, Seattle, WA 98104, USA; Website: <a href="https://www.wordfence.com" target="_blank">https://www.wordfence.com</a>; Datenschutzerkl&auml;rung: <a href="https://www.wordfence.com/privacy-policy/" target="_blank">https://www.wordfence.com/privacy-policy/</a>; Standardvertragsklauseln (Gew&auml;hrleistung Datenschutzniveau bei Verarbeitung in Drittl&auml;ndern): <a href="https://www.wordfence.com/gdpr/dpa.pdf" target="_blank">https://www.wordfence.com/gdpr/dpa.pdf</a>.</li>
                    </ul>

                    <p><strong>Kontaktaufnahme</strong></p>

                    <p>Bei der Kontaktaufnahme mit uns (z.B. per Kontaktformular, E-Mail, Telefon oder via soziale Medien) werden die Angaben der anfragenden Personen verarbeitet, soweit dies zur Beantwortung der Kontaktanfragen und etwaiger angefragter Massnahmen erforderlich ist.</p>

                    <p>Die Beantwortung der Kontaktanfragen im Rahmen von vertraglichen oder vorvertraglichen Beziehungen erfolgt zur Erf&uuml;llung unserer vertraglichen Pflichten oder zur Beantwortung von (vor)vertraglichen Anfragen und im &Uuml;brigen auf Grundlage der berechtigten Interessen an der Beantwortung der Anfragen.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen).</li>
                        <li><strong>Betroffene Personen:</strong> Kommunikationspartner.</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Kontaktanfragen und Kommunikation.</li>
                        <li><strong>Rechtsgrundlagen:</strong> Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Kommunikation via Messenger</strong></p>

                    <p>Wir setzen zu Zwecken der Kommunikation Messenger ein und bitten daher darum, die nachfolgenden Hinweise zur Funktionsf&auml;higkeit der Messenger, zur Verschl&uuml;sselung, zur Nutzung der Metadaten der Kommunikation und zu Ihren Widerspruchsm&ouml;glichkeiten zu beachten.</p>

                    <p>Sie k&ouml;nnen uns auch auf alternativen Wegen, z.B. via Telefon oder E-Mail, kontaktieren. Bitte nutzen Sie die Ihnen mitgeteilten Kontaktm&ouml;glichkeiten oder die innerhalb unseres Onlineangebotes angegebenen Kontaktm&ouml;glichkeiten.</p>

                    <p>Im Fall einer Ende-zu-Ende-Verschl&uuml;sselung von Inhalten (d.h., der Inhalt Ihrer Nachricht und Anh&auml;nge) weisen wir darauf hin, dass die Kommunikationsinhalte (d.h., der Inhalt der Nachricht und angeh&auml;ngte Bilder) von Ende zu Ende verschl&uuml;sselt werden. Das bedeutet, dass der Inhalt der Nachrichten nicht einsehbar ist, nicht einmal durch die Messenger-Anbieter selbst. Sie sollten immer eine aktuelle Version der Messenger mit aktivierter Verschl&uuml;sselung nutzen, damit die Verschl&uuml;sselung der Nachrichteninhalte sichergestellt ist.</p>

                    <p>Wir weisen unsere Kommunikationspartner jedoch zus&auml;tzlich darauf hin, dass die Anbieter der Messenger zwar nicht den Inhalt einsehen, aber in Erfahrung bringen k&ouml;nnen, dass und wann Kommunikationspartner mit uns kommunizieren sowie technische Informationen zum verwendeten Ger&auml;t der Kommunikationspartner und je nach Einstellungen ihres Ger&auml;tes auch Standortinformationen (sogenannte Metadaten) verarbeitet werden.</p>

                    <p><strong>Hinweise zu Rechtsgrundlagen: </strong>Sofern wir Kommunikationspartner vor der Kommunikation mit ihnen via Messenger um eine Erlaubnis bitten, ist die Rechtsgrundlage unserer Verarbeitung ihrer Daten deren Einwilligung. Im &Uuml;brigen, falls wir nicht um eine Einwilligung bitten und sie z.B. von sich aus Kontakt mit uns aufnehmen, nutzen wir Messenger im Verh&auml;ltnis zu unseren Vertragspartnern sowie im Rahmen der Vertragsanbahnung als eine vertragliche Massnahme und im Fall anderer Interessenten und Kommunikationspartner auf Grundlage unserer berechtigten Interessen an einer schnellen und effizienten Kommunikation und Erf&uuml;llung der Bed&uuml;rfnisse unser Kommunikationspartner an der Kommunikation via Messenger. Ferner weisen wir Sie darauf hin, dass wir die uns mitgeteilten Kontaktdaten ohne Ihre Einwilligung nicht erstmalig an die Messenger &uuml;bermitteln.</p>

                    <p><strong>Widerruf, Widerspruch und L&ouml;schung:</strong> Sie k&ouml;nnen jederzeit eine erteilte Einwilligung widerrufen und der Kommunikation mit uns via Messenger jederzeit widersprechen. Im Fall der Kommunikation via Messenger l&ouml;schen wir die Nachrichten entsprechend unseren generellen L&ouml;schrichtlinien (d.h. z.B., wie oben beschrieben, nach Ende vertraglicher Beziehungen, im Kontext von Archivierungsvorgaben etc.) und sonst, sobald wir davon ausgehen k&ouml;nnen, etwaige Ausk&uuml;nfte der Kommunikationspartner beantwortet zu haben, wenn kein R&uuml;ckbezug auf eine vorhergehende Konversation zu erwarten ist und der L&ouml;schung keine gesetzlichen Aufbewahrungspflichten entgegenstehen.</p>

                    <p><strong>Vorbehalt des Verweises auf andere Kommunikationswege:</strong> Zum Abschluss m&ouml;chten wir darauf hinweisen, dass wir uns aus Gr&uuml;nden Ihrer Sicherheit vorbehalten, Anfragen &uuml;ber Messenger nicht zu beantworten. Das ist der Fall, wenn z.B. Vertragsinterna besonderer Geheimhaltung bed&uuml;rfen oder eine Antwort &uuml;ber Messenger den formellen Anspr&uuml;chen nicht gen&uuml;gt. In solchen F&auml;llen verweisen wir Sie auf ad&auml;quatere Kommunikationswege.</p>

                    <p><strong>WhatsApp</strong>:</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Kontaktdaten (z.B. E-Mail, Telefonnummern), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen), Inhaltsdaten (z.B. Eingaben in Onlineformularen).</li>
                        <li><strong>Betroffene Personen:</strong> Kommunikationspartner.</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Kontaktanfragen und Kommunikation, Direktmarketing (z.B. per E-Mail oder postalisch).</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>

                    <ul>
                        <li><strong>Signal:</strong> Signal Messenger mit Ende-zu-Ende-Verschl&uuml;sselung; Dienstanbieter: Privacy Signal Messenger, LLC 650 Castro Street, Suite 120-223 Mountain View, CA 94041, USA; Website: <a href="https://signal.org/de" target="_blank">https://signal.org/de</a>; Datenschutzerkl&auml;rung: <a href="https://signal.org/legal/" target="_blank">https://signal.org/legal/</a>.</li>
                        <li><strong>Telegram Broadcasts:</strong> Telegram Broadcasts - Messenger mit Ende-zu-Ende-Verschl&uuml;sselung; Dienstanbieter: Telegram, Dubai; Website: <a href="https://telegram.org/" target="_blank">https://telegram.org/</a>; Datenschutzerkl&auml;rung: <a href="https://telegram.org/privacy" target="_blank">https://telegram.org/privacy</a>.</li>
                    </ul>

                    <p><strong>Newsletter und elektronische Benachrichtigungen</strong></p>

                    <p>Wir versenden Newsletter, E-Mails und weitere elektronische Benachrichtigungen (nachfolgend &quot;Newsletter&ldquo;) nur mit der Einwilligung der Empf&auml;nger oder einer gesetzlichen Erlaubnis. Sofern im Rahmen einer Anmeldung zum Newsletter dessen Inhalte konkret umschrieben werden, sind sie f&uuml;r die Einwilligung der Nutzer massgeblich. Im &Uuml;brigen enthalten unsere Newsletter Informationen zu unseren Leistungen und uns.</p>

                    <p>Um sich zu unseren Newslettern anzumelden, reicht es grunds&auml;tzlich aus, wenn Sie Ihre E-Mail-Adresse angeben. Wir k&ouml;nnen Sie jedoch bitten, einen Namen, zwecks pers&ouml;nlicher Ansprache im Newsletter, oder weitere Angaben, sofern diese f&uuml;r die Zwecke des Newsletters erforderlich sind, zu t&auml;tigen.</p>

                    <p><strong>Double-Opt-In-Verfahren:</strong> Die Anmeldung zu unserem Newsletter erfolgt grunds&auml;tzlich in einem sogenannte Double-Opt-In-Verfahren. D.h., Sie erhalten nach der Anmeldung eine E-Mail, in der Sie um die Best&auml;tigung Ihrer Anmeldung gebeten werden. Diese Best&auml;tigung ist notwendig, damit sich niemand mit fremden E-Mail-Adressen anmelden kann. Die Anmeldungen zum Newsletter werden protokolliert, um den Anmeldeprozess entsprechend den rechtlichen Anforderungen nachweisen zu k&ouml;nnen. Hierzu geh&ouml;rt die Speicherung des Anmelde- und des Best&auml;tigungszeitpunkts als auch der IP-Adresse. Ebenso werden die &Auml;nderungen Ihrer bei dem Versanddienstleister gespeicherten Daten protokolliert.</p>

                    <p><strong>L&ouml;schung und Einschr&auml;nkung der Verarbeitung: </strong>Wir k&ouml;nnen die ausgetragenen E-Mail-Adressen bis zu drei Jahren auf Grundlage unserer berechtigten Interessen speichern, bevor wir sie l&ouml;schen, um eine ehemals gegebene Einwilligung nachweisen zu k&ouml;nnen. Die Verarbeitung dieser Daten wird auf den Zweck einer m&ouml;glichen Abwehr von Anspr&uuml;chen beschr&auml;nkt. Ein individueller L&ouml;schungsantrag ist jederzeit m&ouml;glich, sofern zugleich das ehemalige Bestehen einer Einwilligung best&auml;tigt wird. Im Fall von Pflichten zur dauerhaften Beachtung von Widerspr&uuml;chen behalten wir uns die Speicherung der E-Mail-Adresse alleine zu diesem Zweck in einer Sperrliste (sogenannte &quot;Blocklist&quot;) vor.</p>

                    <p>Die Protokollierung des Anmeldeverfahrens erfolgt auf Grundlage unserer berechtigten Interessen zu Zwecken des Nachweises seines ordnungsgem&auml;ssen Ablaufs. Soweit wir einen Dienstleister mit dem Versand von E-Mails beauftragen, erfolgt dies auf Grundlage unserer berechtigten Interessen an einem effizienten und sicheren Versandsystem.</p>

                    <p><strong>Hinweise zu Rechtsgrundlagen:</strong> Der Versand der Newsletter erfolgt auf Grundlage einer Einwilligung der Empf&auml;nger oder, falls eine Einwilligung nicht erforderlich ist, auf Grundlage unserer berechtigten Interessen am Direktmarketing, sofern und soweit diese gesetzlich, z.B. im Fall von Bestandskundenwerbung, erlaubt ist. Soweit wir einen Dienstleister mit dem Versand von E-Mails beauftragen, geschieht dies auf der Grundlage unserer berechtigten Interessen. Das Registrierungsverfahren wird auf der Grundlage unserer berechtigten Interessen aufgezeichnet, um nachzuweisen, dass es in &Uuml;bereinstimmung mit dem Gesetz durchgef&uuml;hrt wurde.</p>

                    <p><strong>Inhalte:</strong> Informationen zu uns, unseren Leistungen, Aktionen und Angeboten.</p>

                    <p><strong>Messung von &Ouml;ffnungs- und Klickraten</strong>:</p>

                    <p>Die Newsletter enthalten einen sogenannte &quot;web-beacon&ldquo;, d.h., eine pixelgrosse Datei, die beim &Ouml;ffnen des Newsletters von unserem Server, bzw., sofern wir einen Versanddienstleister einsetzen, von dessen Server abgerufen wird. Im Rahmen dieses Abrufs werden zun&auml;chst technische Informationen, wie Informationen zum Browser und Ihrem System, als auch Ihre IP-Adresse und der Zeitpunkt des Abrufs, erhoben.</p>

                    <p>Diese Informationen werden zur technischen Verbesserung unseres Newsletters anhand der technischen Daten oder der Zielgruppen und ihres Leseverhaltens auf Basis ihrer Abruforte (die mit Hilfe der IP-Adresse bestimmbar sind) oder der Zugriffszeiten genutzt. Diese Analyse beinhaltet ebenfalls die Feststellung, ob die Newsletter ge&ouml;ffnet werden, wann sie ge&ouml;ffnet werden und welche Links benutzt werden.</p>

                    <p><strong>Voraussetzung der Inanspruchnahme kostenloser Leistungen</strong>: Die Einwilligungen in den Versand von Mailings kann als Voraussetzung zur Inanspruchnahme kostenloser Leistungen (z.B. Zugang zu bestimmten Inhalten oder Teilnahme an bestimmten Aktionen) abh&auml;ngig gemacht werden. Sofern die Nutzer die kostenlose Leistung in Anspruch nehmen m&ouml;chten, ohne sich zum Newsletter anzumelden, bitten wir Sie um eine Kontaktaufnahme.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten).</li>
                        <li><strong>Betroffene Personen:</strong> Kommunikationspartner, Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Direktmarketing (z.B. per E-Mail oder postalisch), Erbringung vertragliche Leistungen und Kundenservice.</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                        <li><strong>Widerspruchsm&ouml;glichkeit (Opt-Out):</strong> Sie k&ouml;nnen den Empfang unseres Newsletters jederzeit k&uuml;ndigen, d.h. Ihre Einwilligungen widerrufen, bzw. dem weiteren Empfang widersprechen. Einen Link zur K&uuml;ndigung des Newsletters finden Sie entweder am Ende eines jeden Newsletters oder k&ouml;nnen sonst eine der oben angegebenen Kontaktm&ouml;glichkeiten, vorzugsw&uuml;rdig E-Mail, hierzu nutzen.</li>
                    </ul>

                    <p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>

                    <ul>
                        <li><strong>HubSpot:</strong> E-Mail-Marketing-Plattform; Dienstanbieter: HubSpot, Inc., 25 First St., 2nd floor, Cambridge, Massachusetts 02141, USA; Website: <a href="https://www.hubspot.de" target="_blank">https://www.hubspot.de</a>; Datenschutzerkl&auml;rung: <a href="https://legal.hubspot.com/de/privacy-policy" target="_blank">https://legal.hubspot.com/de/privacy-policy</a>.</li>
                        <li><strong>Mailchimp:</strong> E-Mail-Marketing-Plattform; Dienstanbieter: &quot;Mailchimp&quot; - Rocket Science Group, LLC, 675 Ponce De Leon Ave NE #5000, Atlanta, GA 30308, USA; Website: <a href="https://mailchimp.com" target="_blank">https://mailchimp.com</a>; Datenschutzerkl&auml;rung: <a href="https://mailchimp.com/legal/privacy/" target="_blank">https://mailchimp.com/legal/privacy/</a>.</li>
                    </ul>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p><strong>Werbliche Kommunikation via E-Mail, Post, Fax oder Telefon</strong></p>

                    <p>Wir verarbeiten personenbezogene Daten zu Zwecken der werblichen Kommunikation, die &uuml;ber diverse Kan&auml;le, wie z.B. E-Mail, Telefon, Post oder Fax, entsprechend den gesetzlichen Vorgaben erfolgen kann.</p>

                    <p>Die Empf&auml;nger haben das Recht, erteilte Einwilligungen jederzeit zu widerrufen oder der werblichen Kommunikation jederzeit zu widersprechen.</p>

                    <p>Nach Widerruf bzw. Widerspruch k&ouml;nnen wir die zum Nachweis der Einwilligung erforderlichen Daten bis zu drei Jahren auf Grundlage unserer berechtigten Interessen speichern, bevor wir sie l&ouml;schen. Die Verarbeitung dieser Daten wird auf den Zweck einer m&ouml;glichen Abwehr von Anspr&uuml;chen beschr&auml;nkt. Ein individueller L&ouml;schungsantrag ist jederzeit m&ouml;glich, sofern zugleich das ehemalige Bestehen einer Einwilligung best&auml;tigt wird.</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern).</li>
                        <li><strong>Betroffene Personen:</strong> Kommunikationspartner.</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Direktmarketing (z.B. per E-Mail oder postalisch).</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Pr&auml;senzen in sozialen Netzwerken (Social Media)</strong></p>

                    <p>Wir unterhalten Onlinepr&auml;senzen innerhalb sozialer Netzwerke und verarbeiten in diesem Rahmen Daten der Nutzer, um mit den dort aktiven Nutzern zu kommunizieren oder um Informationen &uuml;ber uns anzubieten.</p>

                    <p>Wir weisen darauf hin, dass dabei Daten der Nutzer ausserhalb des Raumes der Europ&auml;ischen Union verarbeitet werden k&ouml;nnen. Hierdurch k&ouml;nnen sich f&uuml;r die Nutzer Risiken ergeben, weil so z.B. die Durchsetzung der Rechte der Nutzer erschwert werden k&ouml;nnte.</p>

                    <p>Ferner werden die Daten der Nutzer innerhalb sozialer Netzwerke im Regelfall f&uuml;r Marktforschungs- und Werbezwecke verarbeitet. So k&ouml;nnen z.B. anhand des Nutzungsverhaltens und sich daraus ergebender Interessen der Nutzer Nutzungsprofile erstellt werden. Die Nutzungsprofile k&ouml;nnen wiederum verwendet werden, um z.B. Werbeanzeigen innerhalb und ausserhalb der Netzwerke zu schalten, die mutmasslich den Interessen der Nutzer entsprechen. Zu diesen Zwecken werden im Regelfall Cookies auf den Rechnern der Nutzer gespeichert, in denen das Nutzungsverhalten und die Interessen der Nutzer gespeichert werden. Ferner k&ouml;nnen in den Nutzungsprofilen auch Daten unabh&auml;ngig der von den Nutzern verwendeten Ger&auml;te gespeichert werden (insbesondere, wenn die Nutzer Mitglieder der jeweiligen Plattformen sind und bei diesen eingeloggt sind).</p>

                    <p>F&uuml;r eine detaillierte Darstellung der jeweiligen Verarbeitungsformen und der Widerspruchsm&ouml;glichkeiten (Opt-Out) verweisen wir auf die Datenschutzerkl&auml;rungen und Angaben der Betreiber der jeweiligen Netzwerke.</p>

                    <p>Auch im Fall von Auskunftsanfragen und der Geltendmachung von Betroffenenrechten weisen wir darauf hin, dass diese am effektivsten bei den Anbietern geltend gemacht werden k&ouml;nnen. Nur die Anbieter haben jeweils Zugriff auf die Daten der Nutzer und k&ouml;nnen direkt entsprechende Massnahmen ergreifen und Ausk&uuml;nfte geben. Sollten Sie dennoch Hilfe ben&ouml;tigen, dann k&ouml;nnen Sie sich an uns wenden.</p>

                    <p><strong>Facebook</strong>: Wir sind gemeinsam mit Facebook Irland Ltd. f&uuml;r die Erhebung (jedoch nicht die weitere Verarbeitung) von Daten der Besucher unserer Facebook-Seite (sog. &quot;Fanpage&quot;) verantwortlich. Zu diesen Daten geh&ouml;ren Informationen zu den Arten von Inhalten, die Nutzer sich ansehen oder mit denen sie interagieren, oder die von ihnen vorgenommenen Handlungen (siehe unter &bdquo;Von dir und anderen get&auml;tigte und bereitgestellte Dinge&ldquo; in der Facebook-Datenrichtlinie: <a href="https://www.facebook.com/policy" target="_blank">https://www.facebook.com/policy</a>), sowie Informationen &uuml;ber die von den Nutzern genutzten Ger&auml;te (z. B. IP-Adressen, Betriebssystem, Browsertyp, Spracheinstellungen, Cookie-Daten; siehe unter &bdquo;Ger&auml;teinformationen&ldquo; in der Facebook-Datenrichtlinie-erkl&auml;rung: <a href="https://www.facebook.com/policy" target="_blank">https://www.facebook.com/policy</a>). Wie in der Facebook-Datenrichtlinie unter &bdquo;Wie verwenden wir diese Informationen?&ldquo; erl&auml;utert, erhebt und verwendet Facebook Informationen auch, um Analysedienste, so genannte &quot;Seiten-Insights&quot;, f&uuml;r Seitenbetreiber bereitzustellen, damit diese Erkenntnisse dar&uuml;ber erhalten, wie Personen mit ihren Seiten und mit den mit ihnen verbundenen Inhalten interagieren. Wir haben mit Facebook eine spezielle Vereinbarung abgeschlossen (&quot;Informationen zu Seiten-Insights&quot;, <a href="https://www.facebook.com/legal/terms/page_controller_addendum" target="_blank">https://www.facebook.com/legal/terms/page_controller_addendum</a>), in der insbesondere geregelt wird, welche Sicherheitsmassnahmen Facebook beachten muss und in der Facebook sich bereit erkl&auml;rt hat die Betroffenenrechte zu erf&uuml;llen (d. h. Nutzer k&ouml;nnen z. B. Ausk&uuml;nfte oder L&ouml;schungsanfragen direkt an Facebook richten). Die Rechte der Nutzer (insbesondere auf Auskunft, L&ouml;schung, Widerspruch und Beschwerde bei zust&auml;ndiger Aufsichtsbeh&ouml;rde), werden durch die Vereinbarungen mit Facebook nicht eingeschr&auml;nkt. Weitere Hinweise finden sich in den &quot;Informationen zu Seiten-Insights&quot; (<a href="https://www.facebook.com/legal/terms/information_about_page_insights_data" target="_blank">https://www.facebook.com/legal/terms/information_about_page_insights_data</a>).</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Kontaktanfragen und Kommunikation, Tracking (z.B. interessens-/verhaltensbezogenes Profiling, Nutzung von Cookies), Remarketing, Reichweitenmessung (z.B. Zugriffsstatistiken, Erkennung wiederkehrender Besucher).</li>
                        <li><strong>Rechtsgrundlagen:</strong> Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>

                    <ul>
                        <li><strong>Instagram:</strong> Soziales Netzwerk; Dienstanbieter: Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA, Mutterunternehmen: Facebook, 1 Hacker Way, Menlo Park, CA 94025, USA; Website: <a href="https://www.instagram.com" target="_blank">https://www.instagram.com</a>; Datenschutzerkl&auml;rung: <a href="https://instagram.com/about/legal/privacy" target="_blank">https://instagram.com/about/legal/privacy</a>.</li>
                        <li><strong>Facebook:</strong> Soziales Netzwerk; Dienstanbieter: Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland, Mutterunternehmen: Facebook, 1 Hacker Way, Menlo Park, CA 94025, USA; Website: <a href="https://www.facebook.com" target="_blank">https://www.facebook.com</a>; Datenschutzerkl&auml;rung: <a href="https://www.facebook.com/about/privacy" target="_blank">https://www.facebook.com/about/privacy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): Einstellungen f&uuml;r Werbeanzeigen: <a href="https://www.facebook.com/settings?tab=ads" target="_blank">https://www.facebook.com/settings?tab=ads</a>.</li>
                        <li><strong>LinkedIn:</strong> Soziales Netzwerk; Dienstanbieter: LinkedIn Ireland Unlimited Company, Wilton Place, Dublin 2, Irland; Website: <a href="https://www.linkedin.com" target="_blank">https://www.linkedin.com</a>; Datenschutzerkl&auml;rung: <a href="https://www.linkedin.com/legal/privacy-policy" target="_blank">https://www.linkedin.com/legal/privacy-policy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): <a href="https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out" target="_blank">https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out</a>.</li>
                        <li><strong>Pinterest:</strong> Soziales Netzwerk; Dienstanbieter: Pinterest Inc., 635 High Street, Palo Alto, CA, 94301, USA,; Website: <a href="https://www.pinterest.com" target="_blank">https://www.pinterest.com</a>; Datenschutzerkl&auml;rung: <a href="https://about.pinterest.com/de/privacy-policy" target="_blank">https://about.pinterest.com/de/privacy-policy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): <a href="https://about.pinterest.com/de/privacy-policy" target="_blank">https://about.pinterest.com/de/privacy-policy</a>.</li>
                        <li><strong>Twitter:</strong> Soziales Netzwerk; Dienstanbieter: Twitter International Company, One Cumberland Place, Fenian Street, Dublin 2 D02 AX07, Irland, Mutterunternehmen: Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA; Datenschutzerkl&auml;rung: <a href="https://twitter.com/de/privacy" target="_blank">https://twitter.com/de/privacy</a>, (Einstellungen) <a href="https://twitter.com/personalization" target="_blank">https://twitter.com/personalization</a>.</li>
                        <li><strong>YouTube:</strong> Soziales Netzwerk und Videoplattform; Dienstanbieter: Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; Datenschutzerkl&auml;rung: <a href="https://policies.google.com/privacy" target="_blank">https://policies.google.com/privacy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): <a href="https://adssettings.google.com/authenticated" target="_blank">https://adssettings.google.com/authenticated</a>.</li>
                    </ul>

                    <p><strong>Plugins und eingebettete Funktionen sowie Inhalte</strong></p>

                    <p>Wir binden in unser Onlineangebot Funktions- und Inhaltselemente ein, die von den Servern ihrer jeweiligen Anbieter (nachfolgend bezeichnet als &quot;Drittanbieter&rdquo;) bezogen werden. Dabei kann es sich zum Beispiel um Grafiken, Videos oder Social-Media-Schaltfl&auml;chen sowie Beitr&auml;ge handeln (nachfolgend einheitlich bezeichnet als &quot;Inhalte&rdquo;).</p>

                    <p>Die Einbindung setzt immer voraus, dass die Drittanbieter dieser Inhalte die IP-Adresse der Nutzer verarbeiten, da sie ohne die IP-Adresse die Inhalte nicht an deren Browser senden k&ouml;nnten. Die IP-Adresse ist damit f&uuml;r die Darstellung dieser Inhalte oder Funktionen erforderlich. Wir bem&uuml;hen uns, nur solche Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur Auslieferung der Inhalte verwenden. Drittanbieter k&ouml;nnen ferner sogenannte Pixel-Tags (unsichtbare Grafiken, auch als &quot;Web Beacons&quot; bezeichnet) f&uuml;r statistische oder Marketingzwecke verwenden. Durch die &quot;Pixel-Tags&quot; k&ouml;nnen Informationen, wie der Besucherverkehr auf den Seiten dieser Webseite, ausgewertet werden. Die pseudonymen Informationen k&ouml;nnen ferner in Cookies auf dem Ger&auml;t der Nutzer gespeichert werden und unter anderem technische Informationen zum Browser und zum Betriebssystem, zu verweisenden Webseiten, zur Besuchszeit sowie weitere Angaben zur Nutzung unseres Onlineangebotes enthalten als auch mit solchen Informationen aus anderen Quellen verbunden werden.</p>

                    <p><strong>Hinweise zu Rechtsgrundlagen:</strong> Sofern wir die Nutzer um deren Einwilligung in den Einsatz der Drittanbieter bitten, ist die Rechtsgrundlage der Verarbeitung von Daten die Einwilligung. Ansonsten werden die Daten der Nutzer auf Grundlage unserer berechtigten Interessen (d.h. Interesse an effizienten, wirtschaftlichen und empf&auml;ngerfreundlichen Leistungen) verarbeitet. In diesem Zusammenhang m&ouml;chten wir Sie auch auf die Informationen zur Verwendung von Cookies in dieser Datenschutzerkl&auml;rung hinweisen.</p>

                    <p><strong>Facebook-Plugins und -Inhalte</strong>: Wir sind gemeinsam mit Facebook Irland Ltd. f&uuml;r die Erhebung oder den Erhalt im Rahmen einer &Uuml;bermittlung (jedoch nicht die weitere Verarbeitung) von &quot;Event-Daten&quot;, die Facebook mittels der Facebook-Social-Plugins (und Einbettungsfunktionen f&uuml;r Inhalte), die auf unserem Onlineangebot ausgef&uuml;hrt werden, erhebt oder im Rahmen einer &Uuml;bermittlung zu folgenden Zwecken erh&auml;lt, gemeinsam verantwortlich: a) Anzeige von Inhalten sowie Werbeinformationen, die den mutmasslichen Interessen der Nutzer entsprechen; b) Zustellung kommerzieller und transaktionsbezogener Nachrichten (z. B. Ansprache von Nutzern via Facebook-Messenger); c) Verbesserung der Anzeigenauslieferung und Personalisierung von Funktionen und Inhalten (z. B. Verbesserung der Erkennung, welche Inhalte oder Werbeinformationen mutmasslich den Interessen der Nutzer entsprechen). Wir haben mit Facebook eine spezielle Vereinbarung abgeschlossen (&quot;Zusatz f&uuml;r Verantwortliche&quot;, <a href="https://www.facebook.com/legal/controller_addendum" target="_blank">https://www.facebook.com/legal/controller_addendum</a>), in der insbesondere geregelt wird, welche Sicherheitsmassnahmen Facebook beachten muss (<a href="https://www.facebook.com/legal/terms/data_security_terms" target="_blank">https://www.facebook.com/legal/terms/data_security_terms</a>) und in der Facebook sich bereit erkl&auml;rt hat die Betroffenenrechte zu erf&uuml;llen (d. h. Nutzer k&ouml;nnen z. B. Ausk&uuml;nfte oder L&ouml;schungsanfragen direkt an Facebook richten). Hinweis: Wenn Facebook uns Messwerte, Analysen und Berichte bereitstellt (die aggregiert sind, d. h. keine Angaben zu einzelnen Nutzern erhalten und f&uuml;r uns anonym sind), dann erfolgt diese Verarbeitung nicht im Rahmen der gemeinsamen Verantwortlichkeit, sondern auf Grundlage eines Auftragsverarbeitungsvertrages (&quot;Datenverarbeitungsbedingungen &quot;, <a href="https://www.facebook.com/legal/terms/dataprocessing" target="_blank">https://www.facebook.com/legal/terms/dataprocessing</a>) , der &quot;Datensicherheitsbedingungen&quot; (<a href="https://www.facebook.com/legal/terms/data_security_terms" target="_blank">https://www.facebook.com/legal/terms/data_security_terms</a>) sowie im Hinblick auf die Verarbeitung in den USA auf Grundlage von Standardvertragsklauseln (&quot;Facebook-EU-Daten&uuml;bermittlungszusatz, <a href="https://www.facebook.com/legal/EU_data_transfer_addendum" target="_blank">https://www.facebook.com/legal/EU_data_transfer_addendum</a>). Die Rechte der Nutzer (insbesondere auf Auskunft, L&ouml;schung, Widerspruch und Beschwerde bei zust&auml;ndiger Aufsichtsbeh&ouml;rde), werden durch die Vereinbarungen mit Facebook nicht eingeschr&auml;nkt.</p>

                    <p><strong>Google Maps APIs und SDKs</strong>:</p>

                    <p><strong>Instagram-Plugins und -Inhalte</strong>: Wir sind gemeinsam mit Facebook Irland Ltd. f&uuml;r die Erhebung oder den Erhalt im Rahmen einer &Uuml;bermittlung (jedoch nicht die weitere Verarbeitung) von &quot;Event-Daten&quot;, die Facebook mittels Funktionen von Instagram (z. B. Einbettungsfunktionen f&uuml;r Inhalte), die auf unserem Onlineangebot ausgef&uuml;hrt werden, erhebt oder im Rahmen einer &Uuml;bermittlung zu folgenden Zwecken erh&auml;lt, gemeinsam verantwortlich: a) Anzeige von Inhalten sowie Werbeinformationen, die den mutmasslichen Interessen der Nutzer entsprechen; b) Zustellung kommerzieller und transaktionsbezogener Nachrichten (z. B. Ansprache von Nutzern via Facebook-Messenger); c) Verbesserung der Anzeigenauslieferung und Personalisierung von Funktionen und Inhalten (z. B. Verbesserung der Erkennung, welche Inhalte oder Werbeinformationen mutmasslich den Interessen der Nutzer entsprechen). Wir haben mit Facebook eine spezielle Vereinbarung abgeschlossen (&quot;Zusatz f&uuml;r Verantwortliche&quot;, <a href="https://www.facebook.com/legal/controller_addendum" target="_blank">https://www.facebook.com/legal/controller_addendum</a>), in der insbesondere geregelt wird, welche Sicherheitsmassnahmen Facebook beachten muss (<a href="https://www.facebook.com/legal/terms/data_security_terms" target="_blank">https://www.facebook.com/legal/terms/data_security_terms</a>) und in der Facebook sich bereit erkl&auml;rt hat die Betroffenenrechte zu erf&uuml;llen (d. h. Nutzer k&ouml;nnen z. B. Ausk&uuml;nfte oder L&ouml;schungsanfragen direkt an Facebook richten). Hinweis: Wenn Facebook uns Messwerte, Analysen und Berichte bereitstellt (die aggregiert sind, d. h. keine Angaben zu einzelnen Nutzern erhalten und f&uuml;r uns anonym sind), dann erfolgt diese Verarbeitung nicht im Rahmen der gemeinsamen Verantwortlichkeit, sondern auf Grundlage eines Auftragsverarbeitungsvertrages (&quot;Datenverarbeitungsbedingungen &quot;, <a href="https://www.facebook.com/legal/terms/dataprocessing" target="_blank">https://www.facebook.com/legal/terms/dataprocessing</a>) , der &quot;Datensicherheitsbedingungen&quot; (<a href="https://www.facebook.com/legal/terms/data_security_terms" target="_blank">https://www.facebook.com/legal/terms/data_security_terms</a>) sowie im Hinblick auf die Verarbeitung in den USA auf Grundlage von Standardvertragsklauseln (&quot;Facebook-EU-Daten&uuml;bermittlungszusatz, <a href="https://www.facebook.com/legal/EU_data_transfer_addendum" target="_blank">https://www.facebook.com/legal/EU_data_transfer_addendum</a>). Die Rechte der Nutzer (insbesondere auf Auskunft, L&ouml;schung, Widerspruch und Beschwerde bei zust&auml;ndiger Aufsichtsbeh&ouml;rde), werden durch die Vereinbarungen mit Facebook nicht eingeschr&auml;nkt.</p>

                    <p><strong>reCAPTCHA</strong>:</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen), Event-Daten (Facebook) (&quot;Event-Daten&quot; sind Daten, die z. B. via Facebook-Pixel (via Apps oder auf anderen Wegen) von uns an Facebook &uuml;bermittelt werden k&ouml;nnen und sich auf Personen oder deren Handlungen beziehen; Zu den Daten geh&ouml;ren z. B. Angaben &uuml;ber Besuche auf Websites, Interaktionen mit Inhalten, Funktionen, Installationen von Apps, K&auml;ufe von Produkten, etc.; die Event-Daten werden zwecks Bildung von Zielgruppen f&uuml;r Inhalte und Werbeinformationen (Custom Audiences) verarbeitet; Event Daten beinhalten nicht die eigentlichen Inhalte (wie z. B. verfasste Kommentare), keine Login-Informationen und keine Kontaktinformationen (also keine Namen, E-Mail-Adressen und Telefonnummern). Event Daten werden durch Facebook nach maximal zwei Jahren gel&ouml;scht, die aus ihnen gebildeten Zielgruppen mit der L&ouml;schung unseres Facebook-Kontos), Standortdaten (Angaben zur geografischen Position eines Ger&auml;tes oder einer Person), Bestandsdaten (z.B. Namen, Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten), Kommunikationspartner.</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> Bereitstellung unseres Onlineangebotes und Nutzerfreundlichkeit, Tracking (z.B. interessens-/verhaltensbezogenes Profiling, Nutzung von Cookies), Feedback (z.B. Sammeln von Feedback via Online-Formular), Erbringung vertragliche Leistungen und Kundenservice, Kontaktanfragen und Kommunikation, Direktmarketing (z.B. per E-Mail oder postalisch), Interessenbasiertes und verhaltensbezogenes Marketing, Profiling (Erstellen von Nutzerprofilen), Sicherheitsmassnahmen, Verwaltung und Beantwortung von Anfragen.</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO), Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO).</li>
                    </ul>

                    <p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>

                    <ul>
                        <li><strong>AddThis:</strong> AddThis - Funktionen zum Teilen von Inhalten in sozialen Netzwerken, AddThis nutzt die personenbezogenen Informationen der Nutzer f&uuml;r die Zurverf&uuml;gungstellung und das Ausf&uuml;hren der Sharing-Funktionen. Dar&uuml;ber hinaus kann AddThis pseudonyme Informationen der Nutzer zu Marketingzwecken nutzen. ; Dienstanbieter: AddThis, 1595 Spring Hill Rd Suite 300 Vienna, VA 22182, USA; Website: <a href="https://www.addthis.com" target="_blank">https://www.addthis.com</a>; Datenschutzerkl&auml;rung: <a href="https://www.addthis.com/privacy" target="_blank">https://www.addthis.com/privacy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): <a href="https://www.addthis.com/privacy/opt-out" target="_blank">https://www.addthis.com/privacy/opt-out</a>.</li>
                        <li><strong>Facebook-Plugins und -Inhalte:</strong> Facebook Social Plugins und Inhalte - Hierzu k&ouml;nnen z.B. Inhalte wie Bilder, Videos oder Texte und Schaltfl&auml;chen geh&ouml;ren, mit denen Nutzer Inhalte dieses Onlineangebotes innerhalb von Facebook teilen k&ouml;nnen. Die Liste und das Aussehen der Facebook Social Plugins k&ouml;nnen hier eingesehen werden: <a href="https://developers.facebook.com/docs/plugins/" target="_blank">https://developers.facebook.com/docs/plugins/</a>; Dienstanbieter: Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland, Mutterunternehmen: Facebook, 1 Hacker Way, Menlo Park, CA 94025, USA; Website: <a href="https://www.facebook.com" target="_blank">https://www.facebook.com</a>; Datenschutzerkl&auml;rung: <a href="https://www.facebook.com/about/privacy" target="_blank">https://www.facebook.com/about/privacy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): Einstellungen f&uuml;r Werbeanzeigen: <a href="https://www.facebook.com/settings?tab=ads" target="_blank">https://www.facebook.com/settings?tab=ads</a>.</li>
                        <li><strong>Google Fonts:</strong> Wir binden die Schriftarten (&quot;Google Fonts&quot;) des Anbieters Google ein, wobei die Daten der Nutzer allein zu Zwecken der Darstellung der Schriftarten im Browser der Nutzer verwendet werden. Die Einbindung erfolgt auf Grundlage unserer berechtigten Interessen an einer technisch sicheren, wartungsfreien und effizienten Nutzung von Schriftarten, deren einheitlicher Darstellung sowie unter Ber&uuml;cksichtigung m&ouml;glicher lizenzrechtlicher Restriktionen f&uuml;r deren Einbindung. Dienstanbieter: Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; Website: <a href="https://fonts.google.com/" target="_blank">https://fonts.google.com/</a>; Datenschutzerkl&auml;rung: <a href="https://policies.google.com/privacy" target="_blank">https://policies.google.com/privacy</a>.</li>
                        <li><strong>Google Maps:</strong> Wir binden die Landkarten des Dienstes &ldquo;Google Maps&rdquo; des Anbieters Google ein. Zu den verarbeiteten Daten k&ouml;nnen insbesondere IP-Adressen und Standortdaten der Nutzer geh&ouml;ren, die jedoch nicht ohne deren Einwilligung (im Regelfall im Rahmen der Einstellungen ihrer Mobilger&auml;te vollzogen), erhoben werden; Dienstanbieter: Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; Website: <a href="https://cloud.google.com/maps-platform" target="_blank">https://cloud.google.com/maps-platform</a>; Datenschutzerkl&auml;rung: <a href="https://policies.google.com/privacy" target="_blank">https://policies.google.com/privacy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): Opt-Out-Plugin: <a href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=de</a>, Einstellungen f&uuml;r die Darstellung von Werbeeinblendungen: <a href="https://adssettings.google.com/authenticated" target="_blank">https://adssettings.google.com/authenticated</a>.</li>
                        <li><strong>Instagram-Plugins und -Inhalte:</strong> Instagram Plugins und -Inhalte - Hierzu k&ouml;nnen z.B. Inhalte wie Bilder, Videos oder Texte und Schaltfl&auml;chen geh&ouml;ren, mit denen Nutzer Inhalte dieses Onlineangebotes innerhalb von Instagram teilen k&ouml;nnen. Dienstanbieter: <a href="https://www.instagram.com" target="_blank">https://www.instagram.com</a>, Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA; Website: <a href="https://www.instagram.com" target="_blank">https://www.instagram.com</a>; Datenschutzerkl&auml;rung: <a href="https://instagram.com/about/legal/privacy" target="_blank">https://instagram.com/about/legal/privacy</a>.</li>
                        <li><strong>LinkedIn-Plugins und -Inhalte:</strong> LinkedIn-Plugins und -Inhalte- Hierzu k&ouml;nnen z.B. Inhalte wie Bilder, Videos oder Texte und Schaltfl&auml;chen geh&ouml;ren, mit denen Nutzer Inhalte dieses Onlineangebotes innerhalb von LinkedIn teilen k&ouml;nnen. Dienstanbieter: LinkedIn Ireland Unlimited Company, Wilton Place, Dublin 2, Irland; Website: <a href="https://www.linkedin.com" target="_blank">https://www.linkedin.com</a>; Datenschutzerkl&auml;rung: <a href="https://www.linkedin.com/legal/privacy-policy" target="_blank">https://www.linkedin.com/legal/privacy-policy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): <a href="https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out" target="_blank">https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out</a>.</li>
                        <li><strong>MyFonts:</strong> Schriftarten; im Rahmen des Schriftartenabrufs verarbeitete Daten die Identifikationsnummer des Webfont-Projektes (anonymisiert), die URL der lizenzierten Website, die mit einer Kundennummer verkn&uuml;pft ist, um den Lizenznehmer und die lizenzierten Webfonts zu identifizieren, und die Referrer URL; die anonymisierte Webfont-Projekt-Identifikationsnummer wird in verschl&uuml;sselten Protokolldateien mit solchen Daten f&uuml;r 30 Tage, um die monatliche Anzahl der Seitenaufrufe zu ermitteln, gespeichert; Nach einer solchen Extraktion und Speicherung der Anzahl der Seitenaufrufe werden die Protokolldateien gel&ouml;scht; Dienstanbieter: Monotype Imaging Holdings Inc., 600 Unicorn Park Drive, Woburn, Massachusetts 01801, USA; Website: <a href="https://www.myfonts.co" target="_blank">https://www.myfonts.co</a>; Datenschutzerkl&auml;rung: <a href="https://www.myfonts.com/info/legal/#Privacy" target="_blank">https://www.myfonts.com/info/legal/#Privacy</a>.</li>
                        <li><strong>Twitter-Plugins und -Inhalte:</strong> Twitter Plugins und -Schaltfl&auml;chen - Hierzu k&ouml;nnen z.B. Inhalte wie Bilder, Videos oder Texte und Schaltfl&auml;chen geh&ouml;ren, mit denen Nutzer Inhalte dieses Onlineangebotes innerhalb von Twitter teilen k&ouml;nnen. Dienstanbieter: Twitter International Company, One Cumberland Place, Fenian Street, Dublin 2 D02 AX07, Irland, Mutterunternehmen: Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA; Website: <a href="https://twitter.com/de" target="_blank">https://twitter.com/de</a>; Datenschutzerkl&auml;rung: <a href="https://twitter.com/de/privacy" target="_blank">https://twitter.com/de/privacy</a>.</li>
                        <li><strong>YouTube-Videos:</strong> Videoinhalte; Dienstanbieter: Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; Website: <a href="https://www.youtube.com" target="_blank">https://www.youtube.com</a>; Datenschutzerkl&auml;rung: <a href="https://policies.google.com/privacy" target="_blank">https://policies.google.com/privacy</a>; Widerspruchsm&ouml;glichkeit (Opt-Out): Opt-Out-Plugin: <a href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=de</a>, Einstellungen f&uuml;r die Darstellung von Werbeeinblendungen: <a href="https://adssettings.google.com/authenticated" target="_blank">https://adssettings.google.com/authenticated</a>.</li>
                    </ul>

                    <p><strong>Planung, Organisation und Hilfswerkzeuge</strong></p>

                    <p>Wir setzen Dienstleistungen, Plattformen und Software anderer Anbieter (nachfolgend bezeichnet als &quot;Drittanbieter&rdquo;) zu Zwecken der Organisation, Verwaltung, Planung sowie Erbringung unserer Leistungen ein. Bei der Auswahl der Drittanbieter und ihrer Leistungen beachten wir die gesetzlichen Vorgaben.</p>

                    <p>In diesem Rahmen k&ouml;nnen personenbezogenen Daten verarbeitet und auf den Servern der Drittanbieter gespeichert werden. Hiervon k&ouml;nnen diverse Daten betroffen sein, die wir entsprechend dieser Datenschutzerkl&auml;rung verarbeiten. Zu diesen Daten k&ouml;nnen insbesondere Stammdaten und Kontaktdaten der Nutzer, Daten zu Vorg&auml;ngen, Vertr&auml;gen, sonstigen Prozessen und deren Inhalte geh&ouml;ren.</p>

                    <p>Sofern Nutzer im Rahmen der Kommunikation, von Gesch&auml;fts- oder anderen Beziehungen mit uns auf die Drittanbieter bzw. deren Software oder Plattformen verwiesen werden, k&ouml;nnen die Drittanbieter Nutzungsdaten und Metadaten zu Sicherheitszwecken, zur Serviceoptimierung oder zu Marketingzwecken verarbeiten. Wir bitten daher darum, die Datenschutzhinweise der jeweiligen Drittanbieter zu beachten.</p>

                    <p><strong>Hinweise zu Rechtsgrundlagen:</strong> Sofern wir die Nutzer um deren Einwilligung in den Einsatz der Drittanbieter bitten, ist die Rechtsgrundlage der Verarbeitung von Daten die Einwilligung. Ferner kann deren Einsatz ein Bestandteil unserer (vor)vertraglichen Leistungen sein, sofern der Einsatz der Drittanbieter in diesem Rahmen vereinbart wurde. Ansonsten werden die Daten der Nutzer auf Grundlage unserer berechtigten Interessen (d.h. Interesse an effizienten, wirtschaftlichen und empf&auml;ngerfreundlichen Leistungen) verarbeitet. In diesem Zusammenhang m&ouml;chten wir Sie auch auf die Informationen zur Verwendung von Cookies in dieser Datenschutzerkl&auml;rung hinweisen.</p>

                    <p><strong>calendly</strong>:</p>

                    <p><strong>Hootsuite</strong>:</p>

                    <ul>
                        <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Ger&auml;te-Informationen, IP-Adressen).</li>
                        <li><strong>Betroffene Personen:</strong> Kommunikationspartner, Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
                        <li><strong>Zwecke der Verarbeitung:</strong> B&uuml;ro- und Organisationsverfahren, Kontaktanfragen und Kommunikation, Reichweitenmessung (z.B. Zugriffsstatistiken, Erkennung wiederkehrender Besucher), Tracking (z.B. interessens-/verhaltensbezogenes Profiling, Nutzung von Cookies), Verwaltung und Beantwortung von Anfragen, Feedback (z.B. Sammeln von Feedback via Online-Formular), Umfragen und Frageb&ouml;gen (z.B. Umfragen mit Eingabem&ouml;glichkeiten, Multiple-Choice-Fragen), Profiling (Erstellen von Nutzerprofilen), Zielgruppenbildung (Bestimmung von f&uuml;r Marketingzwecke relevanten Zielgruppen oder sonstige Ausgabe von Inhalten).</li>
                        <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Vertragserf&uuml;llung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
                    </ul>

                    <p><strong>L&ouml;schung von Daten</strong></p>

                    <p>Die von uns verarbeiteten Daten werden nach Massgabe der gesetzlichen Vorgaben gel&ouml;scht, sobald deren zur Verarbeitung erlaubten Einwilligungen widerrufen werden oder sonstige Erlaubnisse entfallen (z.B., wenn der Zweck der Verarbeitung dieser Daten entfallen ist oder sie f&uuml;r den Zweck nicht erforderlich sind).</p>

                    <p>Sofern die Daten nicht gel&ouml;scht werden, weil sie f&uuml;r andere und gesetzlich zul&auml;ssige Zwecke erforderlich sind, wird deren Verarbeitung auf diese Zwecke beschr&auml;nkt. D.h., die Daten werden gesperrt und nicht f&uuml;r andere Zwecke verarbeitet. Das gilt z.B. f&uuml;r Daten, die aus handels- oder steuerrechtlichen Gr&uuml;nden aufbewahrt werden m&uuml;ssen oder deren Speicherung zur Geltendmachung, Aus&uuml;bung oder Verteidigung von Rechtsanspr&uuml;chen oder zum Schutz der Rechte einer anderen nat&uuml;rlichen oder juristischen Person erforderlich ist.</p>

                    <p>Weitere Hinweise zu der L&ouml;schung von personenbezogenen Daten k&ouml;nnen ferner im Rahmen der einzelnen Datenschutzhinweise dieser Datenschutzerkl&auml;rung erfolgen.</p>

                    <p><strong>&Auml;nderung und Aktualisierung der Datenschutzerkl&auml;rung</strong></p>

                    <p>Wir bitten Sie, sich regelm&auml;ssig &uuml;ber den Inhalt unserer Datenschutzerkl&auml;rung zu informieren. Wir passen die Datenschutzerkl&auml;rung an, sobald die &Auml;nderungen der von uns durchgef&uuml;hrten Datenverarbeitungen dies erforderlich machen. Wir informieren Sie, sobald durch die &Auml;nderungen eine Mitwirkungshandlung Ihrerseits (z.B. Einwilligung) oder eine sonstige individuelle Benachrichtigung erforderlich wird.</p>

                    <p>Sofern wir in dieser Datenschutzerkl&auml;rung Adressen und Kontaktinformationen von Unternehmen und Organisationen angeben, bitten wir zu beachten, dass die Adressen sich &uuml;ber die Zeit &auml;ndern k&ouml;nnen und bitten die Angaben vor Kontaktaufnahme zu pr&uuml;fen.</p>

                    <p><strong>Rechte der betroffenen Personen</strong></p>

                    <p>Ihnen stehen als Betroffene nach der DSGVO verschiedene Rechte zu, die sich insbesondere aus Art. 15 bis 21 DSGVO ergeben:</p>

                    <ul>
                        <li><strong>Widerspruchsrecht: Sie haben das Recht, aus Gr&uuml;nden, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch f&uuml;r ein auf diese Bestimmungen gest&uuml;tztes Profiling. Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch f&uuml;r das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht.</strong></li>
                        <li><strong>Widerrufsrecht bei Einwilligungen:</strong> Sie haben das Recht, erteilte Einwilligungen jederzeit zu widerrufen.</li>
                        <li><strong>Auskunftsrecht:</strong> Sie haben das Recht, eine Best&auml;tigung dar&uuml;ber zu verlangen, ob betreffende Daten verarbeitet werden und auf Auskunft &uuml;ber diese Daten sowie auf weitere Informationen und Kopie der Daten entsprechend den gesetzlichen Vorgaben.</li>
                        <li><strong>Recht auf Berichtigung:</strong> Sie haben entsprechend den gesetzlichen Vorgaben das Recht, die Vervollst&auml;ndigung der Sie betreffenden Daten oder die Berichtigung der Sie betreffenden unrichtigen Daten zu verlangen.</li>
                        <li><strong>Recht auf L&ouml;schung und Einschr&auml;nkung der Verarbeitung:</strong> Sie haben nach Massgabe der gesetzlichen Vorgaben das Recht, zu verlangen, dass Sie betreffende Daten unverz&uuml;glich gel&ouml;scht werden, bzw. alternativ nach Massgabe der gesetzlichen Vorgaben eine Einschr&auml;nkung der Verarbeitung der Daten zu verlangen.</li>
                        <li><strong>Recht auf Daten&uuml;bertragbarkeit:</strong> Sie haben das Recht, Sie betreffende Daten, die Sie uns bereitgestellt haben, nach Massgabe der gesetzlichen Vorgaben in einem strukturierten, g&auml;ngigen und maschinenlesbaren Format zu erhalten oder deren &Uuml;bermittlung an einen anderen Verantwortlichen zu fordern.</li>
                        <li><strong>Beschwerde bei Aufsichtsbeh&ouml;rde:</strong> Sie haben ferner nach Massgabe der gesetzlichen Vorgaben das Recht, bei einer Aufsichtsbeh&ouml;rde, insbesondere in dem Mitgliedstaat Ihres gew&ouml;hnlichen Aufenthaltsorts, Ihres Arbeitsplatzes oder des Orts des mutmasslichen Verstosses Beschwerde einzulegen, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verst&ouml;sst.</li>
                    </ul>

                    <p><strong>Begriffsdefinitionen</strong></p>

                    <p>In diesem Abschnitt erhalten Sie eine &Uuml;bersicht &uuml;ber die in dieser Datenschutzerkl&auml;rung verwendeten Begrifflichkeiten. Viele der Begriffe sind dem Gesetz entnommen und vor allem im Art. 4 DSGVO definiert. Die gesetzlichen Definitionen sind verbindlich. Die nachfolgenden Erl&auml;uterungen sollen dagegen vor allem dem Verst&auml;ndnis dienen. Die Begriffe sind alphabetisch sortiert.</p>

                    <ul>
                        <li><strong>Interessenbasiertes und verhaltensbezogenes Marketing:</strong> Von interessens- und/oder verhaltensbezogenem Marketing spricht man, wenn potentielle Interessen von Nutzern an Anzeigen und sonstigen Inhalten m&ouml;glichst genau vorbestimmt werden. Dies geschieht anhand von Angaben zu deren Vorverhalten (z.B. Aufsuchen von bestimmten Webseiten und Verweilen auf diesen, Kaufverhaltens oder Interaktion mit anderen Nutzern), die in einem sogenannten Profil gespeichert werden. Zu diesen Zwecken werden im Regelfall Cookies eingesetzt.</li>
                        <li><strong>Personenbezogene Daten:</strong> &quot;Personenbezogene Daten&ldquo; sind alle Informationen, die sich auf eine identifizierte oder identifizierbare nat&uuml;rliche Person (im Folgenden &quot;betroffene Person&ldquo;) beziehen; als identifizierbar wird eine nat&uuml;rliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B. Cookie) oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identit&auml;t dieser nat&uuml;rlichen Person sind.</li>
                        <li><strong>Profiling:</strong> Als &quot;Profiling&ldquo; wird jede Art der automatisierten Verarbeitung personenbezogener Daten bezeichnet, die darin besteht, dass diese personenbezogenen Daten verwendet werden, um bestimmte pers&ouml;nliche Aspekte, die sich auf eine nat&uuml;rliche Person beziehen (je nach Art des Profilings geh&ouml;ren dazu Informationen betreffend das Alter, das Geschlecht, Standortdaten und Bewegungsdaten, Interaktion mit Webseiten und deren Inhalten, Einkaufsverhalten, soziale Interaktionen mit anderen Menschen) zu analysieren, zu bewerten oder, um sie vorherzusagen (z.B. die Interessen an bestimmten Inhalten oder Produkten, das Klickverhalten auf einer Webseite oder den Aufenthaltsort). Zu Zwecken des Profilings werden h&auml;ufig Cookies und Web-Beacons eingesetzt.</li>
                        <li><strong>Reichweitenmessung:</strong> Die Reichweitenmessung (auch als Web Analytics bezeichnet) dient der Auswertung der Besucherstr&ouml;me eines Onlineangebotes und kann das Verhalten oder Interessen der Besucher an bestimmten Informationen, wie z.B. Inhalten von Webseiten, umfassen. Mit Hilfe der Reichweitenanalyse k&ouml;nnen Webseiteninhaber z.B. erkennen, zu welcher Zeit Besucher ihre Webseite besuchen und f&uuml;r welche Inhalte sie sich interessieren. Dadurch k&ouml;nnen sie z.B. die Inhalte der Webseite besser an die Bed&uuml;rfnisse ihrer Besucher anpassen. Zu Zwecken der Reichweitenanalyse werden h&auml;ufig pseudonyme Cookies und Web-Beacons eingesetzt, um wiederkehrende Besucher zu erkennen und so genauere Analysen zur Nutzung eines Onlineangebotes zu erhalten.</li>
                        <li><strong>Remarketing:</strong> Vom &quot;Remarketing&ldquo; bzw. &quot;Retargeting&ldquo; spricht man, wenn z.B. zu Werbezwecken vermerkt wird, f&uuml;r welche Produkte sich ein Nutzer auf einer Webseite interessiert hat, um den Nutzer auf anderen Webseiten an diese Produkte, z.B. in Werbeanzeigen, zu erinnern.</li>
                        <li><strong>Standortdaten:</strong> Standortdaten entstehen, wenn sich ein mobiles Ger&auml;t (oder ein anderes Ger&auml;t mit den technischen Voraussetzungen einer Standortbestimmung) mit einer Funkzelle, einem WLAN oder &auml;hnlichen technischen Mittlern und Funktionen der Standortbestimmung, verbindet. Standortdaten dienen der Angabe, an welcher geografisch bestimmbaren Position der Erde sich das jeweilige Ger&auml;t befindet. Standortdaten k&ouml;nnen z. B. eingesetzt werden, um Kartenfunktionen oder andere von einem Ort abh&auml;ngige Informationen darzustellen.</li>
                        <li><strong>Tracking:</strong> Vom &quot;Tracking&ldquo; spricht man, wenn das Verhalten von Nutzern &uuml;ber mehrere Onlineangebote hinweg nachvollzogen werden kann. Im Regelfall werden im Hinblick auf die genutzten Onlineangebote Verhaltens- und Interessensinformationen in Cookies oder auf Servern der Anbieter der Trackingtechnologien gespeichert (sogenanntes Profiling). Diese Informationen k&ouml;nnen anschliessend z.B. eingesetzt werden, um den Nutzern Werbeanzeigen anzuzeigen, die voraussichtlich deren Interessen entsprechen.</li>
                        <li><strong>Verantwortlicher:</strong> Als &quot;Verantwortlicher&ldquo; wird die nat&uuml;rliche oder juristische Person, Beh&ouml;rde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen &uuml;ber die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet, bezeichnet.</li>
                        <li><strong>Verarbeitung:</strong> &quot;Verarbeitung&quot; ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgef&uuml;hrte Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der Begriff reicht weit und umfasst praktisch jeden Umgang mit Daten, sei es das Erheben, das Auswerten, das Speichern, das &Uuml;bermitteln oder das L&ouml;schen.</li>
                        <li><strong>Zielgruppenbildung:</strong> Von Zielgruppenbildung (bzw. &quot;Custom Audiences&ldquo;) spricht man, wenn Zielgruppen f&uuml;r Werbezwecke, z.B. Einblendung von Werbeanzeigen, bestimmt werden. So kann z.B. anhand des Interesses eines Nutzers an bestimmten Produkten oder Themen im Internet geschlussfolgert werden, dass dieser Nutzer sich f&uuml;r Werbeanzeigen f&uuml;r &auml;hnliche Produkte oder den Onlineshop, in dem er die Produkte betrachtet hat, interessiert. Von &quot;Lookalike Audiences&ldquo; (bzw. &auml;hnlichen Zielgruppen) spricht man wiederum, wenn die als geeignet eingesch&auml;tzten Inhalte Nutzern angezeigt werden, deren Profile bzw. Interessen mutmasslich den Nutzern, zu denen die Profile gebildet wurden, entsprechen. Zu Zwecken der Bildung von Custom Audiences und Lookalike Audiences werden im Regelfall Cookies und Web-Beacons eingesetzt.</li>
                    </ul>

                    <p><strong>Kontaktieren Sie Uns</strong></p>

                    <p>Wir stehen Ihnen gerne zur Verf&uuml;gung. Wenn Sie Fragen haben, rufen Sie uns gerne an.</p>

                    <p>Tel. +41 56 410 24 24</p>

                    <p>Wir freuen uns auf Sie!</p>

                    <p><a href="https://datenschutz-generator.de/?l=de" target="_blank">Erstellt mit kostenlosem Datenschutz-Generator.de von Dr. Thomas Schwenke</a></p>

                    <p>&nbsp;</p>



                </div>
            </div>
        </div>
    </div>





@endsection







