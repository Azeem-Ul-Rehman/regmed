<div class="copyright">
    <div class="container">
        <p>RegmedApp © {{ date('Y') }}. All Rights Reserved. Terms of Use and Privacy Policy</p>
    </div>
</div>
