@if(is_null($meta_information))
    <title>RegmedApp | @yield('title')</title>
    <meta name="description"
          content="SmartFoods – Where Quality comes at a Realistic Price">
    <meta name="keywords" content="SmartFoods – Where Quality comes at a Realistic Price">
@else
    <title>RegmedApp | {{$meta_information->title}}</title>
    <meta name="description" content="{{ $meta_information->description }}">
    <meta name="keywords" content="{{ $meta_information->keywords }}">
@endif


