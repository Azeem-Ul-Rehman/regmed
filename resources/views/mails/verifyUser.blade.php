<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css" rel="stylesheet" media="all">


        /* Base ------------------------------ */
        *:not(br):not(tr):not(html) {
            font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        body {
            width: 100% !important;
            height: 100%;
            margin: 0;
            line-height: 1.4;
            background-color: #F5F7F9;
            color: #839197;
            -webkit-text-size-adjust: none;
        }

        a {
            color: #414EF9;
        }

        /* Layout ------------------------------ */
        .email-wrapper {
            width: 100%;
            margin: 0;
            padding: 0;
            background-color: #F5F7F9;
        }

        .email-content {
            width: 100%;
            margin: 0;
            padding: 0;
        }

        /* Masthead ----------------------- */
        .email-masthead {
            padding: 25px 0;
            text-align: center;
        }

        .email-masthead_logo {
            max-width: 400px;
            border: 0;
        }

        .email-masthead_name {
            font-size: 16px;
            font-weight: bold;
            color: #839197;
            text-decoration: none;
            text-shadow: 0 1px 0 white;
        }

        /* Body ------------------------------ */
        .email-body {
            width: 100%;
            margin: 0;
            padding: 0;
            border-top: 1px solid #E7EAEC;
            border-bottom: 1px solid #E7EAEC;
            background-color: #FFFFFF;
        }

        .email-body_inner {
            width: 570px;
            margin: 0 auto;
            padding: 0;
        }

        .email-footer {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            text-align: center;
        }

        .email-footer p {
            color: #839197;
        }

        .body-action {
            width: 100%;
            margin: 30px auto;
            padding: 0;
            text-align: center;
        }

        .body-sub {
            margin-top: 25px;
            padding-top: 25px;
            border-top: 1px solid #E7EAEC;
        }

        .content-cell {
            padding: 35px;
        }

        .align-right {
            text-align: right;
        }

        /* Type ------------------------------ */
        h1 {
            margin-top: 0;
            color: #292E31;
            font-size: 19px;
            font-weight: bold;
            text-align: left;
        }

        h2 {
            margin-top: 0;
            color: #292E31;
            font-size: 16px;
            font-weight: bold;
            text-align: left;
        }

        h3 {
            margin-top: 0;
            color: #292E31;
            font-size: 14px;
            font-weight: bold;
            text-align: left;
        }

        p {
            margin-top: 0;
            color: #839197;
            font-size: 16px;
            line-height: 1.5em;
            text-align: left;
        }

        p.sub {
            font-size: 12px;
        }

        p.center {
            text-align: center;
        }

        /* Buttons ------------------------------ */
        .button {
            display: inline-block;
            width: 200px;
            background-color: #414EF9;
            border-radius: 3px;
            color: #ffffff;
            font-size: 15px;
            line-height: 45px;
            text-align: center;
            text-decoration: none;
            -webkit-text-size-adjust: none;
            mso-hide: all;
        }

        .button--green {
            background-color: #28DB67;
        }

        .button--red {
            background-color: #FF3665;
        }

        .button--blue {
            background-color: #414EF9;
        }

        /*Media Queries ------------------------------ */
        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }

        h2 {
            margin-top: 12px;
        }

        a.button {
            cursor: pointer;
            cursor: hand;
        }

        .verify-btn {
            font-family: Avenir, Helvetica, sans-serif;
            box-sizing: border-box;
            border-radius: 3px;
            color: #fff !important;
            display: inline-block;
            text-decoration: none;
            background-color: #121457;
            border-top: 10px solid #121457;
            border-right: 18px solid #121457;
            border-bottom: 10px solid #121457;
            border-left: 18px solid #121457;
        }
        .img-class {
            width: 16% !important;
            height: auto !important;
        }
    </style>
</head>
<body>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                    <td class="email-masthead content-cell" style="background: #121457;color:white;">
                        <img src="{{ asset('frontend/images/admin-logo.png') }}" class="img-class"
                             style="width: 16% !important; height: auto; !important;" alt="logo">
                        <span class="logodesc">
                  <span class="logo-title"
                        style="
                        color:#fff;
                        font-size: 26px;
                        display: block;
                        font-weight: bold;
                        margin-top: 5px;
                    ">RegMed App</span>
              </span>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell">
                                    <!-- Action -->
                                    <table class="body-action" align="center" width="100%" cellpadding="0"
                                           cellspacing="0">
                                        <tr>
                                            <td valign="top" align="left">
                                                <table class="p100"
                                                       style="margin: 0; mso-table-lspace: 0; mso-table-rspace: 0; padding: 0;"
                                                       cellspacing="0" cellpadding="0" border="0" align="left">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <table class="p100"
                                                                   style="margin: 0; mso-table-lspace: 0; mso-table-rspace: 0; padding: 0; width: 600px;"
                                                                   width="600" cellspacing="0" cellpadding="0"
                                                                   border="0" align="left">
                                                                <tbody>


                                                                <tr>
                                                                    <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left"
                                                                        class="editable_text text">
                                                                        <font face="'Open Sans', sans-serif">

                                                                            <h1>
                                                                                Hello {{$user['first_name']}}  {{$user['last_name']}}
                                                                                ,
                                                                            </h1>

                                                                        </font>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left"
                                                                        class="editable_text text">
                                                                        <font face="'Open Sans', sans-serif">

                                                                            <p> You have received this email because you
                                                                                have registered with RegmedApp and your
                                                                                activation link follows.
                                                                            </p>

                                                                        </font>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left"
                                                                        class="editable_text text">
                                                                        <font face="'Open Sans', sans-serif">

                                                                            <p>
                                                                                Please confirm your email address here
                                                                            </p>

                                                                        </font>
                                                                    </td>
                                                                </tr>
                                                                @if($user['user_type'] == 'prospect')
                                                                    <br>
                                                                    <br>
                                                                    <tr>
                                                                        <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                            valign="top" align="left"
                                                                            class="editable_text text">
                                                                            <font face="'Open Sans', sans-serif">

                                                                                <p>
                                                                                    Thank you for
                                                                                    subscribing to
                                                                                    the information system for services
                                                                                    of
                                                                                    the Swiss RegMed Society AG. In
                                                                                    order to
                                                                                    process the subject of your
                                                                                    interest,
                                                                                    please verify your permission to
                                                                                    send
                                                                                    this information to the counselors.
                                                                                    We
                                                                                    will never share this information
                                                                                    outside of this service.
                                                                                </p>

                                                                            </font>
                                                                        </td>
                                                                    </tr>
                                                                    <br>
                                                                    <br>
                                                                    <tr>
                                                                        <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                            valign="top" align="left"
                                                                            class="editable_text text">
                                                                            <font face="'Open Sans', sans-serif">

                                                                                <p>
                                                                                    Vielen Dank, dass
                                                                                    Sie
                                                                                    das
                                                                                    Informationssystem für
                                                                                    Dienstleistungen
                                                                                    der
                                                                                    Swiss RegMed Society AG abonniert
                                                                                    haben.
                                                                                    Um
                                                                                    das Thema Ihres Interesses zu
                                                                                    bearbeiten,
                                                                                    überprüfen Sie bitte Ihre Erlaubnis,
                                                                                    diese
                                                                                    Informationen an die Berater zu
                                                                                    senden.
                                                                                    Wir
                                                                                    werden diese Informationen niemals
                                                                                    außerhalb
                                                                                    dieses Dienstes weitergeben.
                                                                                </p>

                                                                            </font>
                                                                        </td>
                                                                    </tr>
                                                                    <br>
                                                                    <br>
                                                                @endif
                                                                <tr>
                                                                    <td style=" font-size: 21px; font-weight: 500;height: 20px; text-align:center;mso-line-height-rule: exactly;"
                                                                        valign="top" align="left">

                                                                        <a class="verify-btn"
                                                                           href="{{url('user/verify',$user->activation->token)}}">Verify
                                                                            Email</a>
                                                                        {{--                                                                        FTA: {{$user->activation->token}}--}}
                                                                    </td>
                                                                </tr>

                                                                @if($user['user_type'] == 'prospect')
                                                                    <br>
                                                                    <br>
                                                                    <tr>
                                                                        <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                            valign="top" align="left"
                                                                            class="editable_text text">
                                                                            <font face="'Open Sans', sans-serif">

                                                                                <p>
                                                                                    If you send us this email
                                                                                    confirmation of your data, your
                                                                                    details from the request form,
                                                                                    including the contact details you
                                                                                    provided there, will be stored by us
                                                                                    for the purpose of processing the
                                                                                    request and in case of follow-up
                                                                                    questions. We do not pass on this
                                                                                    data without your consent. You can
                                                                                    revoke your consent to the storage
                                                                                    of the data, the e-mail address and
                                                                                    their use for sending the newsletter
                                                                                    at any time. The data required to
                                                                                    create a service offer are passed on
                                                                                    to a cooperation partner (service
                                                                                    provider). By confirming your
                                                                                    registration, you consent to the use
                                                                                    of your personal data for the
                                                                                    purposes agreed here. You hereby
                                                                                    confirm that you agree to these
                                                                                    purposes, terms and conditions and
                                                                                    data protection provisions.
                                                                                </p>

                                                                            </font>
                                                                        </td>
                                                                    </tr>
                                                                    <br>
                                                                    <br>
                                                                    <tr>
                                                                        <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                            valign="top" align="left"
                                                                            class="editable_text text">
                                                                            <font face="'Open Sans', sans-serif">

                                                                                <p>
                                                                                    Wenn Sie uns diese
                                                                                    E-Mail-Bestätigung Ihrer Daten
                                                                                    senden, werden Ihre Daten aus dem
                                                                                    Anfrageformular, einschließlich der
                                                                                    dort angegebenen Kontaktdaten, von
                                                                                    uns zum Zwecke der Bearbeitung der
                                                                                    Anfrage und bei weiteren Fragen
                                                                                    gespeichert. Wir geben diese Daten
                                                                                    nicht ohne Ihre Zustimmung weiter.
                                                                                    Sie können Ihre Einwilligung zur
                                                                                    Speicherung der Daten, der
                                                                                    E-Mail-Adresse und deren Verwendung
                                                                                    zum Versenden des Newsletters
                                                                                    jederzeit widerrufen.Die zur
                                                                                    Erstellung eines Serviceangebots
                                                                                    erforderlichen Daten werden an einen
                                                                                    Kooperationspartner (Dienstleister)
                                                                                    weitergegeben. Mit der Bestätigung
                                                                                    Ihrer Registrierung stimmen Sie der
                                                                                    Verwendung Ihrer persönlichen Daten
                                                                                    für die hier vereinbarten Zwecke zu.
                                                                                    Sie bestätigen hiermit, dass Sie
                                                                                    diesen Zwecken, Geschäftsbedingungen
                                                                                    und Datenschutzbestimmungen
                                                                                    zustimmen. Allgemeines: Wir
                                                                                    bearbeiten Personendaten
                                                                                    (Personendaten sind alle Angaben,
                                                                                    die sich auf eine bestimmte oder
                                                                                    bestimmbare Person beziehen) für
                                                                                    jene Dauer, die für den jeweiligen
                                                                                    Zweck oder die jeweiligen Zwecke
                                                                                    erforderlich ist, und im Einklang
                                                                                    mit dem schweizerischen
                                                                                    Datenschutzrecht. Im Übrigen
                                                                                    bearbeiten wir – soweit und sofern
                                                                                    die EU-DSGVO anwendbar ist –
                                                                                    Personendaten gemäss
                                                                                    Rechtsgrundlagen im Zusammenhang mit
                                                                                    Art. 6 Abs. 1 DSGVO.
                                                                                </p>

                                                                            </font>
                                                                        </td>
                                                                    </tr>
                                                                    <br>
                                                                    <br>
                                                                @endif
                                                                <tr>
                                                                    <td style="color: rgb(141, 141, 141); font-size: 14px; font-weight: 400; letter-spacing: 0.02em; line-height: 23px; text-align: center; cursor: pointer; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left"
                                                                        class="editable_text text">
                                                                        <font face="'Open Sans', sans-serif">

                                                                            <p> If you received this message by error,
                                                                                please notify the sender and delete /
                                                                                ignore the message.
                                                                            </p>

                                                                        </font>
                                                                    </td>
                                                                </tr>
                                                                <br>
                                                                <br>
                                                                <tr>
                                                                    <td style="color: #000; font-size: 16px; font-weight: 600; letter-spacing: 0.02em; line-height: 23px; text-align: left; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left">
                                                                        <font face="'Open Sans', sans-serif">
                                                                            <a href="https://regmedapp.ch/terms-and-conditions">Terms
                                                                                & Conditions</a> <span> and </span>
                                                                            <a href="https://regmedapp.ch/privacy-policy">Privacy
                                                                                Policy</a>
                                                                        </font>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color: #000; font-size: 16px; font-weight: 600; letter-spacing: 0.02em; line-height: 23px; text-align: left; box-sizing: content-box; outline: none 0;"
                                                                        valign="top" align="left">
                                                                        <font face="'Open Sans', sans-serif">
                                                                            Many Thanks.
                                                                        </font>
                                                                    </td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background: #121457;color:white;">
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-cell">
                                    <p class="sub center" style="color:#fff">
                                        Support Team
                                        <br>
                                        <b>RegmedApp</b>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
