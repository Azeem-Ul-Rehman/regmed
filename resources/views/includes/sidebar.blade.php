<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
        class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">


            <li class="m-menu__item {{ (request()->is('admin/dashboard')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.dashboard.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-dashboard"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
											</span></span></a></li>

            <li class="m-menu__item {{ (request()->is('admin/users') || request()->is('admin/users/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.users.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-user-add"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Users</span>
											</span></span></a></li>


            <li class="m-menu__item  {{ (request()->is('admin/categories') || request()->is('admin/categories/*'))? 'activeMenu ' : ''}}"
                aria-haspopup="true" data-redirect="true"><a href="{{ route('admin.categories.index') }}"
                                                             class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-book"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Categories</span>
											</span></span></a></li>


            <li class="m-menu__item  {{ (request()->is('admin/prospects') || request()->is('admin/prospects/*'))? 'activeMenu ' : ''}}"
                aria-haspopup="true" data-redirect="true"><a href="{{ route('admin.prospects.index') }}"
                                                             class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-users-1"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Prospects</span>
											</span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('admin/notifications') || request()->is('admin/notifications/*'))? 'activeMenu ' : ''}}"
                aria-haspopup="true" data-redirect="true"><a href="{{ route('admin.notifications.index') }}"
                                                             class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-alarm-1"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Notifications</span>
											</span></span></a></li>

{{--            <li class="m-menu__item {{ (request()->is('admin/contacts') || request()->is('admin/contacts/*')) ? 'activeMenu' : '' }} "--}}
{{--                aria-haspopup="true"><a href="{{ route('admin.contacts.index') }}" class="m-menu__link "><i--}}
{{--                        class="m-menu__link-icon flaticon-bell"></i><span--}}
{{--                        class="m-menu__link-title"> <span--}}
{{--                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Contact Us</span>--}}
{{--											</span></span></a></li>--}}


        </ul>

    </div>

    <!-- END: Aside Menu -->
</div>
