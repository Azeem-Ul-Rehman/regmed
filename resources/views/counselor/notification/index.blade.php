@extends('counselor.layouts.master')
@section('title','Notification')
@push('css')
    <style>

        @media screen and (max-width: 650px) {
            .responsiveTable {
                overflow-x: scroll;
            }

            .responsiveTable a {
                width: 100%;
                margin: 5px 0;
            }

        }

    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Notification
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('counselor.notifications.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Send Notification</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <form action="{{ route('counselor.notifications.index') }}" method="GET" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Filter</h4>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xl-6">

                            <label for="sent_to"
                                   class="col-md-4 col-form-label text-md-left"><strong>Users:</strong></label>

                            <select id="sent_to"
                                    class="form-control states @error('sent_to') is-invalid @enderror"
                                    name="sent_to" autocomplete="sent_to">

                                <option value="">Select User</option>
                                @if(!empty($users) && count($users) >0)
                                    @foreach($users as $user)
                                        <option
                                            value="{{$user->id}}" {{ (old('sent_to') == $user->id) ? 'selected' : ''  }}> {{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}</option>
                                    @endforeach
                                @endif
                            </select>

                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="m-form__actions m-form__actions">
                                <label style="display: block"
                                       class="col-md-4 col-form-label text-md-left">&nbsp;</label>
                                <a href="{{ route('counselor.notifications.index') }}"
                                   class="btn btn-accent m-btn m-btn--icon m-btn--air refreshBtn">
                                <span>
                                    <i class="la la-refresh"></i>

                                </span>
                                </a>
                                <button class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    Submit
                                </button>
                            </div>

                        </div>
                    </div>


                </form>


                <hr>
                <div class="responsiveTable">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                        <tr>

                            <th> Sent Date</th>
                            <th> Sent By</th>
                            <th> Sent To</th>
                            <th style="width: 50%;"> Message</th>
                            <th> Is Read ?</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($notifications))
                            @foreach($notifications as $notification)
                                <tr>
                                    <td style="width: 9%;">{{$notification->created_at ? date('d-m-Y',strtotime($notification->created_at)) : '--'}}</td>
                                    <td>{{$notification->sentByUser ? $notification->sentByUser->first_name : 'Not Found'}} {{$notification->sentByUser ? $notification->sentByUser->last_name : ''}}</td>
                                    <td>{{$notification->sentToUser ? $notification->sentToUser->first_name : 'Not Found'}} {{$notification->sentToUser ? $notification->sentToUser->last_name : ''}}</td>
                                    <td style="word-break: break-word;">{{$notification->message}}</td>
                                    <td id="isReadButton{{$notification->id}}">
                                        @if($notification->is_read == 0)
                                            <button class="btn btn-sm btn-warning"
                                                    style="border-radius: 25% !important;color: #fff">No
                                            </button>
                                        @else
                                            <button class="btn btn-sm btn-success"
                                                    style="border-radius: 25% !important;color: #fff">Yes
                                            </button>
                                        @endif
                                    </td>
                                    <td style="width: 20%;">
                                        <a href="javascript:void(0)" id="clickToRead"
                                           data-notification-id="{{$notification->id}}"
                                           data-notification-read="{{$notification->is_read}}"
                                           class="btn btn-sm btn-info pull-left notificationText{{$notification->id}} clickToRead">
                                            @if($notification->is_read == 0)
                                                Click To Read
                                            @else
                                                Click To UnRead
                                            @endif
                                        </a>
                                        <form method="post"
                                              action="{{ route('counselor.notifications.destroy', $notification->id) }}"
                                              id="delete_{{ $notification->id }}">
                                            @csrf
                                            @method('DELETE')
                                            <a class="btn btn-sm btn-danger m-left"
                                               href="javascript:void(0)"
                                               onclick="if(confirmDelete()){
                                                   document.getElementById('delete_<?=$notification->id?>').submit(); }">
                                                Delete
                                            </a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $("select").select2();
        $("#m_table_1").dataTable({});

        $(document).on('click', '.clickToRead', function () {
            var notificationId = $(this).data('notification-id');
            var notificationReadValue = $(this).data('notification-read');
            var request = "id=" + notificationId + "&is_read=" + notificationReadValue;
            $.ajaxSetup({
                headers: {
                    'csrftoken': '<?php echo e(csrf_token()); ?>',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.click.to.read.notification') }}",
                data: request,
                dataType: "json",
                cache: true,
                global: false,
                success: function (response) {
                    if (response.status == "success") {
                        toastr['success']("Notification Updated Successfully.");
                        var html = '';
                        if (notificationReadValue == 0) {
                            html += '<button class="btn btn-sm btn-success"  style="border-radius: 25% !important;color: #fff">Yes </button>';
                            document.getElementsByClassName(".notificationText" + notificationId + "").text = "Click To Read";
                        } else {
                            html += '<button class="btn btn-sm btn-warning"  style="border-radius: 25% !important;color: #fff">No </button>';
                            document.getElementsByClassName(".notificationText" + notificationId + "").text = "Click To UnRead";
                        }
                        $('#isReadButton' + notificationId).html(html);


                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);


                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        });
    </script>
@endpush
