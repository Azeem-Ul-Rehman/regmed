@extends('counselor.layouts.master')
@section('title','Users')
@push('css')
    <style>
        .modal .modal-content .modal-header {

            background: #f1743b !important;
        }

        .modal .modal-content .modal-header .modal-title {
            color: #fff !important;
        }


        .button-right {
            float: right;
            margin-top: 12px;
        }
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('User') }}
                        </h3>
                    </div>
                </div>
            </div>


            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('counselor.users.update',$user->id) }}"
                              id="create"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="first_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('First Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="first_name" type="text"
                                                   class="form-control @error('first_name') is-invalid @enderror"
                                                   name="first_name" value="{{ old('first_name',$user->first_name) }}"
                                                   autocomplete="first_name" autofocus>

                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="last_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Last Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="last_name" type="text"
                                                   class="form-control @error('last_name') is-invalid @enderror"
                                                   name="last_name" value="{{ old('last_name',$user->last_name) }}"
                                                   autocomplete="last_name" autofocus>

                                            @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="email"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email"
                                                   value="{{ old('email',$user->email) }}" autocomplete="email"
                                                   readonly>

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="phone_number"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Mobile Number') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="phone_number" type="text"
                                                   class="form-control @error('phone_number') is-invalid @enderror"
                                                   name="phone_number"
                                                   value="{{ old('phone_number',$user->phone_number) }}"
                                                   autocomplete="phone_number" autofocus placeholder="03001234567">

                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="address"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   value="{{ old('address',$user->address) }}" autocomplete="address">

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
{{--                                    <div class="form-group row">--}}
{{--                                        <div class="col-md-6">--}}
{{--                                            <label for="role_id"--}}
{{--                                                   class="col-md-4 col-form-label text-md-left">{{ __('Role') }} <span--}}
{{--                                                    class="mandatorySign">*</span></label>--}}

{{--                                            <select id="role_id"--}}
{{--                                                    class="form-control roles @error('role_id') is-invalid @enderror"--}}
{{--                                                    name="role_id" autocomplete="role_id">--}}
{{--                                                <option value="">Select an role</option>--}}
{{--                                                @if(!empty($roles))--}}
{{--                                                    @foreach($roles as $role)--}}
{{--                                                        @if($role->id != '1')--}}
{{--                                                            <option--}}
{{--                                                                value="{{$role->id}}" {{ (old('role_id',$user->role_id) == $role->id) ? 'selected' : '' }}>{{ucfirst($role->name)}}</option>--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            </select>--}}

{{--                                            @error('role_id')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                              <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-6">--}}
{{--                                            <label for="category_id"--}}
{{--                                                   class="col-md-4 col-form-label text-md-left">{{ __('Categories') }}--}}
{{--                                                <span--}}
{{--                                                    class="mandatorySign">*</span></label>--}}

{{--                                            <select id="role_id"--}}
{{--                                                    class="form-control categories @error('category_id') is-invalid @enderror"--}}
{{--                                                    name="category_id" autocomplete="category_id">--}}
{{--                                                <option value="">Select an Category</option>--}}
{{--                                                @if(!empty($categories))--}}
{{--                                                    @foreach($categories as $category)--}}
{{--                                                        <option--}}
{{--                                                            value="{{$category->id}}" {{ (old('category_id',$user->category_id) == $category->id) ? 'selected' : '' }}>{{ucfirst($category->name)}}</option>--}}

{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            </select>--}}

{{--                                            @error('category_id')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                              <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label for="password"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Password') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="password" type="password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password"
                                                   autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">

                                            <label for="password-confirm"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Confirm Password') }}</label>
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" autocomplete="new-password">

                                        </div>
                                        <div class="col-md-4">
                                            <label for="status"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="status"
                                                    class="form-control cities @error('status') is-invalid @enderror"
                                                    name="status" autocomplete="status">
                                                <option value="" {{ (old('status') == '') ? 'selected' : '' }}>Select an
                                                    option
                                                </option>
                                                <option
                                                    value="pending" {{ (old('status',$user->status) == 'pending') ? 'selected' : '' }}>
                                                    Pending
                                                </option>
                                                <option
                                                    value="verified" {{ (old('status',$user->status) == 'verified') ? 'selected' : '' }}>
                                                    Verified
                                                </option>
                                                <option
                                                    value="suspended" {{ (old('status',$user->status) == 'suspended') ? 'selected' : '' }}>
                                                    Suspended
                                                </option>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('counselor.users.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection



@push('js')
    <script>

        $('.roles').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.categories';
            var role_id = $(this).val();
            var request = "role_id=" + role_id;

            if (role_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.role.categories') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.categories, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Category</option>");


                            $('.categories').find('option[value="{{ old('categroy_id') }}"]').attr('selected', 'selected');
                            $('.categories').trigger('change');
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select Area</option>");
            }
        });
    </script>
@endpush
