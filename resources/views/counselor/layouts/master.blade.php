<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>RegmedApp | @yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!--end::Web font -->

    <!--begin::Global Theme Styles -->
    <link href="{{asset('vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css"/>

    <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{asset('demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css"/>

    {{--   Custom CSS --}}
    <link href="{{asset('demo/default/base/custom.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('demo/default/base/components.min.css')}}" rel="stylesheet" type="text/css"/>


    <!--RTL version:<link href="assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{asset('vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <link href="{{asset('redactor/redactor.min.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <!--begin::Page Vendors Styles -->
    <link href="{{asset('vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet"
          type="text/css"/>

    <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--end::Page Vendors Styles -->
    {{--    <link rel="shortcut icon" href="{{asset('demo/default/media/img/logo/favicon.ico')}}"/>--}}

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}"/>


    <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->


    @stack('css')

    <style>
        /* ----------- iPhone 4 and 4S ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* Landscape */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* ----------- iPhone 5, 5S, 5C and 5SE ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 568px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* Portrait */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 568px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 568px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* ----------- iPhone 6, 6S, 7 and 8 ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 375px)
        and (max-device-width: 667px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* Portrait */
        @media only screen
        and (min-device-width: 375px)
        and (max-device-width: 667px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* Landscape */
        @media only screen
        and (min-device-width: 375px)
        and (max-device-width: 667px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* ----------- iPhone 6+, 7+ and 8+ ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 414px)
        and (max-device-width: 736px)
        and (-webkit-min-device-pixel-ratio: 3) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* Portrait */
        @media only screen
        and (min-device-width: 414px)
        and (max-device-width: 736px)
        and (-webkit-min-device-pixel-ratio: 3)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* Landscape */
        @media only screen
        and (min-device-width: 414px)
        and (max-device-width: 736px)
        and (-webkit-min-device-pixel-ratio: 3)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* ----------- iPhone X ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 375px)
        and (max-device-width: 812px)
        and (-webkit-min-device-pixel-ratio: 3) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* Portrait */
        @media only screen
        and (min-device-width: 375px)
        and (max-device-width: 812px)
        and (-webkit-min-device-pixel-ratio: 3)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media only screen
        and (min-device-width: 375px)
        and (max-device-width: 812px)
        and (-webkit-min-device-pixel-ratio: 3)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }

        }

        /* ----------- Galaxy S3 ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 320px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 2)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 320px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 2)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Galaxy S4, S5 and Note 3 ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 320px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 320px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 320px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Galaxy S6 ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 4) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 4)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 4)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Google Pixel ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Google Pixel XL ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 4) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 4)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 4)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- HTC One ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 360px)
        and (device-height: 640px)
        and (-webkit-device-pixel-ratio: 3)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Windows Phone ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 480px)
        and (device-height: 800px) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 480px)
        and (device-height: 800px)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 480px)
        and (device-height: 800px)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- iPad 1, 2, Mini and Air ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 768px)
        and (max-device-width: 1024px)
        and (-webkit-min-device-pixel-ratio: 1) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media only screen
        and (min-device-width: 768px)
        and (max-device-width: 1024px)
        and (orientation: portrait)
        and (-webkit-min-device-pixel-ratio: 1) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media only screen
        and (min-device-width: 768px)
        and (max-device-width: 1024px)
        and (orientation: landscape)
        and (-webkit-min-device-pixel-ratio: 1) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- iPad 3, 4 and Pro 9.7" ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 768px)
        and (max-device-width: 1024px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media only screen
        and (min-device-width: 768px)
        and (max-device-width: 1024px)
        and (orientation: portrait)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media only screen
        and (min-device-width: 768px)
        and (max-device-width: 1024px)
        and (orientation: landscape)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- iPad Pro 10.5" ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 834px)
        and (max-device-width: 1112px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        /* Declare the same value for min- and max-width to avoid colliding with desktops */
        /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
        @media only screen
        and (min-device-width: 834px)
        and (max-device-width: 834px)
        and (orientation: portrait)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        /* Declare the same value for min- and max-width to avoid colliding with desktops */
        /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
        @media only screen
        and (min-device-width: 1112px)
        and (max-device-width: 1112px)
        and (orientation: landscape)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- iPad Pro 12.9" ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 1024px)
        and (max-device-width: 1366px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        /* Declare the same value for min- and max-width to avoid colliding with desktops */
        /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
        @media only screen
        and (min-device-width: 1024px)
        and (max-device-width: 1024px)
        and (orientation: portrait)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        /* Declare the same value for min- and max-width to avoid colliding with desktops */
        /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
        @media only screen
        and (min-device-width: 1366px)
        and (max-device-width: 1366px)
        and (orientation: landscape)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Galaxy Tab 2 ----------- */

        /* Portrait and Landscape */
        @media (min-device-width: 800px)
        and (max-device-width: 1280px) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media (max-device-width: 800px)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media (max-device-width: 1280px)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Galaxy Tab S ----------- */

        /* Portrait and Landscape */
        @media (min-device-width: 800px)
        and (max-device-width: 1280px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media (max-device-width: 800px)
        and (orientation: portrait)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media (max-device-width: 1280px)
        and (orientation: landscape)
        and (-webkit-min-device-pixel-ratio: 2) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Nexus 7 ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 601px)
        and (device-height: 906px)
        and (-webkit-min-device-pixel-ratio: 1.331)
        and (-webkit-max-device-pixel-ratio: 1.332) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 601px)
        and (device-height: 906px)
        and (-webkit-min-device-pixel-ratio: 1.331)
        and (-webkit-max-device-pixel-ratio: 1.332)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 601px)
        and (device-height: 906px)
        and (-webkit-min-device-pixel-ratio: 1.331)
        and (-webkit-max-device-pixel-ratio: 1.332)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Nexus 9 ----------- */

        /* Portrait and Landscape */
        @media screen
        and (device-width: 1536px)
        and (device-height: 2048px)
        and (-webkit-min-device-pixel-ratio: 1.331)
        and (-webkit-max-device-pixel-ratio: 1.332) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media screen
        and (device-width: 1536px)
        and (device-height: 2048px)
        and (-webkit-min-device-pixel-ratio: 1.331)
        and (-webkit-max-device-pixel-ratio: 1.332)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media screen
        and (device-width: 1536px)
        and (device-height: 2048px)
        and (-webkit-min-device-pixel-ratio: 1.331)
        and (-webkit-max-device-pixel-ratio: 1.332)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Kindle Fire HD 7" ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 800px)
        and (max-device-width: 1280px)
        and (-webkit-min-device-pixel-ratio: 1.5) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media only screen
        and (min-device-width: 800px)
        and (max-device-width: 1280px)
        and (-webkit-min-device-pixel-ratio: 1.5)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media only screen
        and (min-device-width: 800px)
        and (max-device-width: 1280px)
        and (-webkit-min-device-pixel-ratio: 1.5)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* ----------- Kindle Fire HD 8.9" ----------- */

        /* Portrait and Landscape */
        @media only screen
        and (min-device-width: 1200px)
        and (max-device-width: 1600px)
        and (-webkit-min-device-pixel-ratio: 1.5) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Portrait */
        @media only screen
        and (min-device-width: 1200px)
        and (max-device-width: 1600px)
        and (-webkit-min-device-pixel-ratio: 1.5)
        and (orientation: portrait) {
            .ipadLogo {
                display: none !important;
            }
        }

        /* Landscape */
        @media only screen
        and (min-device-width: 1200px)
        and (max-device-width: 1600px)
        and (-webkit-min-device-pixel-ratio: 1.5)
        and (orientation: landscape) {
            .ipadLogo {
                display: none !important;
            }
        }

        .m-topbar .m-topbar__nav.m-nav > .m-nav__item > .m-nav__link .m-topbar__userpic img {
             height: 45px !important;
            border-radius: 50% !important;
        }
    </style>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body
    class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">


    <!-- BEGIN: Header -->
@include('counselor.includes.header')
<!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
    @include('counselor.includes.sidebar')
    <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            @yield('content')
        </div>
    </div>
    <!-- begin::Footer -->
@include('counselor.includes.footer')
<!-- end::Footer -->
</div>

<script src="{{asset('vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<script src="{{asset('vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<!--begin::Page Vendors -->
<script src="{{asset('vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<script src="{{asset('redactor/redactor.min.js')}}"></script>
<script src="{{asset('js/select2.js')}}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>

<script type="text/javascript" src="{{ asset('assets/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>


<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<!--end::Page Scripts -->
<script type="text/javascript">

    $(document).ready(function () {
        if ($('.cnic-mask').length) {
            $('.cnic-mask').mask('00000-0000000-0');
        }
        if ($('.phone_number').length) {
            $('.phone_number').mask('00000000000');
        }
    });

    function readURL(input) {
        console.log(input);
        if (input.files && input.files[0]) {

            let size = input.files[0].size;
            console.log(`Image Size: ${size}`);

            var reader = new FileReader();
            reader.onload = function (e) {


                if (size > 2000000) {
                    $('#img').attr('src', '');
                    $('#image').val('');
                    toastr.warning(" Image Size is exceeding 2 Mb");


                } else {
                    $('#img').attr('src', e.target.result);
                    $('#img').css("display", "block");
                    $('#hidden-field').val('');
                }


            };
            reader.readAsDataURL(input.files[0]);
        }
    }


    function readURLThumbnail(input) {
        if (input.files && input.files[0]) {

            let size = input.files[0].size;
            console.log(`Image Size: ${size}`);


            var reader = new FileReader();
            reader.onload = function (e) {

                if (size > 2000000) {
                    $('#img_thumbnail').attr('src', '');
                    $('#thumbnail_image').val('');
                    toastr.warning("Thumbnail Image Size is exceeding 2 Mb");
                } else {
                    $('#img_thumbnail').attr('src', e.target.result);
                    $('#img_thumbnail').css("display", "block");
                    $('#hidden-field').val('');
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function confirmDelete() {
        var r = confirm("Are you sure you want to perform this action");
        if (r === true) {
            return true;
        } else {
            return false;
        }
    }
</script>
<script>
    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif
</script>
@stack('models')
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js"></script>
<link rel="manifest" href="{{ asset('manifest.json') }}">
<script src="{{ asset('js/firebase.js') }}"></script>
@stack('js')
</body>
</html>
