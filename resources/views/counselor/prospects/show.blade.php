@extends('counselor.layouts.master')
@section('title','Prospect')
@push('css')
    <style>
        .show-shifts, .show-services {
            display: none;
        }

        .select2-container {
            width: 100% !important;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            View {{ __('Prospect') }} Detail
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                <div class="m-form__actions m-form__actions">
                    <a href="{{ route('counselor.prospects.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">

                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{ $prospect->first_name }} {{ $prospect->last_name }}</td>
                            </tr>
                            <tr>
                                <th>Role</th>
                                <td>{{ucwords(str_replace('-',' ',$prospect->user_type))}}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>{{$prospect->category ?  $prospect->category->name : 'Not Found'}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $prospect->email }}</td>
                            </tr>
                            <tr>
                                <th>Phone Number</th>
                                <td>{{ $prospect->phone_number ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{ $prospect->address ?? '-' }}</td>
                            </tr>

                            <tr>
                                <th>City</th>
                                <td>{{ $prospect->city ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>{{ $prospect->country ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Postal Code</th>
                                <td>{{ $prospect->postal_code ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ $prospect->status ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Interested In</th>
                                <td>{{ str_replace('_',' ',strtoupper($prospect->interested_in)) ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Date of Birth</th>
                                <td>{{ $prospect->date_of_birth ? date('Y-m-d', strtotime($prospect->date_of_birth)) : '-' }}</td>
                            </tr>
                            <tr>
                                <th>How Did If Find Us</th>
                                <td>{{ $prospect->how_did_it_find_us ?? '-' }}</td>
                            </tr>
                            @if($prospect->category_id == 3)
                                <tr>
                                    <th>Father Name</th>
                                    <td>{{ $prospect->father_name ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Expected Baby Date of Birth</th>
                                    <td>{{ $prospect->expected_date_of_birth_of_baby ? date('Y-m-d', strtotime($prospect->expected_date_of_birth_of_baby)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Expected Clinic</th>
                                    <td>{{ $prospect->expected_date_clinic ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Twin pregancy</th>
                                    <td>{{ strtoupper($prospect->twin_pregnancy) ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Contract Signed</th>
                                    <td>{{ $prospect->contract_signed ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Contract Signed Date</th>
                                    <td>{{ $prospect->contract_signed_date ? date('Y-m-d', strtotime($prospect->contract_signed_date)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Kit Sent</th>
                                    <td>{{ $prospect->kit_sent ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Kit Sent Date</th>
                                    <td>{{ $prospect->kit_sent_date ? date('Y-m-d', strtotime($prospect->kit_sent_date)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Process Completed</th>
                                    <td>{{ $prospect->process_completed ??  '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Dr./Midwife</th>
                                    <td>{{ $prospect->mild_wife ??  '-' }}</td>
                                </tr>

                            @endif
                            @if($prospect->category_id == 4)
                                <tr>
                                    <th>Liposuction Date</th>
                                    <td>{{ $prospect->date_of_liposuction ? date('Y-m-d', strtotime($prospect->date_of_liposuction)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Doctor</th>
                                    <td>{{ $prospect->doctor ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Clinic</th>
                                    <td>{{ $prospect->clinic ?? '-' }}</td>
                                </tr>
                            @endif

                            @if($prospect->category_id == 5)
                                <tr>
                                    <th>Area of interest</th>
                                    <td>{{ $prospect->area_of_interest ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Willing to Travel Inside EU</th>
                                    <td>{{ $prospect->willing_to_travel_inside_eu ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Willing to Travel Outside EU</th>
                                    <td>{{ $prospect->willing_to_travel_outside_eu ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Vacations Included</th>
                                    <td>{{ $prospect->vacations_included ?? '-' }}</td>
                                </tr>
                            @endif
                            @if($prospect->category_id == 3 || $prospect->category_id == 5)
                                <tr>
                                    <th>Comments</th>
                                    <td>{{ $prospect->comments ?? '-' }}</td>
                                </tr>
                            @endif
                            {{--                            @if($prospect->user_type == 'service-provider')--}}
                            @if(!is_null( $prospect->agreement_signed_on))
                                <tr>
                                    <th>Agreement Signed On</th>
                                    <td>{{ $prospect->agreement_signed_on ? date('Y-m-d', strtotime($prospect->agreement_signed_on)) : '-' }}</td>
                                </tr>
                            @endif
                            @if(!is_null($prospect->signed_agreement_sent))
                                <tr>
                                    <th>Signed Agreement Sent</th>
                                    <td>{{ $prospect->signed_agreement_sent ? date('Y-m-d', strtotime($prospect->signed_agreement_sent)) : '-' }}</td>
                                </tr>
                            @endif
                            @if(!is_null($prospect->attached_files))
                                <tr>
                                    <th>Attached File</th>
                                    <td>

                                        <img width="300" height="200" class="img-thumbnail"
                                             style="display:{{($prospect->attached_files) ? 'block' : 'none'}};"
                                             id="img"
                                             src="{{ asset('/uploads/attachedFiles/'.$prospect->attached_files) }}"
                                             alt="your image"/>

                                    </td>
                                </tr>
                            @endif
                            {{--                            @endif--}}


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
