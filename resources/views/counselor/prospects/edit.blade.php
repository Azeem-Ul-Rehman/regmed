@extends('counselor.layouts.master')
@section('title','Prospects')
@push('css')
    <style>
        .modal .modal-content .modal-header {

            background: #f1743b !important;
        }

        .modal .modal-content .modal-header .modal-title {
            color: #fff !important;
        }


        .button-right {
            float: right;
            margin-top: 12px;
        }
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('Prospect') }}
                        </h3>
                    </div>
                </div>
            </div>


            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post"
                              action="{{ route('counselor.prospects.update',$prospect->id) }}"
                              id="create"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="interested_in"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Interested In') }}
                                                <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="interested_in"
                                                    class="form-control @error('interested_in') is-invalid @enderror"
                                                    name="interested_in" autocomplete="interested_in">
                                                <option value="" {{ (old('interested_in') == '') ? 'selected' : '' }}>
                                                    Select an
                                                    option
                                                </option>
                                                <option
                                                    value="cb_ct" {{ (old('interested_in',$prospect->interested_in) == 'cb_ct') ? 'selected' : '' }}>
                                                    CB CT
                                                </option>
                                                <option
                                                    value="at" {{ (old('interested_in',$prospect->interested_in) == 'at') ? 'selected' : '' }}>
                                                    AT
                                                </option>
                                                <option
                                                    value="treatments" {{ (old('interested_in',$prospect->interested_in) == 'treatments') ? 'selected' : '' }}>
                                                    Treatments
                                                </option>
                                                <option
                                                    value="others" {{ (old('interested_in',$prospect->interested_in) == 'others') ? 'selected' : '' }}>
                                                    Others
                                                </option>
                                            </select>

                                            @error('interested_in')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="date_of_birth"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Date of Birth') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="date_of_birth" type="date"
                                                   class="form-control @error('date_of_birth') is-invalid @enderror"
                                                   name="date_of_birth"
                                                   value="{{ old('date_of_birth',$prospect->date_of_birth) }}"
                                                   autocomplete="date_of_birth">

                                            @error('date_of_birth')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="first_name"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('First Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="first_name" type="text"
                                                   class="form-control @error('first_name') is-invalid @enderror"
                                                   name="first_name"
                                                   value="{{ old('first_name',$prospect->first_name) }}"
                                                   autocomplete="first_name" autofocus>

                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="last_name"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Last Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="last_name" type="text"
                                                   class="form-control @error('last_name') is-invalid @enderror"
                                                   name="last_name" value="{{ old('last_name',$prospect->last_name) }}"
                                                   autocomplete="last_name" autofocus>

                                            @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="address"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   value="{{ old('address',$prospect->address) }}"
                                                   autocomplete="address">

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="postal_code"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Postal Code') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="postal_code" type="text"
                                                   class="form-control @error('postal_code') is-invalid @enderror"
                                                   name="postal_code"
                                                   value="{{ old('postal_code',$prospect->postal_code) }}"
                                                   autocomplete="postal_code">

                                            @error('postal_code')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="city"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('City') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="city" type="text"
                                                   class="form-control @error('city') is-invalid @enderror"
                                                   name="city"
                                                   value="{{ old('city',$prospect->city) }}" autocomplete="city">

                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="country"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Country') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="country" type="text"
                                                   class="form-control @error('country') is-invalid @enderror"
                                                   name="country" value="{{ old('country',$prospect->country) }}"
                                                   autocomplete="country" autofocus>

                                            @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="email"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('E-Mail Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email"
                                                   value="{{ old('email',$prospect->email) }}" autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="phone_number"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Mobile Number') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="phone_number" type="text"
                                                   class="form-control @error('phone_number') is-invalid @enderror"
                                                   name="phone_number"
                                                   value="{{ old('phone_number',$prospect->phone_number) }}"
                                                   autocomplete="phone_number" autofocus placeholder="03001234567">

                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="language"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Language') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="language" type="text"
                                                   class="form-control @error('language') is-invalid @enderror"
                                                   name="language" value="{{ old('language',$prospect->language) }}"
                                                   autocomplete="language" autofocus placeholder="language">

                                            @error('language')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="how_did_it_find_us"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('How did it find us') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="how_did_it_find_us" type="text"
                                                   class="form-control @error('how_did_it_find_us') is-invalid @enderror"
                                                   name="how_did_it_find_us"
                                                   value="{{ old('how_did_it_find_us',$prospect->how_did_it_find_us) }}"
                                                   autocomplete="how_did_it_find_us" autofocus
                                                   placeholder="how_did_it_find_us">

                                            @error('how_did_it_find_us')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-6">
                                            <label for="service_provider"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Service Provider') }}
                                                <span class="mandatorySign">*</span></label>
                                            <select id="service_provider"
                                                    class="form-control serviceproviders @error('service_provider') is-invalid @enderror"
                                                    name="service_provider" autocomplete="service_provider">
                                                @if(!empty($serviceProviders))
                                                    <option value="">Select Service Provider</option>
                                                    @foreach($serviceProviders as $sp)
                                                        <option
                                                            value="{{$sp->id}}" {{ (old('service_provider',$prospect->service_provider) == $sp->service_provider) ? 'selected' : '' }}>{{ucfirst($sp->first_name)}} {{ucfirst($sp->last_name)}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('service_provider')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="status"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Status') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="status"
                                                    class="form-control cities @error('status') is-invalid @enderror"
                                                    name="status" autocomplete="status">
                                                <option value="" {{ (old('status') == '') ? 'selected' : '' }}>Select an
                                                    option
                                                </option>
                                                <option
                                                    value="pending" {{ (old('status',$prospect->status) == 'pending') ? 'selected' : '' }}>
                                                    Pending
                                                </option>
                                                <option
                                                    value="verified" {{ (old('status',$prospect->status) == 'verified') ? 'selected' : '' }}>
                                                    Verified
                                                </option>
                                                <option
                                                    value="suspended" {{ (old('status',$prospect->status) == 'suspended') ? 'selected' : '' }}>
                                                    Suspended
                                                </option>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <hr>


                                    <div id="prospect_cb_ct"
                                         style="display: {{ ($prospect->category_id == 3) ? '' : 'none' }}">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="father_name"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Father Name') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="father_name" type="text"
                                                       class="form-control @error('father_name') is-invalid @enderror"
                                                       name="father_name"
                                                       value="{{ old('father_name',$prospect->father_name) }}"
                                                       autocomplete="father_name" autofocus>

                                                @error('father_name')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="expected_date_of_birth_of_baby"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Expected Baby Date of Birth') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="expected_date_of_birth_of_baby" type="date"
                                                       class="form-control @error('expected_date_of_birth_of_baby') is-invalid @enderror"
                                                       name="expected_date_of_birth_of_baby"
                                                       value="{{ old('expected_date_of_birth_of_baby',$prospect->expected_date_of_birth_of_baby) }}"
                                                       autocomplete="expected_date_of_birth_of_baby" autofocus>

                                                @error('expected_date_of_birth_of_baby')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="expected_date_clinic"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Expected Clinic') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="expected_date_clinic" type="text"
                                                       class="form-control @error('expected_date_clinic') is-invalid @enderror"
                                                       name="expected_date_clinic"
                                                       value="{{ old('expected_date_clinic',$prospect->expected_date_clinic) }}"
                                                       autocomplete="expected_date_clinic" autofocus>

                                                @error('expected_date_clinic')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="twin_pregnancy"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Twin Pregancy') }}
                                                    <span
                                                        class="mandatorySign">*</span></label>

                                                <select id="twin_pregnancy"
                                                        class="form-control @error('twin_pregnancy') is-invalid @enderror"
                                                        name="twin_pregnancy" autocomplete="twin_pregnancy">
                                                    <option
                                                        value="" {{ (old('twin_pregnancy') == '') ? 'selected' : '' }}>
                                                        Select an
                                                        option
                                                    </option>
                                                    <option
                                                        value="yes" {{ (old('twin_pregnancy',$prospect->twin_pregnancy) == 'yes') ? 'selected' : '' }}>
                                                        Yes
                                                    </option>
                                                    <option
                                                        value="no" {{ (old('twin_pregnancy',$prospect->twin_pregnancy) == 'no') ? 'selected' : '' }}>
                                                        No
                                                    </option>
                                                </select>

                                                @error('twin_pregnancy')
                                                <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="contract_signed"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Contract Signed') }}
                                                    <span class="contract_signed">*</span></label>

                                                <select id="contract_signed"
                                                        class="form-control @error('contract_signed') is-invalid @enderror"
                                                        name="contract_signed" autocomplete="contract_signed">
                                                    <option value="">Select an Contract Signed</option>
                                                    <option
                                                        value="yes" {{ old('contract_signed',$prospect->contract_signed) == 'yes' ? 'selected' : '' }}>
                                                        Yes
                                                    </option>
                                                    <option
                                                        value="no" {{ old('contract_signed',$prospect->contract_signed) == 'no' ? 'selected' : '' }}>
                                                        No
                                                    </option>
                                                </select>

                                                @error('contract_signed')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="contract_signed_date"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Contract Signed Date') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="contract_signed_date" type="date"
                                                       class="form-control @error('contract_signed_date') is-invalid @enderror"
                                                       name="contract_signed_date"
                                                       value="{{ old('contract_signed_date',$prospect->contract_signed_date) }}"
                                                       autocomplete="contract_signed_date">

                                                @error('contract_signed_date')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="kit_sent"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Kit Sent') }}
                                                    <span class="demo_kit">*</span></label>

                                                <select id="kit_sent"
                                                        class="form-control @error('kit_sent') is-invalid @enderror"
                                                        name="kit_sent" autocomplete="kit_sent">
                                                    <option value="">Select an Kit Sent</option>
                                                    <option
                                                        value="yes" {{ old('kit_sent',$prospect->kit_sent) == 'yes' ? 'selected' : '' }}>
                                                        Yes
                                                    </option>
                                                    <option
                                                        value="no" {{ old('kit_sent',$prospect->kit_sent) == 'no' ? 'selected' : '' }}>
                                                        No
                                                    </option>
                                                </select>

                                                @error('kit_sent')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="kit_sent_date"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Kit Sent Date') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="kit_sent_date" type="date"
                                                       class="form-control @error('kit_sent_date') is-invalid @enderror"
                                                       name="kit_sent_date"
                                                       value="{{ old('kit_sent_date',$prospect->kit_sent_date) }}"
                                                       autocomplete="kit_sent_date">

                                                @error('kit_sent_date')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="process_completed"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Process Completed') }}
                                                    <span class="process_completed">*</span></label>

                                                <select id="process_completed"
                                                        class="form-control @error('process_completed') is-invalid @enderror"
                                                        name="process_completed" autocomplete="process_completed">
                                                    <option value="">Select an Process Completed</option>
                                                    <option
                                                        value="yes" {{ old('process_completed',$prospect->process_completed) == 'yes' ? 'selected' : '' }}>
                                                        Yes
                                                    </option>
                                                    <option
                                                        value="no" {{ old('process_completed',$prospect->process_completed) == 'no' ? 'selected' : '' }}>
                                                        No
                                                    </option>
                                                </select>

                                                @error('process_completed')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="mild_wife"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Dr./Midwife') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="mild_wife" type="text"
                                                       class="form-control @error('mild_wife') is-invalid @enderror"
                                                       name="mild_wife"
                                                       value="{{ old('mild_wife',$prospect->mild_wife) }}"
                                                       autocomplete="mild_wife">

                                                @error('mild_wife')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div id="prospects_at"
                                         style="display: {{ ($prospect->category_id == 4) ? '' : 'none' }}">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="date_of_liposuction"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Date of Liposuction') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="date_of_liposuction" type="date"
                                                       class="form-control @error('date_of_liposuction') is-invalid @enderror"
                                                       name="date_of_liposuction"
                                                       value="{{ old('date_of_liposuction',$prospect->date_of_liposuction) }}"
                                                       autocomplete="date_of_liposuction" autofocus>

                                                @error('date_of_liposuction')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="doctor"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Doctor') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="doctor" type="text"
                                                       class="form-control @error('doctor') is-invalid @enderror"
                                                       name="doctor" value="{{ old('doctor') }}"
                                                       autocomplete="doctor" autofocus>

                                                @error('doctor')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6" id="prospects_at">
                                                <label for="clinic"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Clinic') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="clinic" type="text"
                                                       class="form-control @error('clinic') is-invalid @enderror"
                                                       name="clinic" value="{{ old('clinic',$prospect->clinic) }}"
                                                       autocomplete="clinic" autofocus>

                                                @error('clinic')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>


                                    <div id="prospects_treatments"
                                         style="display: {{ ($prospect->category_id == 5) ? '' : 'none' }}">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="area_of_interest"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Area of Interest') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="area_of_interest" type="text"
                                                       class="form-control @error('area_of_interest') is-invalid @enderror"
                                                       name="area_of_interest"
                                                       value="{{ old('area_of_interest',$prospect->area_of_interest) }}"
                                                       autocomplete="area_of_interest" autofocus>

                                                @error('area_of_interest')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="vacations_included"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Vacations Included') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="vacations_included" type="text"
                                                       class="form-control @error('vacations_included') is-invalid @enderror"
                                                       name="vacations_included"
                                                       value="{{ old('vacations_included',$prospect->vacations_included) }}"
                                                       autocomplete="vacations_included" autofocus>

                                                @error('vacations_included')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="willing_to_travel_inside_eu"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Willing to Travel Inside EU') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="willing_to_travel_inside_eu" type="text"
                                                       class="form-control @error('willing_to_travel_inside_eu') is-invalid @enderror"
                                                       name="willing_to_travel_inside_eu"
                                                       value="{{ old('willing_to_travel_inside_eu',$prospect->willing_to_travel_inside_eu) }}"
                                                       autocomplete="willing_to_travel_inside_eu" autofocus>

                                                @error('willing_to_travel_inside_eu')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="willing_to_travel_outside_eu"
                                                       class="col-md-6 col-form-label text-md-left">{{ __('Willing to Travel Outside EU') }}
                                                    <span class="mandatorySign">*</span></label>
                                                <input id="willing_to_travel_outside_eu" type="text"
                                                       class="form-control @error('willing_to_travel_outside_eu') is-invalid @enderror"
                                                       name="willing_to_travel_outside_eu"
                                                       value="{{ old('willing_to_travel_outside_eu',$prospect->willing_to_travel_outside_eu) }}"
                                                       autocomplete="willing_to_travel_outside_eu" autofocus>

                                                @error('willing_to_travel_outside_eu')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row" id="comments_treatments"
                                         style="display: {{ ($prospect->category_id == 3 || $prospect->category_id == 5) ? '' : 'none' }}">
                                        <div class="col-md-12">
                                            <label for="comments"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Comments') }}
                                                <span class="mandatorySign">*</span></label>
                                            <textarea id="comments" type="text"
                                                      class="form-control @error('comments') is-invalid @enderror"
                                                      name="comments"
                                                      autocomplete="comments"
                                                      autofocus>{{ old('comments',$prospect->comments) }}</textarea>

                                            @error('comments')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                    @if($prospect->user_type == 'service-provider')
                                        <div>
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <label for="agreement_signed_on"
                                                           class="col-md-6 col-form-label text-md-left">{{ __('Agreement Signed On') }}
                                                        <span class="mandatorySign">*</span></label>
                                                    <input id="agreement_signed_on" type="date"
                                                           class="form-control @error('agreement_signed_on') is-invalid @enderror"
                                                           name="agreement_signed_on"
                                                           value="{{ old('agreement_signed_on',$prospect->agreement_signed_on) }}"
                                                           autocomplete="agreement_signed_on" autofocus>

                                                    @error('agreement_signed_on')
                                                    <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="signed_agreement_sent"
                                                           class="col-md-6 col-form-label text-md-left">{{ __('Signed Agreement Sent') }}
                                                        <span class="mandatorySign">*</span></label>
                                                    <input id="signed_agreement_sent" type="date"
                                                           class="form-control @error('signed_agreement_sent') is-invalid @enderror"
                                                           name="signed_agreement_sent"
                                                           value="{{ old('signed_agreement_sent',$prospect->signed_agreement_sent) }}"
                                                           autocomplete="signed_agreement_sent" autofocus>

                                                    @error('signed_agreement_sent')
                                                    <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">

                                                <div class="col-md-6">
                                                    <label for="attached_files"
                                                           class="col-md-4 col-form-label text-md-left">{{ __('Attached Files') }}</label>
                                                    <input value="{{old('attached_files')}}" type="file"
                                                           class="form-control @error('attached_files') is-invalid @enderror"
                                                           onchange="readURL(this)" id="image"
                                                           name="attached_files" style="padding: 9px; cursor: pointer">
                                                    <img width="300" height="200" class="img-thumbnail"
                                                         style="display:{{($prospect->attached_files) ? 'block' : 'none'}};"
                                                         id="img"
                                                         src="{{ asset('/uploads/attachedFiles/'.$prospect->attached_files) }}"
                                                         alt="your image"/>

                                                    @error('attached_files')
                                                    <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    @endif


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('counselor.prospects.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection



@push('js')
    <script>

        $('.roles').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.categories';
            var role_id = $(this).val();
            var request = "role_id=" + role_id;

            if (role_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.role.categories') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.categories, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Category</option>");


                            $('.categories').find('option[value="{{ old('categroy_id') }}"]').attr('selected', 'selected');
                            $('.categories').trigger('change');
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select Area</option>");
            }
        });

        $('.categories').change(function () {
            var cat = $("#category_id option:selected").val();

            if (cat == '3') {
                $('#prospects_at').css('display', 'none');
                $('#prospects_treatments').css('display', 'none');
                $('#prospect_cb_ct').css('display', '');
                $('#comments_treatments').css('display', '');
            } else if (cat == '4') {
                $('#prospects_at').css('display', '');
                $('#prospects_treatments').css('display', 'none');
                $('#prospect_cb_ct').css('display', 'none');
                $('#comments_treatments').css('display', 'none');
            } else if (cat == '5') {
                $('#prospects_at').css('display', 'none');
                $('#prospects_treatments').css('display', '');
                $('#prospect_cb_ct').css('display', 'none');
                $('#comments_treatments').css('display', '');
            } else {
                $('#prospects_at').css('display', 'none');
                $('#prospects_treatments').css('display', 'none');
                $('#prospect_cb_ct').css('display', 'none');
                $('#comments_treatments').css('display', 'none');
            }

        });
    </script>
@endpush
