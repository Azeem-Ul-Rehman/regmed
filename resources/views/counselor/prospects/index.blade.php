@extends('counselor.layouts.master')
@section('title','Prospects')
@push('css')
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">--}}
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">--}}
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <style>
        .responsiveTable {
            overflow-x: scroll;
            margin-top: 10px;
        }

        @media screen and (max-width: 650px) {
            .responsiveTable {
                overflow-x: scroll;
                margin-top: 10px;
            }

            .responsiveTable a {
                width: 100%;
                margin: 5px 0;
            }

            #show_list, #download_list, #refresh {
                margin-bottom: 10px;
            }

        }

        .daterangepicker .ranges .input-mini {
            width: 100% !important;
        }

        .daterangepicker .ranges .range_inputs > div:nth-child(2) {
            padding-left: 0px !important;
        }

        .daterangepicker .ranges .range_inputs .btn {
            padding: 7px !important;
            margin-right: 4px !important;
        }

    </style>

@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Prospects
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('counselor.prospects.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Prospect</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="box">
                    <div class="col-md-12">
                        <div class="box-body">
                            <form action="#" method="get" id="form-data">
                                @csrf
                                <div class="row">

                                    <div class="col-md-4 {{ $errors->has('category_id') ? 'has-error' : ''}}">
                                        <label for="category_id">Prospect Category</label>
                                        <select class="form-control"
                                                name="category_id"
                                                id="category_id">
                                            <option value="">Select Category
                                            </option>
                                            @if(!empty($categories) && count($categories) > 0)
                                                @foreach ($categories as $category)
                                                    @if(!is_null($category))
                                                        <option
                                                            value="{{$category->id}}" {{ $category->id == old('category_id') ? 'selected' : ''}}>{{$category->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <option value="all">All
                                            </option>
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('category_id') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('city') ? 'has-error' : ''}}">
                                        <label for="city">City</label>
                                        <select class="form-control"
                                                name="city"
                                                id="city">
                                            <option value="">Select City
                                            </option>
                                            @foreach ($cities as $city)
                                                @if(!is_null($city))
                                                    <option
                                                        value="{{$city}}" {{ $city == old('city') ? 'selected' : ''}}>{{ucfirst($city) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('city'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('city') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('country') ? 'has-error' : ''}}">
                                        <label for="country">Country</label>
                                        <select class="form-control"
                                                name="country"
                                                id="country">
                                            <option value="">Select Country
                                            </option>
                                            @foreach ($countries as $country)
                                                @if(!is_null($country))
                                                    <option
                                                        value="{{$country}}" {{ $country == old('country') ? 'selected' : ''}}>{{ucfirst($country) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('country') }}</span>
                                        @endif
                                    </div>

                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4 {{ $errors->has('contract_signed') ? 'has-error' : ''}}">
                                        <label for="contract_signed">Signed Contract</label>
                                        <select class="form-control" name="contract_signed" id="contract_signed">
                                            <option value="">Select option</option>
                                            <option value="yes" {{ 'yes' == old('contract_signed') ? 'selected' : ''}}>
                                                YES
                                            </option>
                                            <option
                                                value="no" {{ 'no' == old('contract_signed') ? 'selected' : ''}}>
                                                NO
                                            </option>
                                        </select>
                                        @if ($errors->has('contract_signed'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('contract_signed') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('contract_signed_date') ? 'has-error' : ''}}">
                                        <label for="contract_signed_date">Signed Contract Date</label>
                                        <input type="text" class="form-control" name="contract_signed_date"
                                               id="contract_signed_date"
                                               placeholder="DD-MM-YYYY"
                                               value="{{ old('contract_signed_date') }}">
                                        @if ($errors->has('contract_signed_date'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('contract_signed_date') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('registration_date') ? 'has-error' : ''}}">
                                        <label for="registration_date">Registration Date</label>
                                        <input type="text" class="form-control" name="registration_date"
                                               id="registration_date"
                                               placeholder="DD-MM-YYYY"
                                               value="{{ old('registration_date') }}">
                                        @if ($errors->has('registration_date'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('registration_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4 {{ $errors->has('kit_sent') ? 'has-error' : ''}}">
                                        <label for="kit_sent">Kit Sent</label>
                                        <select class="form-control" name="kit_sent" id="kit_sent">
                                            <option value="">Select option</option>
                                            <option value="yes" {{ 'yes' == old('kit_sent') ? 'selected' : ''}}>
                                                YES
                                            </option>
                                            <option
                                                value="no" {{ 'no' == old('kit_sent') ? 'selected' : ''}}>
                                                NO
                                            </option>
                                        </select>
                                        @if ($errors->has('kit_sent'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('kit_sent') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('kit_sent_date') ? 'has-error' : ''}}">
                                        <label for="kit_sent_date">Kit Sent Date</label>
                                        <input type="text" class="form-control" name="kit_sent_date"
                                               id="kit_sent_date"
                                               placeholder="DD-MM-YYYY"
                                               value="{{ old('kit_sent_date') }}">
                                        @if ($errors->has('kit_sent_date'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('kit_sent_date') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('process_completed') ? 'has-error' : ''}}">
                                        <label for="process_completed">Process Completed</label>
                                        <select class="form-control" name="process_completed" id="process_completed">
                                            <option value="">Select option</option>
                                            <option
                                                value="yes" {{ 'yes' == old('process_completed') ? 'selected' : ''}}>
                                                YES
                                            </option>
                                            <option
                                                value="no" {{ 'no' == old('process_completed') ? 'selected' : ''}}>
                                                NO
                                            </option>
                                        </select>
                                        @if ($errors->has('process_completed'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('process_completed') }}</span>
                                        @endif
                                    </div>

                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4 {{ $errors->has('interested_in') ? 'has-error' : ''}}">
                                        <label for="interested_in">Interested In</label>
                                        <select class="form-control" name="interested_in" id="interested_in">
                                            <option value="">Select option</option>
                                            <option
                                                value="cb_ct" {{ 'cb_ct' == old('interested_in') ? 'selected' : ''}}>
                                                CB CT
                                            </option>
                                            <option
                                                value="at" {{ 'at' == old('interested_in') ? 'selected' : ''}}>
                                                AT
                                            </option>
                                            <option
                                                value="treatments" {{ 'treatments' == old('interested_in') ? 'selected' : ''}}>
                                                Treatments
                                            </option>
                                            <option
                                                value="others" {{ 'others' == old('interested_in') ? 'selected' : ''}}>
                                                Others
                                            </option>
                                        </select>
                                        @if ($errors->has('interested_in'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('interested_in') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('date_of_birth') ? 'has-error' : ''}}">
                                        <label for="date_of_birth">Date of Birth</label>
                                        <input type="text" class="form-control" name="date_of_birth"
                                               id="date_of_birth"
                                               placeholder="DD-MM-YYYY"
                                               value="{{ old('date_of_birth') }}">
                                        @if ($errors->has('date_of_birth'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('date_of_birth') }}</span>
                                        @endif
                                    </div>
                                    <div
                                        class="col-md-4 {{ $errors->has('expected_date_of_birth_of_baby') ? 'has-error' : ''}}">
                                        <label for="expected_date_of_birth_of_baby">Expected Baby Date of Birth</label>
                                        <input type="text" class="form-control" name="expected_date_of_birth_of_baby"
                                               id="expected_date_of_birth_of_baby"
                                               placeholder="DD-MM-YYYY"
                                               value="{{ old('expected_date_of_birth_of_baby') }}">
                                        @if ($errors->has('expected_date_of_birth_of_baby'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('expected_date_of_birth_of_baby') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4 {{ $errors->has('twin_pregnancy') ? 'has-error' : ''}}">
                                        <label for="twin_pregnancy">Twin Pregnancy</label>
                                        <select class="form-control" name="twin_pregnancy" id="twin_pregnancy">
                                            <option value="">Select option</option>
                                            <option value="yes" {{ 'yes' == old('twin_pregnancy') ? 'selected' : ''}}>
                                                YES
                                            </option>
                                            <option
                                                value="no" {{ 'no' == old('twin_pregnancy') ? 'selected' : ''}}>
                                                NO
                                            </option>
                                        </select>
                                        @if ($errors->has('twin_pregnancy'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('twin_pregnancy') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('date_of_liposuction') ? 'has-error' : ''}}">
                                        <label for="date_of_liposuction">Date of Liposuction</label>
                                        <input type="text" class="form-control" name="date_of_liposuction"
                                               id="date_of_liposuction"
                                               placeholder="DD-MM-YYYY"
                                               value="{{ old('date_of_liposuction') }}">
                                        @if ($errors->has('date_of_liposuction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('date_of_liposuction') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <a href="{{ route('counselor.prospects.index') }}" id="refresh"
                                               class="btn btn-block btn-primary float-right"> <i
                                                    class="la la-refresh"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" id="show_list"
                                                class="btn btn-block btn-primary float-right">Show Lists
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" id="download_list"
                                                class="btn btn-block btn-primary float-right">Download
                                            Report
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="responsiveTable">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                        <tr>

                            <th> Registration Date</th>
                            <th> Created By</th>
                            <th> Role</th>
                            <th> Category</th>
                            <th> Name</th>
                            <th> Email</th>
                            <th> City</th>
                            <th> Country</th>
                            <th> Contract Signed</th>
                            <th> Kit Sent</th>
                            <th> Process Finalized</th>
                            <th> Sign Up Status</th>
                            <th class='notexport'> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($prospects))
                            @foreach($prospects as $prospect)
                                @if($prospect->user_type != 'super-admin')
                                    <tr>
                                        <td>{{$prospect->registration_date ? date('d-m-Y',strtotime($prospect->registration_date)) : '--'}}</td>
                                        <td>{{$prospect->creator ? $prospect->creator->first_name . " ". $prospect->creator->last_name : ''}}</td>
                                        <td>{{$prospect->roles ?  $prospect->roles[0]->name : 'Not Found'}}</td>
                                        <td>{{$prospect->category ?  $prospect->category->name : 'Not Found'}}</td>
                                        <td>{{ucfirst($prospect->last_name)}} {{ ucfirst($prospect->first_name) }}</td>
                                        <td>{{$prospect->email ?? ''}}</td>
                                        <td>{{$prospect->city ?? '--'}}</td>
                                        <td>{{$prospect->country ?? '--'}}</td>
                                        <td>{{$prospect->contract_signed ?? '--'}}</td>
                                        <td>{{$prospect->kit_sent ?? '--'}}</td>
                                        <td>{{$prospect->process_completed ?? '--'}}</td>
                                        <td style="width: 10%;">{{ucfirst($prospect->status)}}</td>
                                        <td style="display: flex;">
                                            <a href="{{route('counselor.prospects.show',$prospect->id)}}"
                                               id="view{{$prospect->id}}"
                                               class="btn btn-sm btn-info pull-left ">View</a>
                                            <a href="{{route('counselor.prospects.edit',$prospect->id)}}"
                                               id="edit{{$prospect->id}}"
                                               class="btn btn-sm btn-success pull-left ">Edit</a>
                                            <form method="post"
                                                  action="{{ route('counselor.prospects.destroy', $prospect->id) }}"
                                                  id="delete_{{ $prospect->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <a class="btn btn-sm btn-danger m-left"
                                                   href="javascript:void(0)"
                                                   onclick="if(confirmDelete()){ document.getElementById('delete_<?=$prospect->id?>').submit(); }">
                                                    Delete
                                                </a>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

    {{--    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>--}}
    {{--    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>--}}
    {{--    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>--}}
    {{--    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>--}}
    {{--    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>--}}

    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>


    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        $('#contract_signed_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#registration_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#date_of_birth').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#kit_sent_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#expected_date_of_birth_of_baby').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#date_of_liposuction').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $("#m_table_1").DataTable({
            // dom: 'Blrtip',
            "columnDefs": [
                {orderable: false, targets: [12]}
            ],
            // buttons: [
            //     {
            //         extend: 'copy',
            //         text: 'Copy',
            //         exportOptions: {
            //             columns: ':visible:not(.notexport)'
            //         }
            //     },
            //     {
            //         extend: 'csv',
            //         text: 'CSV',
            //         exportOptions: {
            //             columns: ':visible:not(.notexport)'
            //         }
            //     },
            //     {
            //         extend: 'excel',
            //         text: 'EXCEL',
            //         exportOptions: {
            //             columns: ':visible:not(.notexport)'
            //         }
            //     },
            //     {
            //         extend: 'pdf',
            //         text: 'PDF',
            //         exportOptions: {
            //             columns: ':visible:not(.notexport)'
            //         }
            //     },
            //     {
            //         extend: 'print',
            //         text: 'PRINT',
            //         exportOptions: {
            //             columns: ':visible:not(.notexport)'
            //         }
            //     }
            // ]

        });

        $('#show_list').click(function () {
            $('#form-data').attr('action', '{{ route('counselor.prospects.index') }}');
            $('#form-data').attr('method', 'GET');
            $("#form-data").submit();
        });
        $('#download_list').click(function () {
            $('#form-data').attr('action', '{{ route('counselor.prospect.export') }}');
            $('#form-data').attr('method', 'POST');
            $("#form-data").submit();
        });


    </script>


@endpush
