@extends('counselor.layouts.master')
@section('title','Dashboard')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 purple" href="{{ route('counselor.prospects.index') }}">
                            <div class="visual">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup"
                                          data-value="{{ $users_count_prospects }}">{{ $users_count_prospects }}</span>
                                </div>
                                <div class="desc"> Prospects</div>
                            </div>
                        </a>
                    </div>


                </div>

            </div>


        </div>
    </div>
@endsection
@push('js')



@endpush
